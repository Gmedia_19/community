<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserCoverReleation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user', function($table) {
            $table->integer('cover_pic_id')->unsigned()->change();
            $table->foreign('cover_pic_id')->references('cover_id')->on('cover_pic')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user', function($table) {
            $table->dropForeign('user_cover_pic_id_foreign');
            $table->foreign('cover_pic_id')->references('cover_id')->on('cover_pic')->onDelete('cascade');
        });
    }
}
