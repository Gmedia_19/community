<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddJewellwryImageField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('jewellery_hub', function($table) {
        $table->string('image3')->after('stock_details')->nullable();
        $table->string('image2')->after('stock_details')->nullable();
        $table->string('image1')->after('stock_details')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::table('jewellery_hub', function($table) {
        $table->dropColumn('image3');
        $table->dropColumn('image2');
        $table->dropColumn('image1');
       });
    }
}
