<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeMatrimonyIntrestedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('matrimony_interest', function (Blueprint $table) {
      $table->engine = 'InnoDB';
      $table->increments('id')->unsigned();
      $table->integer('logged_id')->unsigned();
      $table->foreign('logged_id')->references('user_id')->on('matrimony')->onDelete('cascade');
      $table->integer('interested_in_id')->unsigned();
      $table->foreign('interested_in_id')->references('user_id')->on('matrimony')->onDelete('cascade');
      $table->string('status', 10)->nullable();
      $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('matrimony_interest');
    }
}
