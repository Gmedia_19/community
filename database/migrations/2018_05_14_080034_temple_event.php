<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TempleEvent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temple_event', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('user_id')->on('user')->onDelete('cascade');
            $table->integer('temple_id')->unsigned();
            $table->foreign('temple_id')->references('id')->on('temple')->onDelete('cascade');
            $table->string('event_name', 200)->nullable();
            $table->string('image')->nullable();
            $table->string('event_detail', 600)->nullable();
            $table->timestamp('date_time');
            $table->string('status', 30)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('temple_event'); 
    }
}
