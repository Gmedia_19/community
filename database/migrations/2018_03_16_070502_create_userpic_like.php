<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserpicLike extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('userpic_like', function (Blueprint $table) {
        $table->engine = 'InnoDB';
        $table->increments('pic_like_id')->unsigned();
        $table->integer('user_id')->unsigned();
        $table->foreign('user_id')->references('user_id')->on('user')->onDelete('cascade');
        $table->integer('pic_id')->unsigned();
        $table->foreign('pic_id')->references('pic_id')->on('user_pic')->onDelete('cascade');
        $table->timestamps(); 
     });  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('usepic_like');
    }
}
