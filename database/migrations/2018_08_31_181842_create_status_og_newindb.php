<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatusOgNewindb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('newdb_status', function (Blueprint $table) {
        $table->engine = 'InnoDB';
        $table->increments('id')->unsigned();
        $table->integer('user_id')->unsigned();
        $table->foreign('user_id')->references('user_id')->on('user')->onDelete('cascade');
        $table->integer('new_id')->unsigned();
        $table->foreign('new_id')->references('id')->on('new_in_db')->onDelete('cascade');
        $table->string('seen', 5)->nullable();
        $table->timestamps(); 
     });  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('newdb_status');
    }
}
