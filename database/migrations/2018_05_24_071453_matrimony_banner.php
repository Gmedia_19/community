<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MatrimonyBanner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('matrimony_banner', function (Blueprint $table) {
        $table->engine = 'InnoDB';
        $table->increments('id')->unsigned();
        $table->string('banner1')->nullable();
        $table->string('banner2')->nullable();
        $table->string('banner3')->nullable();
        $table->string('text1', 100)->nullable();
        $table->string('text2', 100)->nullable();
        $table->string('text3', 100)->nullable();
        $table->timestamps(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('matrimony_banner');
    }
}
