<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class B2bTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
     Schema::create('b2b', function (Blueprint $table) {
        $table->engine = 'InnoDB';
        $table->increments('b_id')->unsigned();
        $table->integer('user_id')->unsigned();
        $table->foreign('user_id')->references('user_id')->on('user')->onDelete('cascade');
        $table->string('product_name', 100)->nullable();
        $table->string('name', 100)->nullable();
        $table->string('contact_number', 10)->nullable();
        $table->string('emailId', 150)->nullable();
        $table->string('address', 200)->nullable();
        $table->string('content', 500)->nullable();
        $table->string('image')->nullable();
        $table->string('type', 30)->nullable();
        $table->string('quantity', 10)->nullable();
        $table->timestamps();
      });   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('b2b'); 
    }
}
