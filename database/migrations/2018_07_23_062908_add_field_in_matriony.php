<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldInMatriony extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        Schema::table('matrimony', function($table) {
        $table->string('marital_status', 30)->after('user_id')->nullable();
        $table->string('country', 20)->after('ambition')->nullable();
        $table->string('permanent_city', 30)->after('ambition')->nullable();
        $table->string('present_city', 30)->after('ambition')->nullable();
        $table->string('expectation', 150)->after('ambition')->nullable();
        $table->string('hobbies', 150)->after('ambition')->nullable();
        $table->string('ganna', 20)->after('nakshatra')->nullable();
        $table->string('blood_group', 20)->after('age')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::table('matrimony', function($table) {
        $table->dropColumn('marital_status');
        $table->dropColumn('country');
        $table->dropColumn('permanent_city');
        $table->dropColumn('present_city');
        $table->dropColumn('expectation');
        $table->dropColumn('hobbies');
        $table->dropColumn('ganna');
        $table->dropColumn('blood_group');
       });
    }
}
