<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSeenStatusInMatrimony extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('matrimony', function($table) {
        $table->unique('user_id');
        });

       Schema::table('matrimony_interest', function($table) {
        $table->string('n_status', 10)->after('status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('matrimony', function($table) {
            $table->dropColumn('user_id');
        });

        Schema::table('matrimony_interest', function($table) {
            $table->dropColumn('n_status');
       });
    }
}
