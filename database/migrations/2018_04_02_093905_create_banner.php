<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBanner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('banner', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('banner_id')->unsigned();
            $table->string('banner1')->nullable();
            $table->string('banner2')->nullable();
            $table->string('banner3')->nullable();
            $table->string('banner1text')->nullable();
            $table->string('banner2text')->nullable();
            $table->string('banner3text')->nullable();
            $table->string('banner1description')->nullable();
            $table->string('banner2description')->nullable();
            $table->string('banner3description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('banner');
    }
}
