<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class JewelleryHub extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
     Schema::create('jewellery_hub', function (Blueprint $table) {
          $table->engine = 'InnoDB';
          $table->increments('j_id')->unsigned();
          $table->integer('user_id')->unsigned();
          $table->foreign('user_id')->references('user_id')->on('user')->onDelete('cascade');
          $table->string('name', 100)->nullable();
          $table->string('shop_name', 100)->nullable();
          $table->string('contact_number')->nullable();
          $table->string('address')->nullable();
          $table->string('father_name', 100)->nullable();
          $table->string('quantity', 5)->nullable();
          $table->string('quality', 100)->nullable();
          $table->string('content')->nullable();
          $table->string('image')->nullable();
          $table->string('jewellery_type')->nullable();
          $table->string('type')->nullable();
          $table->string('stock_details')->nullable();
          //$table->string();
          $table->timestamps();
        });   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('jewellery_hub'); 
    }
}
