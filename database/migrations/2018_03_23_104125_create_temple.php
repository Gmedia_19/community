<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemple extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('temple', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('temple_id')->unsigned();
            $table->string('name')->nullable;
            $table->string('location')->nullable;
            $table->string('godsName')->nullable;
            $table->string('pic')->nullable;
            $table->string('admin')->nullable;
            $table->string('about')->nullable;
            $table->string('history')->nullable;
            $table->string('contacts')->nullable;
            $table->string('events')->nullable;
            $table->string('trust')->nullable;
            $table->string('members')->nullable;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('temple');
    }
}
