<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateB2B extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('b2b', function (Blueprint $table) {
        $table->engine = 'InnoDB';
        $table->increments('b_id')->unsigned();
        $table->string('name')->nullable();
        $table->string('contact_number')->nullable();
        $table->string('emailId')->nullable();
        $table->string('address')->nullable();
        $table->string('content')->nullable();
        $table->string('image')->nullable();
        $table->string('type')->nullable();
        $table->string('quantity')->nullable();
        $table->timestamps();
      });   //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('b2b'); 
    }
}
