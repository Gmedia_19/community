<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimelineComment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('timeline_comment', function (Blueprint $table) {
        $table->engine = 'InnoDB';
        $table->increments('id')->unsigned();
        $table->integer('user_id')->unsigned();
        $table->foreign('user_id')->references('user_id')->on('user')->onDelete('cascade');
        $table->integer('timeline_id')->unsigned();
        $table->foreign('timeline_id')->references('timeline_id')->on('timeline')->onDelete('cascade');
        $table->string('comment');
        $table->timestamps(); 
      });  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('timeline_comment');
    }
}
