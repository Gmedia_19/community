<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StatusInUserpicCoverpicTimeline extends Migration
{
    
    public function up()
    {
       Schema::table('coverpic_comment', function($table) {
            $table->string('status', 10)->after('pic_id')->nullable();
        });

        Schema::table('cover_pic', function($table) {
            $table->string('status', 10)->after('like_count')->nullable();
        });

        Schema::table('timeline_comment', function($table) {
            $table->string('status', 10)->after('comment')->nullable();
        });

        Schema::table('userpic_comment', function($table) {
            $table->string('status', 10)->after('pic_id')->nullable();
        });

        Schema::table('user_pic', function($table) {
            $table->string('status', 10)->after('like_count')->nullable();
        });
    }
    
    public function down()
    {
       Schema::table('coverpic_comment', function($table) {
            $table->dropColumn('status');
        }); 

       Schema::table('cover_pic', function($table) {
            $table->dropColumn('status');
        });

        Schema::table('timeline_comment', function($table) {
            $table->dropColumn('status');
        }); 

        Schema::table('userpic_comment', function($table) {
            $table->dropColumn('status');
        });

        Schema::table('user_pic', function($table) {
            $table->dropColumn('status');
        });
    }
}
