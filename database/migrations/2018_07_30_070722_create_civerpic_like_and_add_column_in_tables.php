<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCiverpicLikeAndAddColumnInTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('cover_pic_like', function (Blueprint $table) {
      $table->engine = 'InnoDB';
      $table->increments('id')->unsigned();
      $table->integer('user_id')->unsigned();
      $table->foreign('user_id')->references('user_id')->on('user')->onDelete('cascade');
      $table->integer('cover_id')->unsigned();
      $table->foreign('cover_id')->references('cover_id')->on('cover_pic')->onDelete('cascade');
      $table->string('status', 10);
      $table->timestamps();
      });

     Schema::table('timeline_like', function($table) {
      $table->string('status', 10)->after('timeline_id')->nullable();
      });

     Schema::table('userpic_like', function($table) {
      $table->string('status', 10)->after('pic_id')->nullable();
      });

     Schema::table('timeline', function($table) {
     $table->integer('posts_user_id')->unsigned()->after('status')->nullable();
     $table->foreign('posts_user_id')->references('user_id')->on('user')->onDelete('cascade');
     });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('cover_pic_like');

        Schema::table('timeline_like', function($table) {
        $table->dropColumn('status');
        });

        Schema::table('userpic_like', function($table) {
        $table->dropColumn('status');
       });

        Schema::table('timeline', function($table) {
       $table->dropForeign('timeline_posts_user_id_foreign');
       $table->dropColumn('posts_user_id');
       });
    }
}
