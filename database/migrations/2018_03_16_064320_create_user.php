<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('user', function (Blueprint $table) {
        $table->engine = 'InnoDB';
        $table->increments('user_id')->unsigned();
        $table->string('username')->unique();
        $table->string('first_name', 50);
        $table->string('last_name', 50)->nullable();
        $table->date('dob')->nullable();
        $table->string('gender', 30)->nullable();
        $table->string('mobile_number')->unique();
        $table->string('alternate_mobile_number', 50)->nullable();
        $table->string('email_id')->unique();
        $table->string('alternate_email_id', 100)->nullable();
        $table->string('password', 100);
        $table->string('working_in', 200)->nullable();
        $table->string('high_school', 200)->nullable();
        $table->string('graduation', 200)->nullable();
        $table->string('current_city', 200)->nullable();
        $table->string('permanent_city', 200)->nullable();
        $table->string('user_pic_id')->nullable();
        $table->string('cover_pic_id')->nullable();
        $table->string('status', 50);
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('user');
    }
}
