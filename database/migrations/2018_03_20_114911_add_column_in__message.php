<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInMessage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('message', function($table) {
        $table->string('message')->after('recipient_id')->nullable();
        $table->string('photos')->after('recipient_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::table('message', function($table) {
        $table->dropColumn('message');
        $table->dropColumn('photos');
       });
    }
}
