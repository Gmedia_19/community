<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Temple extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('temple', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('user_id')->on('user')->onDelete('cascade');
            $table->string('temple_name', 200)->nullable();
            $table->string('location', 100)->nullable();
            $table->string('gods_name', 300)->nullable();
            $table->string('image')->nullable();
            $table->string('about', 300)->nullable();
            $table->string('history', 1000)->nullable();
            $table->string('contact_number')->nullable();
            $table->string('trust', 500)->nullable();
            $table->string('members', 500)->nullable();
            $table->string('status', 15)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('temple');  
    }
}
