<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusInAllTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
       Schema::table('b2b', function($table) {
        $table->string('status', 10)->after('quantity')->nullable();
        });

       Schema::table('jewellery_hub', function($table) {
        $table->string('status', 10)->after('image3')->nullable();
        });

       Schema::table('news_events', function($table) {
        $table->string('status', 10)->after('content2')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::table('b2b', function($table) {
        $table->dropColumn('status');
       });

       Schema::table('jewellery_hub', function($table) {
        $table->dropColumn('status');
       });

       Schema::table('news_events', function($table) {
        $table->dropColumn('status');
       });
    }
}
