<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCareerTalent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('career_talent', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('user_id')->on('user')->onDelete('cascade');
            $table->string('profile_for', 20)->nullable();
            $table->string('name', 100)->nullable();
            $table->string('father_name', 100)->nullable();
            $table->string('email_id', 100)->unique();
            $table->string('mobile_number', 100)->unique();
            $table->string('image')->nullable();
            $table->string('industry_type', 50)->nullable();
            $table->string('contact_person', 100)->nullable();
            $table->string('address', 500)->nullable();
            $table->string('hiring_for', 250)->nullable();
            $table->string('applying_for', 250)->nullable();
            $table->string('keywords', 250)->nullable();
            $table->string('job_experience')->nullable();
            $table->string('previous_company')->nullable();
            $table->string('resume')->nullable();
            $table->string('status', 10);
            
            $table->timestamps();
        });  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('career_talent');
    }
}
