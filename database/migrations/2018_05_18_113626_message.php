<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Message extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('message', function (Blueprint $table) {
        $table->engine = 'InnoDB';
        $table->increments('message_id')->unsigned();
        $table->integer('sender_id')->unsigned();
        $table->foreign('sender_id')->references('user_id')->on('user');
        $table->integer('recipient_id')->unsigned();
        $table->foreign('recipient_id')->references('user_id')->on('user');
        $table->string('message', 1000)->nullable();
        $table->string('photos')->nullable();
        $table->string('video')->nullable();
        $table->string('sender_status', 20)->nullable();
        $table->string('recipient_status', 20)->nullable();
        $table->timestamp('sender_deleted_at')->nullable();
        $table->timestamp('recipient_deleted_at')->nullable();
        // $table->timestamp('sender_deleted_at')->default(DB::raw('CURRENT_TIMESTAMP'))->nullable();
        // $table->timestamp('recipient_deleted_at')->default(DB::raw('CURRENT_TIMESTAMP'))->nullable();
        $table->timestamps(); 
     });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('message'); 
    }
}
