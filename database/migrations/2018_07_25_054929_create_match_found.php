<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchFound extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('match_found', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('user_id')->unsigned()->unique();
            $table->foreign('user_id')->references('id')->on('matrimony')->onDelete('cascade');
            $table->integer('matched_id')->unsigned()->unique();
            $table->foreign('matched_id')->references('id')->on('matrimony')->onDelete('cascade');
            $table->unique(['user_id', 'matched_id']);
            $table->timestamp('commitment_date')->nullable();
            $table->string('place', 30)->nullable();
            $table->string('feedback', 300)->nullable();
            $table->string('image')->nullable();
            $table->string('status', 30)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('match_found');
    }
}
