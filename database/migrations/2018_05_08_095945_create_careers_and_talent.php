<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCareersAndTalent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('career_talent', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('user_id')->on('user')->onDelete('cascade');
            $table->string('profile_for', 20)->nullable();
            $table->string('name', 100)->nullable();
            $table->string('email_id', 100)->unique();
            $table->string('mobile_number', 100)->unique();
            $table->string('image')->nullable();
            $table->string('industry_type', 50)->nullable();
            $table->string('contact_person', 100)->nullable();
            $table->string('address', 100)->nullable();
            $table->string('current_location', 100)->nullable();
            $table->string('hiring_for')->nullable();
            $table->string('applying_for')->nullable();
            $table->string('keywords', 100)->nullable();
            $table->string('resume')->nullable();
            
            $table->timestamps();
        });  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('career_talent');
    }
}
