<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldInJewelleryHub extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jewellery_hub', function($table) {
        $table->string('home_number', 100)->after('contact_number')->nullable();
        });

        Schema::table('matrimony', function($table) {
        $table->string('alt_number', 15)->after('mobile_number')->nullable();
        });

        Schema::table('banner', function($table) {
        $table->string('banner5', 100)->after('banner3')->nullable();
        $table->string('banner4', 100)->after('banner3')->nullable();
        $table->string('banner5text', 50)->after('banner3text')->nullable();
        $table->string('banner4text', 50)->after('banner3text')->nullable();
        $table->string('banner5description', 100)->after('banner3description')->nullable();
        $table->string('banner4description', 100)->after('banner3description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::table('jewellery_hub', function($table) {
        $table->dropColumn('home_number');
       });

       Schema::table('matrimony', function($table) {
        $table->dropColumn('alt_number');
       });

       Schema::table('banner', function($table) {
        $table->dropColumn('banner5');
        $table->dropColumn('banner4');
        $table->dropColumn('banner5text');
        $table->dropColumn('banner5text');
        $table->dropColumn('banner5description');
        $table->dropColumn('banner4description');
       });
    }
}
