<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEducation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('education', function (Blueprint $table) {
        $table->engine = 'InnoDB';
        $table->increments('e_id')->unsigned();
        $table->string('name');
        $table->string('institutionName');
        $table->string('contact_number');
        $table->string('emailId')->nullable();
        $table->string('address', 300)->nullable();
        $table->string('content', 500)->nullable();
        $table->string('image')->nullable();
        $table->string('courses')->nullable();
        $table->string('type')->nullable();
        $table->timestamps(); 
     });  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('education');
    }
}
