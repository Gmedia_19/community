<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKundlipicInMatrimony extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('matrimony', function($table) {
        $table->string('kundli_pic')->after('rashi')->nullable();
        $table->dropUnique('matrimony_mobile_number_unique');
        $table->dropUnique('matrimony_email_id_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::table('matrimony', function($table) {
        $table->dropColumn('kundli_pic');
        $table->unique('matrimony_mobile_number_unique');
        $table->unique('matrimony_email_id_unique');
       });
    }
}
