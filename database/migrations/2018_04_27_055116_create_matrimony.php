<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatrimony extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('matrimony', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('user_id')->on('user')->onDelete('cascade');
            $table->string('profile_for', 20)->nullable();
            $table->string('first_name', 50);
            $table->string('last_name', 50)->nullable();
            $table->string('gender', 10)->nullable();
            $table->string('image')->nullable();
            $table->string('mobile_number', 15)->unique();
            $table->string('email_id', 70)->unique();
            $table->string('age', 40)->nullable();
            $table->string('colour', 20)->nullable();
            $table->string('height', 20)->nullable();
            $table->string('dob', 30)->nullable();
            $table->string('birth_place', 60)->nullable();
            $table->string('birth_time', 15)->nullable();
            $table->string('gothra', 30)->nullable();
            $table->string('nakshatra', 30)->nullable();
            $table->string('rashi', 30)->nullable();
            $table->string('highest_degree', 90)->nullable();
            $table->string('education_field', 90)->nullable();
            $table->string('income')->nullable();
            $table->string('working_in',90)->nullable();
            $table->string('working_as',90)->nullable();
            $table->string('ambition', 200)->nullable();
            $table->string('father_name')->nullable();
            $table->string('father_profession', 80)->nullable();
            $table->string('mother_name')->nullable();
            $table->string('mother_profession', 80)->nullable();
            $table->string('siblings', 50)->nullable();
            $table->string('status', 20)->nullable();
            $table->timestamps();
        });  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('matrimony');
    }
}
