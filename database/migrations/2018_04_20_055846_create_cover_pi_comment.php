<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoverPiComment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('coverpic_comment', function (Blueprint $table) {
        $table->engine = 'InnoDB';
        $table->increments('id')->unsigned();
        $table->string('comment');
        $table->integer('user_id')->unsigned();
        $table->foreign('user_id')->references('user_id')->on('user')->onDelete('cascade');
        $table->integer('pic_id')->unsigned();
        $table->foreign('pic_id')->references('pic_id')->on('user_pic')->onDelete('cascade');
        $table->timestamps(); 
     });  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('coverpic_comment');
    }
}
