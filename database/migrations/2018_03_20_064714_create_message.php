<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
     Schema::create('message', function (Blueprint $table) {
        $table->engine = 'InnoDB';
        $table->increments('message_id')->unsigned();
        $table->integer('sender_id')->unsigned();
        $table->foreign('sender_id')->references('user_id')->on('user');
        $table->integer('recipient_id')->unsigned();
        $table->foreign('recipient_id')->references('user_id')->on('user');
        $table->timestamp('sender_deleted_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        $table->timestamp('recipient_deleted_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        $table->timestamps(); 
     });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('message');
    }
}
