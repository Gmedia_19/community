<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnSizeInNewsEvents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('news_events', function ($table) {
            $table->string('category', 50)->change();
            $table->string('heading', 300)->change();
            $table->string('content1', 2000)->change();
            $table->string('content2', 3000)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('news_events', function ($table) {
            $table->string('category', 50)->change();
            $table->string('heading', 300)->change();
            $table->string('content1', 1000)->change();
            $table->string('content2', 2000)->change();
        });
    }
}
