<?php

return [
	'STATUS_SUCCESS' 			=> 'success',
    'STATUS_VALIDATION_FAIL' 	=> 'validation-fail',
    'STATUS_ERROR'				=> 'error',
    'STATUS_WARNING'			=> 'warning',
    'MODEL_NOT_FOUND'			=> 'No Record Found',
    'EXCEPTION_MSG'				=> 'Internal Server Error Occured',
    'SUCCESS_CODE' 				=> 1,
    'FAILURE_CODE' 				=> 0,
    'APP_PROFILE_PIC_PATH' 		=> 'images\profilePic',
];