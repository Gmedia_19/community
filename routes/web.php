  <?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::post('/postResetPassword', 'API\V1\ApiController@postResetPassword');

// Route::get('auth/{provider}', 'Auth\LoginController@redirectToProvider');
// Route::get('auth/{provider}/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('check', function()
{
  return session('chat');
});

Route::get('chat', 'ChatController@chat');
Route::post('deleteSession','ChatController@deleteSession');
Route::post('send/{id}', 'ChatController@send');
Route::post('getOldMessage', 'ChatController@getOldMessage');
Route::post('saveToSession', 'ChatController@saveToSession');

// Route::get('login1', function()
//   {
//     return view('password.login');
// });

//home

      Route::get('/', function()
  {
     return view('home.layout.home1');
});


 Route::get('/', function()
  {
     return view('daivajnya.home.layout.home2');
});



 Route::get('home2', function()
  {
     return view('daivajnya.home.layout.home2');
});

 Route::get('about', function()
  {
     return view('daivajnya.about');
});


Route::get('pricing', function()
  {
     return view('daivajnya.home.layout.pricing');
});

Route::get('contact', function()
  {
     return view('daivajnya.home.layout.contact');
});


//login

Route::get('register', function()
  {
     return view('auth.register');
});


//Admin Dashboard//

   Route::get('admin_dashboard', function()
  {
     return view('admin.layouts.admin_dashboard');
});


 Route::get('add', function()

  {
     return view('pages.addingUserDetails');
});

 
// Route::get('profilePic/{id}', 'API\V1\UserPicController@getProfilePic');

// Route::get('user/{id}', 'API\UserController@editUser');




Route::get('user-home', function()
  {
     return view('User_dashboard.pages.user-home');
});


  Route::get('sidebar', function()
  {
     return view('User_dashboard.pages.sidebar');
});


 Route::get('temp_header', function()
  {
     return view('temples.includes.temp_header');
});


  Route::get('temp_dashboard', function()
  {
     return view('temples.layout.temp_dashboard');
});


    Route::get('temp_form', function()
  {
     return view('temples.includes.temp_form');
});



    Route::get('temple_home', function()
  {
     return view('temples.layout.temple_home');
});

     
      Route::get('temple_list', function()
  {
     return view('temples.layout.temple_list');
});




      Route::get('b2b1', function()
  {
     return view('B2B.layout.b2b1');
});


//Matrimony//

       Route::get('matrimony1', function()
  {
     return view('matrimony.layout.matrimony1');
});


       Route::get('inbox', function()
  {
     return view('matrimony.layout.inbox');
});


//JewelleryHub

      Route::get('JewelleryHub1', function()
  {
     return view('JewelleryHub.layout.JewelleryHub1');
});


//Career

      Route::get('career1', function()
  {
     return view('career.layout.career1');
});



       Route::get('b2b', function()
  {
     return view('b2b.layout.b2b');
});



       Route::get('view_profile', function()
  {
     return view('matrimony.includes.view_profile');
});


       Route::get('news', function()
  {
     return view('news and events.layout.news');
});


 

       Route::get('profile', function()
  {
     return view('myprofile.layout.profile');
});


       Route::get('admin1', function()
  {
     return view('admin.layouts.admin1');
});


       Route::get('addnews', function()
  {
     return view('admin.pages.addnews');
});

  Route::post('addNews', 'API\V1\NewsEventsController@addNews');
  Route::get('getNews', 'API\V1\NewsEventsController@getNews');



       Route::get('addnews', function()
  {
     return view('admin.pages.addnews');
});


        Route::get('dbhub1', function()
  {
     return view('dbhub.layout.dbhub1');
});




        Route::get('specific_news', function()
  {
     return view('news and events.layout.specific_news');
});

        Route::get('career_talent', function()
  {
     return view('career.layout.career_talent');
});




         Route::get('entertainment', function()
  {
     return view('news and events.layout.entertainment');
});



         Route::get('allnews', function()
  {
     return view('news and events.includes.allnews');
});

         Route::get('sports', function()
  {
     return view('news and events.layout.sports');
});

         Route::get('death', function()
  {
     return view('news and events.layout.death');
});

         Route::get('marriage', function()
  {
     return view('news and events.layout.marriage');
});

         Route::get('specific_news', function()
  {
     return view('news and events.layout.specific_news');
});



//my profile

Route::get('userhome', function()
{
  return view('myprofile.layout.userhome');
});

Route::get('user-profile', function()
{
  return view('User_dashboard.layouts.user-profile');
});


Route::get('verifymail', function()
{
  return view('emails.verifymail');
});



Route::get('career2', function()
{
  return view('admin.layouts.career2');
});

Route::get('singlecandidate', function()
{
  return view('career.layout.singlecandidate');
});

Route::get('editprofile', function()
{
  return view('career.layout.editprofile');
});

Route::get('login', function()
{
  return view('auth.login');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
