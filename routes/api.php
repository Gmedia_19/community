<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => '1.0'], function () {
    Route::post('mail', 'API\V1\ApiController@mail');
    Route::post('register', 'API\V1\ApiController@register');
    Route::post('login', 'API\V1\ApiController@login');
    Route::post('recover', 'ApiController@recover');
    Route::get('verify/{id}', 'API\V1\ApiController@verifyUser');
    Route::post('forgotPassword', 'API\V1\ApiController@forgotPassword');
    Route::get('resetPassword/{id}', 'API\V1\ApiController@resetPassword');
    Route::group(['middleware' => ['web']], function () {   
        Route::get('auth/{provider}', 'Auth\LoginController@redirectToProvider');
        Route::get('auth/{provider}/callback', 'Auth\LoginController@handleProviderCallback');
    });
	//user

    Route::post('registration', 'API\V1\UserController@registration');
    Route::get('lastName', 'API\V1\UserController@lastName');

    //new in db
    Route::post('postNewInDB', 'API\V1\NewInDbController@postNewInDB');
    Route::get('getNewInDB', 'API\V1\NewInDbController@getNewInDB');

    Route::group(['middleware' => ['jauth']], function() {
        //Route::post('send/{id}', 'ChatController@send');
        Route::get('getAuthUser', 'API\V1\ApiController@getAuthUser');
        Route::get('logout', 'API\V1\ApiController@logout');
        Route::put('addUser/{id}', 'API\V1\UserController@addUserDetails');
        Route::get('user/{id}', 'API\V1\UserController@getUser');
        Route::get('allUser', 'API\V1\UserController@getAllUser');

        // timeline
        Route::get('getTimeline/{id}', 'API\V1\TimelineController@getTimeline');
        Route::get('getAllTimeline', 'API\V1\TimelineController@getAllTimeline');
        Route::post('addTimeline', 'API\V1\TimelineController@addTimeline');
        Route::put('editTimelineComment/{id}', 'API\V1\TimelineController@editTimelineComment');
        Route::post('shareTimeline/{id}', 'API\V1\TimelineController@shareTimeline');
        Route::post('shareTimelineInFriendProfile/{id}', 'API\V1\TimelineController@shareTimelineInFriendProfile');
        Route::post('deleteTimeline/{id}', 'API\V1\TimelineController@deleteTimeline');

        Route::post('timelineComment', 'API\V1\TimelineController@timelineComment');
        Route::get('getTimelineComment/{id}', 'API\V1\TimelineController@getTimelineComment');
        Route::delete('deleteTimelineComment/{id}', 'API\V1\TimelineController@deleteTimelineComment');
        Route::post('timelineLike', 'API\V1\TimelineController@timelineLike');
        Route::get('countTimelineLike/{id}', 'API\V1\TimelineController@countTimelineLike');
        Route::get('getNameOfTimelinePic/{id}', 'API\V1\TimelineController@getNameOfTimelinePic');
        Route::get('timelineLikeStatus/{userId}/{timelineId}', 'API\V1\TimelineController@timelineLikeStatus');

        //message
        Route::post('messages', 'API\V1\MessageController@sendMessage');
        Route::get('getMessages/{id}/{uid}', 'API\V1\MessageController@getMessage');

        //notification
        Route::post('notification', 'API\V1\NotificationController@sendNotification');
        Route::get('getNotification/{id}', 'API\V1\NotificationController@getNotification');
        Route::put('deleteNotification/{id}', 'API\V1\NotificationController@deleteNotification');

        //friends
        Route::post('sendRequest', 'API\V1\FriendController@sendRequest');
        Route::get('getFriendRequest/{id}', 'API\V1\FriendController@getRequest');
        Route::get('sentRequest/{id}', 'API\V1\FriendController@sentRequest');
        Route::post('acceptFriendRequest/{id}', 'API\V1\FriendController@acceptFriendRequest');
        Route::post('cancelFriendRequest/{id}', 'API\V1\FriendController@cancelFriendRequest');
        Route::get('getFriends/{id}', 'API\V1\FriendController@getFriends');
        Route::get('getFriendSuggestion/{id}', 'API\V1\FriendController@getFriendSuggestion');
        Route::get('getFriendStatus/{id}/{f_id}', 'API\V1\FriendController@getFriendStatus');
        Route::delete('deleteFriend/{id}', 'API\V1\FriendController@deleteFriend');

        //profilePic
        Route::post('updateProfilePic', 'API\V1\UserPicController@updateProfilePic');
        Route::get('getAllProfilePic/{id}', 'API\V1\UserPicController@getAllProfilePic');
        Route::put('deleteProfilePic/{id}', 'API\V1\UserPicController@deleteProfilePic');
        Route::post('profilePicComment', 'API\V1\UserPicController@profilePicComment');
        Route::put('editProfilePicComment/{id}', 'API\V1\UserPicController@editProfilePicComment');
        Route::get('getPicComment/{id}', 'API\V1\UserPicController@getPicComment');
        Route::delete('deleteProfilePicComment/{id}', 'API\V1\UserPicController@deleteProfilePicComment');
        Route::post('profilePicLike', 'API\V1\UserPicController@profilePicLike');
        Route::get('countProfilePicLike/{id}', 'API\V1\UserPicController@countProfilePicLike');
        Route::get('getNameOfProfilePic/{id}', 'API\V1\UserPicController@getNameOfProfilePic');
        Route::get('profilePicLikeStatus/{userId}/{picId}', 'API\V1\UserPicController@profilePicLikeStatus');

        //coverPic
        Route::post('updateCoverPic', 'API\V1\CoverPicController@updateCoverPic');
        Route::get('getCoverPic/{id}', 'API\V1\CoverPicController@getCoverPic');
        Route::put('deleteCoverPic/{id}', 'API\V1\CoverPicController@deleteCoverPic');
        Route::post('coverPicComment', 'API\V1\CoverPicController@coverPicComment');
        Route::put('editCoverPicComment/{id}', 'API\V1\CoverPicController@editCoverPicComment');
        Route::get('getCoverPicComment/{id}', 'API\V1\CoverPicController@getCoverPicComment');
        Route::delete('deleteCoverPicComment/{id}', 'API\V1\CoverPicController@deleteCoverPicComment');
        Route::post('coverPicLike', 'API\V1\CoverPicController@coverPicLike');
        Route::get('countCoverPicLike/{id}', 'API\V1\CoverPicController@countCoverPicLike');
        Route::get('getNameOfCoverPic/{id}', 'API\V1\CoverPicController@getNameOfCoverPic');
        Route::get('coverPicLikeStatus/{userId}/{coverId}', 'API\V1\CoverPicController@coverPicLikeStatus');

        //temple
        Route::post('addTemple', 'API\V1\TempleController@addTemple');
        Route::get('getTemples', 'API\V1\TempleController@getTemples');
        Route::put('editTemple/{id}', 'API\V1\TempleController@editTemple');
        Route::get('getTemple/{id}', 'API\V1\TempleController@getTemple');
        Route::put('deleteTemple/{id}', 'API\V1\TempleController@deleteTemple');

        Route::post('addTempleEvent', 'API\V1\TempleController@addTempleEvent');
        Route::put('editTempleEvent/{id}', 'API\V1\TempleController@editTempleEvent');
        Route::get('getTempleEvent/{id}', 'API\V1\TempleController@getTempleEvent');
        Route::get('getSpecificEvent/{id}', 'API\V1\TempleController@getSpecificTempleEvent');
        Route::put('deleteTempleEvent/{id}', 'API\V1\TempleController@deleteTempleEvent');

        //banner
        Route::post('banner/addBanner', 'API\V1\BannerController@addBanner');
        Route::get('banner/getBanners', 'API\V1\BannerController@getBanners');
        Route::put('banner/deleteBanner/{id}', 'API\V1\BannerController@deleteBanner');

        //news and events
        Route::post('addNews', 'API\V1\NewsEventsController@addNews');
        Route::put('editNews/{id}', 'API\V1\NewsEventsController@editNews');
        Route::get('getNews', 'API\V1\NewsEventsController@getNews');
        Route::get('getNews/{id}', 'API\V1\NewsEventsController@getSpecificNews');
        Route::get('getCategoryNews/{category}', 'API\V1\NewsEventsController@getCategoryNews');
        Route::put('deleteNews/{id}', 'API\V1\NewsEventsController@deleteNews');


        //Matrimony
        Route::post('matrimony/{id}', 'API\V1\MatrimonyController@addMatrimony');
        Route::put('updateMatrimony/{id}', 'API\V1\MatrimonyController@updateMatrimony');
        Route::get('getAllMarti/{gender}', 'API\V1\MatrimonyController@getAllMarti');
        
        Route::get('matrimony/filter', 'API\V1\MatrimonyController@filter');
       
        Route::get('getDetails/{id}', 'API\V1\MatrimonyController@getDetails');
        Route::put('deleteMatrimonyDetails/{id}', 'API\V1\MatrimonyController@deleteMatrimonyDetails');
        Route::post('sendInterest', 'API\V1\MatrimonyController@sendInterest');
        Route::get('statusInMatrimonyRequest/{m1Id}/{m2Id}', 'API\V1\MatrimonyController@statusInMatrimonyRequest');
        Route::put('acceptOrRejectInterest/{m1Id}/{m2Id}', 'API\V1\MatrimonyController@acceptOrRejectInterest');
        Route::get('matrimony/sentInterest/{id}', 'API\V1\MatrimonyController@sentInterest');
        Route::get('matrimony/getInterest/{id}', 'API\V1\MatrimonyController@getInterest');
        Route::post('matrimony/matrimonySeenStatus/{id}', 'API\V1\MatrimonyController@matrimonySeenStatus');

        // Match Found
        Route::post('matchFound', 'API\V1\MatchFoundController@matchFound');
        Route::get('getMatchedFound', 'API\V1\MatchFoundController@getMatchedFound');

        //matrimony banner
        Route::post('addMatrimonyBanner', 'API\V1\MatrimonyController@addBanner');
        Route::get('getMatrimonyBanner', 'API\V1\MatrimonyController@getBanners');

        //Jewellery Hub
        Route::post('jewelleryDetails', 'API\V1\JewelleryHubController@addJewelleryHubDetails');
        Route::put('editJewelleryDetails/{id}', 'API\V1\JewelleryHubController@editJewelleryDetails');
        Route::get('getJewelleryByType/{type}', 'API\V1\JewelleryHubController@getJewelleryByType');
        Route::get('myStock/{id}', 'API\V1\JewelleryHubController@myStock');
        Route::get('getSpecificDetails/{id}', 'API\V1\JewelleryHubController@getSpecificDetails');
        Route::put('deleteJewellery/{id}', 'API\V1\JewelleryHubController@deleteJewellery');

        Route::post('addJewelleryImage', 'API\V1\JewelleryHubController@addJewelleryImage');
        
        //B2B
        Route::post('addProduct', 'API\V1\B2BController@addProduct');
        Route::put('editProduct/{id}', 'API\V1\B2BController@editProduct');
        Route::get('getProducts', 'API\V1\B2BController@getProducts');
        Route::get('getProperty', 'API\V1\B2BController@getProperty');
        Route::get('getSpecificProduct/{id}', 'API\V1\B2BController@getSpecificProduct');
        Route::put('deleteProduct/{id}', 'API\V1\B2BController@deleteProduct');

        //DB_Hub
        Route::post('addHub', 'API\V1\DbHubController@addHub');
        Route::put('editHub/{id}', 'API\V1\DbHubController@editHub');
        Route::get('getAllHub', 'API\V1\DbHubController@getAllHub');
        Route::get('getCityHub/{id}', 'API\V1\DbHubController@getCityHub');
        Route::get('getStateHub/{id}', 'API\V1\DbHubController@getStateHub');
        Route::get('getSpecificHub/{id}', 'API\V1\DbHubController@getSpecificHub');
        Route::put('deleteDbHub/{id}', 'API\V1\DbHubController@deleteDbHub');

        Route::post('addHubEvent', 'API\V1\DbHubController@addHubEvent');
        Route::put('editHubEvent/{id}', 'API\V1\DbHubController@editHubEvent');
        Route::get('getHubEvent/{id}', 'API\V1\DbHubController@getHubEvent');
        Route::get('getSpecificHubEvent/{id}', 'API\V1\DbHubController@getSpecificHubEvent');
        Route::put('deleteBookedHub/{id}', 'API\V1\DbHubController@deleteBookedHub');

        //book a hall
        Route::post('addHall', 'API\V1\BookHallController@addHall');
        Route::get('getHalls', 'API\V1\BookHallController@getHalls');


        //career and talent
        Route::post('addProfile', 'API\V1\CareerTalentController@addProfile');
        Route::put('editProfile/{id}', 'API\V1\CareerTalentController@editProfile');
        Route::get('getJobSeeker', 'API\V1\CareerTalentController@getJobSeeker');
        Route::get('getCompany', 'API\V1\CareerTalentController@getCompany');
        Route::get('getCareerDetails/{id}', 'API\V1\CareerTalentController@getCareerDetails');
        Route::put('deleteCarrerAndTalent/{id}', 'API\V1\CareerTalentController@deleteCarrerAndTalent');
    });
});