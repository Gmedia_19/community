$(document).ready(function() {        
	
	/* ======= Fullpage.js ======= */ 
	/* Ref: https://github.com/alvarotrigo/fullPage.js */
        
    $('#fullpage').fullpage({
		anchors: ['home', 'benefit1','benefit2', 'benefit3', 'benefit4','benefit5','benefit6', 'benefit7','benefit8','benefit9','download'],
		navigation: true,
		navigationPosition: 'right',
		navigationTooltips: ['Home', ' New', 'My Profile', ' DB HUb', 'News and events', 'Jewellery Hub', 'Matrimony', 'B2B','Careers', 'Temples',  'Download'],
		resize : false,
		scrollBar: true,
		autoScrolling: false,
		paddingTop: '120px'
		
	});
    

});