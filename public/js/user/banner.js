var banner = {
    bannerDiv: function(data) {
        return               '<div class="item slides active">'
            +                    '<div class="slide">'
            +                        '<img src="' + data.banner1 + '" class="img-responsive"> '
            +                    '</div>'
            +                    '<div class="hero">'
            +                        '<hgroup>'
            +                           '<h1>' + data.banner1text + '</h1>'        
            +                               '<h3>' + data.banner1description + '</h3>'
            +                        '</hgroup>'
            +                        '<button class="btn btn-hero btn-lg" role="button">See all features</button>'
            +                    '</div>'
            +                 '</div>'
            +                 '<div class="item slides">'
            +                      '<div class="slide">'
            +                           '<img src="' + data.banner2 + '" class="img-responsive">'
            +                       '</div>'
            +                       '<div class="hero">'        
            +                           '<hgroup>'
            +                               '<h2>' + data.banner2text + '</h2>'       
            +                                   '<h3>' + data.banner2description + '</h3>'
            +                           '</hgroup>'       
            +                           '<button class="btn btn-hero btn-lg" role="button">See all features</button>'
            +                       '</div>'
            +                  '</div>'
            +                  '<div class="item slides">'
            +                       '<div class="slide">'
            +                           '<img src="' + data.banner3 + '" class="img-responsive">'
            +                       '</div>'
            +                       '<div class="hero">'        
            +                           '<hgroup>'
            +                               '<h1>' + data.banner3text + '</h1>'        
            +                                      '<h3>' + data.banner3description + '</h3>'
            +                           '</hgroup>'
            +                           '<button class="btn btn-hero btn-lg" role="button">See all features</button>'
            +                       '</div>'
            +                   '</div>'
    },
	getBanner: function() {
		// add loader
		$.ajax({
  			url: "/api/1.0/getBanners",
  			method: "GET",
  			dataType : 'JSON',
            success  : function(response) {
            	if(response.reasonCode) { // if +ve result
            		//populate timeline
                    var html = "";
                    $.each(response.data, function(index, value) {
                        html += banner.bannerDiv(value);
                    });
                    $('#banner').append(html);
            	} else { // if fail
            		// error handling
            		console.log("error");
            	} 
            },
            error: function(response) {
            	//handle error
            },
            complete: function(response) {
            	// remove loader
            }
		});
	},
	init: function() {
		banner.getBanner();
	}
}

$(function(){
	banner.init();
});