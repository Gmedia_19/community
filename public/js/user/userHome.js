
var allTimeline = {
    	allTimelineDiv: function(data) {
        return     '<div class="panel panel-white post panel-shadow">'
            +        '<div class="post-heading">'
            +           '<div class="pull-left image">'
            +               '<img src= "' + data.profilePic + '" class="img-circle avatar" alt="user profile image">'
            +            '</div>'
            +             '<div class="pull-left meta">'
            +                   '<div class="title h5">'
            +                       '<a href=""><b>' + data.firstName + '&nbsp'+ data.lastName + '</b></a>'
            +                   '</div>'
            +                   '<h6 class="text-muted time"> ' + data.postingTime + '</h6>'
            +             '</div>'
            +        '</div> '
            +       '<div class="post-description">'
            +           '<p>' + data.textContent + '</p>'
            +            '<div class="post-description">'
            +                '<img src="' + data.photos + '" class="img-responsive">' 
            +            '<div class="stats">'
            +               '<a href="#" class="btn btn-default stat-item">'
            +                   '<i class="fa fa-thumbs-up icon"></i>2'
            +               '</a>'
            +               '<a href="#" class="btn btn-default stat-item">'
            +                   '<i class="fa fa-thumbs-down icon"></i>comment'
            +               '</a>'
            +               '<a href="#" class="btn btn-default stat-item">'
            +                   '<i class="fa fa-thumbs-down icon"></i>share'
            +               '</a>'
            +          '</div>'
            +          '</div>'
            +       '</div>'
            +     '</div>'
    },
	getAllTimeline: function() {
		// add loader
		$.ajax({
  			url: "/api/1.0/getAllTimeline",
  			method: "GET",
  			dataType : 'JSON',
            success  : function(response) {
            	if(response.reasonCode) { // if +ve result
            		//populate timeline
                    var html = "<div>";
                    $.each(response.data, function(index, value) {
                        html += timeline.timelineDiv(value);
                    });
                    $('#allTimeline').append(html);
                    console.log(response.data);
            	} else { // if fail
            		// error handling
            		console.log("error");
            	} 
            },
            error: function(response) {
            	//handle error
            },
            complete: function(response) {
            	// remove loader
            }
		});
	},
	init: function() {
		allTimeline.getAllTimeline();
	}
}

$(function(){
	allTimeline.init();
});
