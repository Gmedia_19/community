// var uploadTimeline = {
//     uploadTimelineDiv: function(data) {
//         return     '<div class="panel panel-white post panel-shadow">'
//             +        '<div class="post-heading">'
//             +           '<div class="pull-left image">'
//             +               '<img src= "' + data.profilePic + '" class="img-circle avatar" alt="user profile image">'
//             +            '</div>'
//             +             '<div class="pull-left meta">'
//             +                   '<div class="title h5">'
//             +                       '<a href=""><b>' + data.firstName + '&nbsp'+ data.lastName + '</b></a>'
//             +                   '</div>'
//             +                   '<h6 class="text-muted time"> ' + data.postingTime + '</h6>'
//             +             '</div>'
//             +        '</div> '
//             +       '<div class="post-description">'
//             +           '<p>' + data.textContent + '</p>'
//             +            '<div class="post-description">'
//             +                '<img src="' + data.photos + '" class="img-responsive">' 
//             +            '<div class="stats">'
//             +               '<a href="#" class="btn btn-default stat-item">'
//             +                   '<i class="fa fa-thumbs-up icon"></i>2'
//             +               '</a>'
//             +               '<a href="#" class="btn btn-default stat-item">'
//             +                   '<i class="fa fa-thumbs-down icon"></i>comment'
//             +               '</a>'
//             +               '<a href="#" class="btn btn-default stat-item">'
//             +                   '<i class="fa fa-thumbs-down icon"></i>share'
//             +               '</a>'
//             +          '</div>'
//             +          '</div>'
//             +       '</div>'
//             +     '</div>'
//     },
//     getUploadTimeline: function() {
//         // add loader
//         $.ajax({
//             url: "/api/1.0/getTimeline/3",
//             method: "GET",
//             dataType : 'JSON',
//             success  : function(response) {
//                 if(response.reasonCode) { // if +ve result
//                     //populate timeline
//                     var html = "<div>";
//                     $.each(response.data, function(index, value) {
//                         html += timeline.timelineDiv(value);
//                     });
//                     $('#timeline').append(html);
//                 } else { // if fail
//                     // error handling
//                     console.log("error");
//                 } 
//             },
//             error: function(response) {
//                 //handle error
//             },
//             complete: function(response) {
//                 // remove loader
//             }
//         });
//     },
//     init: function() {
//         uploadTimeline.getUploadTimeline();
//     }
// }

// $(function(){
//     uploadTimeline.init();
// });


var timeline = {
    timelineDiv: function(data) {
        return     '<div class="panel panel-white post panel-shadow">'
            +        '<div class="post-heading">'
            +           '<div class="pull-left image">'
            +               '<img src= "' + data.profilePic + '" class="img-circle avatar" alt="user profile image">'
            +            '</div>'
            +             '<div class="pull-left meta">'
            +                   '<div class="title h5">'
            +                       '<a href=""><b>' + data.firstName + '&nbsp'+ data.lastName + '</b></a>'
            +                   '</div>'
            +                   '<h6 class="text-muted time"> ' + data.postingTime + '</h6>'
            +             '</div>'
            +        '</div> '
            +       '<div class="post-description">'
            +           '<p>' + data.textContent + '</p>'
            +            '<div class="post-description">'
            +                '<img src="' + data.photos + '" class="img-responsive">' 
            +            '<div class="stats">'
            +               '<a href="#" class="btn btn-default stat-item">'
            +                   '<i class="fa fa-thumbs-up icon"></i>2'
            +               '</a>'
            +               '<a href="#" class="btn btn-default stat-item">'
            +                   '<i class="fa fa-thumbs-down icon"></i>comment'
            +               '</a>'
            +               '<a href="#" class="btn btn-default stat-item">'
            +                   '<i class="fa fa-thumbs-down icon"></i>share'
            +               '</a>'
            +          '</div>'
            +          '</div>'
            +       '</div>'
            +     '</div>'
    },
	getTimeline: function() {
		// add loader
		$.ajax({
  			url: "/api/1.0/getTimeline/3",
  			method: "GET",
  			dataType : 'JSON',
            success  : function(response) {
            	if(response.reasonCode) { // if +ve result
            		//populate timeline
                    var html = "<div>";
                    $.each(response.data, function(index, value) {
                        html += timeline.timelineDiv(value);
                    });
                    $('#timeline').append(html);
            	} else { // if fail
            		// error handling
            		console.log("error");
            	} 
            },
            error: function(response) {
            	//handle error
            },
            complete: function(response) {
            	// remove loader
            }
		});
	},
	init: function() {
		timeline.getTimeline();
	}
}

$(function(){
	timeline.init();
});


// getting friends list

var friends = {
    friendsListDiv: function(data) {
        return      '<div class="col-md-6">' 
            +           '<div class="col-md-2">'
            +              '<img src="' + data.pic + '" class="img-circle" width="60px">'
            +           '</div>'
            +           '<div class="col-sm-6">'
            +               '<h4><a href="#">'+ data.firstName + '&nbsp'+ data.lastName +'</a></h4>'
            +                   '<p><a href="#">4 mutual friends</a></p>'
            +            '</div>'
            +            '<div class="col-sm-2">'
            +               '<a href="#">' + data.status + '</a>'
            +            '</div>'
            +         '</div>'
    },
    getFriends: function() {
        // add loader
        $.ajax({
            url: "/api/1.0/getFriends/3",
            method: "GET",
            dataType : 'JSON',
            success  : function(response) {
                if(response.reasonCode) { // if +ve result
                    //populate timeline
                    var html = "<div>";
                    $.each(response.data, function(index, value) {
                        html += friends.friendsListDiv(value);
                    });
                    $('#friendsList').append(html);
                    console.log(response.data);
                } else { // if fail
                    // error handling
                    console.log("error");
                } 
            },
            error: function(response) {
                //handle error
            },
            complete: function(response) {
                // remove loader
            }
        });
    },
    init: function() {
        friends.getFriends();
    }
}

$(function(){
    friends.init();
});

// get friends who are recently added
var recentFriends = {
    recentFriendsListDiv: function(data) {
        return      '<div class="col-md-6">' 
            +           '<div class="col-md-2">'
            +              '<img src="' + data.pic + '" class="img-circle" width="60px">'
            +           '</div>'
            +           '<div class="col-sm-6">'
            +               '<h4><a href="#">'+ data.firstName + '&nbsp'+ data.lastName +'</a></h4>'
            +                   '<p><a href="#">4 mutual friends</a></p>'
            +            '</div>'
            +            '<div class="col-sm-2">'
            +               '<a href="#">' + data.status + '</a>'
            +            '</div>'
            +         '</div>'
    },
    getRecentFriends: function() {
        // add loader
        $.ajax({
            url: "/api/1.0/getRecentFriends/3",
            method: "GET",
            dataType : 'JSON',
            success  : function(response) {
                if(response.reasonCode) { // if +ve result
                    //populate timeline
                    var html = "<div>";
                    $.each(response.data, function(index, value) {
                        html += recentFriends.recentFriendsListDiv(value);
                    });
                    $('#recentFriendsList').append(html);
                    console.log(response.data);
                } else { // if fail
                    // error handling
                    console.log("error");
                } 
            },
            error: function(response) {
                //handle error
            },
            complete: function(response) {
                // remove loader
            }
        });
    },
    init: function() {
        recentFriends.getRecentFriends();
    }
}

$(function(){
    recentFriends.init();
});


var friendsPic = {
    friendsPicDiv: function(data) {
        return          '<div class="photos"> '
            +               '<img  src="' + data.pic + '">'
            +          '</div>'
    },
    getFriendsPic: function() {
        // add loader
        $.ajax({
            url: "/api/1.0/getFriends/3",
            method: "GET",
            dataType : 'JSON',
            success  : function(response) {
                if(response.reasonCode) { // if +ve result
                    //populate timeline
                    var html = "<div>";
                    $.each(response.data, function(index, value) {
                        html += friendsPic.friendsPicDiv(value);
                    });
                    $('#friendsPic').append(html);
                    console.log(response.data);
                } else { // if fail
                    // error handling
                    console.log("error");
                } 
            },
            error: function(response) {
                //handle error
            },
            complete: function(response) {
                // remove loader
            }
        });
    },
    init: function() {
        friendsPic.getFriendsPic();
    }
}

$(function(){
    friendsPic.init();
});

var timelineImages = {
    timelineImagesListDiv: function(data) {
        return      '<div class="photos"> '
            +             '<img  src="' + data.photos + '">'
            +        '</div>'
    },
    gettimelineImages: function() {
        // add loader
        $.ajax({
            url: "/api/1.0/getTimeline/3",
            method: "GET",
            dataType : 'JSON',
            success  : function(response) {
                if(response.reasonCode) { // if +ve result
                    //populate timeline
                    var html = "<div>";
                    $.each(response.data, function(index, value) {
                        html += timelineImages.timelineImagesListDiv(value);
                    });
                    $('#timelineImages').append(html);
                    console.log(response.data);
                } else { // if fail
                    // error handling
                    console.log("error");
                } 
            },
            error: function(response) {
                //handle error
            },
            complete: function(response) {
                // remove loader
            }
        });
    },
    init: function() {
        timelineImages.gettimelineImages();
    }
}
$(function(){
    timelineImages.init();
});

var photosList = {
    photosListDiv: function(data) {
        return          '<div class="box">'
            +               '<a href="#" data-toggle="modal" data-target="#1" >'
            +                   '<img src="'+ data.photos + '">'
            +               '</a>'
            +               '<div class="modal fade" id="1" tabindex="-1" role="dialog">'
            +                   '<div class="modal-dialog" role="document">'
            +                      '<div class="modal-content">'
            +                           '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>'
            +                           '<div class="modal-body">'
            +                                '<img src="'+ data.photos + '">'
            +                           '</div>'
            +                           '<div class="col-md-12 description">'
            +                               '<h4>This is the first one on my Gallery</h4>'
            +                           '</div>'
            +                      '</div>'
            +                   '</div>'
            +               '</div>'
            +            '</div>'
    },
    getphotosList: function() {
        // add loader
        $.ajax({
            url: "/api/1.0/getTimeline/3",
            method: "GET",
            dataType : 'JSON',
            success  : function(response) {
                if(response.reasonCode) { // if +ve result
                    //populate timeline
                    var html = "<div>";
                    $.each(response.data, function(index, value) {
                        html += photosList.photosListDiv(value);
                    });
                    $('#photosList').append(html);
                    console.log(response.data);
                } else { // if fail
                    // error handling
                    console.log("error");
                } 
            },
            error: function(response) {
                //handle error
            },
            complete: function(response) {
                // remove loader
            }
        });
    },
    init: function() {
        photosList.getphotosList();
    }
}
$(function(){
    photosList.init();
});




var profilePic = {
    profileDiv: function(data) {
        return      '<div class="cover-photo">'
            +           '<img align="left" class="cover-image-lg" src="' + data.coverPic + ' " alt="cover photo"/  >'
            +       '</div>'
            +       '<div class="p-image1">'
            +          '<i class="fa fa-camera upload-button1"></i>'
            +             '<input class="file-upload1" type="file" accept="image/*"/>'
            +       '</div>'
            +       '<div class="clearfix"> </div>'
            +       '<div class="col-md-4"> </div>'
            +       '<div class="col-md-4">'   
            +           '<div class="circle">'
            +               '<img class="profile-pic" src="' + data.profilePic + ' ">' 
            +           '</div>'
            +           '<div class="p-image">'
            +               '<i class="fa fa-camera upload-button"></i>'
            +               '<input class="file-upload" type="file" accept="image/*"/>'
            +           '</div>'
            +           '<div class="cover-photo-text text-center">'
            +               '<h1>' + data.firstName + '&nbsp'+ data.lastName + '</h1>'
            +                   '<p>' +data.workingIn + '</p>'
            +                   '<p> Went to '  + data.graduation + '</p>'  
            +                   '<p> Lives in  ' + data.currentCity + '</p>'
            +           '</div>'
            +       '</div>'
    },
    getProfilePic: function() {
        // add loader
        $.ajax({
            url: "/api/1.0/user/3",
            method: "GET",
            dataType : 'JSON',
            success  : function(response) {
                if(response.reasonCode) { // if +ve result
                    //populate timeline
                    var html = "<div>";
                    $.each(response.data, function(index, value) {
                        html += profilePic.profileDiv(value);
                    });
                    $('#profileDetails').append(html);
                } else { // if fail
                    // error handling
                    console.log("error");
                } 
            },
            error: function(response) {
                //handle error
            },
            complete: function(response) {
                // remove loader
            }
        });
    },
    init: function() {
        profilePic.getProfilePic();
    }
}

$(function(){
    profilePic.init();
});


// var profileInfo = {
//     profileInfoDiv: function(data) {
//         return          '<div class="col-md-12">'
//             +               '<div class="about_title">'
//             +                   '<i class="fa fa-user" aria-hidden="true"></i> <span> About</span>'
//             +               '</div>'
//             +               '<div class="about_tab">'
//             +                   '<button class="tablinks" onclick="openCity(event, 'personalInfo')" id="defaultOpen">Contact And Basic info</button>'
//             +                   '<button class="tablinks" onclick="openCity(event, 'professionalInfo')">Work and Education</button>'
//             +                   '<button class="tablinks" onclick="openCity(event, 'familyInfo')">Family and Relationships</button>'
//             +               '</div>'
//             +               '<div id="personalInfo" class="about_content">'
//             +                   '<div class="col-md-12 personal-info">'
//             +                       '<h3>Personal info</h3>'
//             +                       '<form class="form-horizontal" role="form">'
//             +                           '<div class="form-group">'
//             +                               'div class="col-md-1"> </div>'
//             +                                   '<label class="col-lg-3 control-label">First name:</label>'
//             +                               '<div class="col-lg-6">'
//             +                                   '<input class="form-control" type="text" value="' + data.firstName + '">'
//             +                               '</div>'
//             +                           '</div>'
//             +                           '<div class="form-group">'
//             +                               '<div class="col-md-1"> </div>'
//             +                                   '<label class="col-lg-3 control-label">Last name:</label>'
//             +                               '<div class="col-lg-6">'
//             +                                        ' <input class="form-control" type="text" value="Bishop">'  
//             +                               '</div>'
//             +                            '</div>'
//             +                            '<div class="form-group">'
//             +                               '<div class="col-md-1"> </div>'
//             +                                   '<label class="control-label col-lg-3" for="date">Date Of birth</label>' 
//             +                               '<div class="col-lg-6">'
//             +                                    '<input class="form-control" id="date" name="date" placeholder="MM/DD/YYY" type="text"/>'  
//             +                               '</div>'
//             +                           '</div>'
//             +                           '<div class="form-group">'
//             +                               '<div class="col-md-1"> </div>'
//             +                                   ' <label class="control-label col-lg-3" for="date">Gender</label>'
//             +                               '<div class="col-lg-6"> '
//             +                                   '<label class="radio-inline">'
//             +                                       '<input type="radio" name="optradio">Male'
//             +                                   '</label>'
//             +                                   '<label class="radio-inline">'
//             +                                        '<input type="radio" name="optradio">Female'
//             +                                   '</label>'
//             +                               '</div>'
//             +                           '</div>'
//             +                           '<div class="form-group">'
//             +                                '<div class="col-md-1"> </div>'
//             +                                   '<label class="col-lg-3 control-label">Mobile No:</label>'
//             +                                '<div class="col-lg-6">'
//             +                                      '<input class="form-control" type="text" value="">'
//             +                                '</div>'
//             +                           '</div>'
//             +                           '<div class="form-group">'
//             +                               '<label class="col-md-3 control-label"></label>'
//             +                                   '<div class="col-md-8">'
//             +                                       '<input type="button" class="btn btn-primary" value="Save Changes">'
//             +                                       '<span></span>'
//             +                                        ' <input type="reset" class="btn btn-default" value="Cancel">'
//             +                                   '</div>'
//             +                           '</div>'
//             +                       '</form>'
//             +                   '</div>'
//             +               '</div>'
// }
// }

// <div id="professionalInfo" class="about_content">'
//  <div class="col-md-12 personal-info">'
   
//   <h3>Work</h3>'
//   <form class="form-horizontal" role="form">'
//     <div class="form-group">'
//       <div class="col-md-1"> </div>'
//       <label class="col-lg-3 control-label">Working In</label>'
//       <div class="col-lg-6">'
//         <input class="form-control" type="text" value="Jane">'
//       </div>'
//     </div>'
//     <div class="form-group">'
//      <div class="col-md-1"> </div>'
//      <label class="col-lg-3 control-label">Highschool</label>'
//      <div class="col-lg-6">'
//       <input class="form-control" type="text" value="Bishop">'
//     </div>'
//   </div>'
//   <div class="form-group">'
//    <div class="col-md-1"> </div>'
//    <label class="col-lg-3 control-label">Graduation</label>'
//    <div class="col-lg-6">'
//     <input class="form-control" type="text" value="">'
//   </div>'
// </div>'
// <div class="form-group">'
//  <div class="col-md-1"> </div>'
//  <label class="col-lg-3 control-label">Current City</label>'
//  <div class="col-lg-6">'
//   <input class="form-control" type="text" value="">'
// </div>'
// </div>'
// <div class="form-group">'
//  <div class="col-md-1"> </div>'
//  <label class="col-lg-3 control-label">Home Town</label>'
//  <div class="col-lg-6">'
//   <input class="form-control" type="text" value="">'
// </div>'
// </div>'
// <div class="form-group">'
//   <label class="col-md-3 control-label"></label>'
//   <div class="col-md-8">'
//     <input type="button" class="btn btn-primary" value="Save Changes">'
//     <span></span>'
//     <input type="reset" class="btn btn-default" value="Cancel">'
//   </div>'
// </div>'
// </form>'
// </div>'
// </div>'

// <div id="familyInfo" class="about_content">'
//   <div class="col-md-12 personal-info">'
   
//     <h3>Family</h3>'
//     <form class="form-horizontal" role="form">'
//       <div class="form-group">'
//         <div class="col-md-1"> </div>'
//         <label class="col-lg-3 control-label">Relationship Status</label>'
//         <div class="col-lg-6">'
//           <input class="form-control" type="text" value="Jane">'
//         </div>'
//       </div>'
//       <div class="form-group">'
//        <div class="col-md-1"> </div>'
//        <label class="col-lg-3 control-label">Family Members</label>'
//        <div class="col-lg-6">'
//         <input class="form-control" type="text" value="Bishop">'
//       </div>'
//     </div>'
//     <div class="form-group">'
//       <label class="col-md-3 control-label"></label>'
//       <div class="col-md-8">'
//         <input type="button" class="btn btn-primary" value="Save Changes">'
//         <span></span>'
//         <input type="reset" class="btn btn-default" value="Cancel">'
//       </div>'
//     </div>'

//   </form>'
// </div>'
// div>'
//         + '</div>'
//     },
//     getProfileInfo() {
//         // add loader
//         $.ajax({
//             url: "/api/1.0/user/3",
//             method: "GET",
//             dataType : 'JSON',
//             success  : function(response) {
//                 if(response.reasonCode) { // if +ve result
//                     //populate timeline
//                     var html = "<div>";
//                     $.each(response.data, function(index, value) {
//                         html += profileInfo.profileInfoDiv(value);
//                     });
//                     $('#profile').append(html);
//                 } else { // if fail
//                     // error handling
//                     console.log("error");
//                 } 
//             },
//             error: function(response) {
//                 //handle error
//             },
//             complete: function(response) {
//                 // remove loader
//             }
//         });
//     },
//     init: function() {
//         profileInfo.getProfileInfo();
//     }
// }

// $(function(){
//     profileInfo.init();
// });


