<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use App\Http\Controllers\Controller;
use App\Events\ChatEvent;
use Illuminate\Facades\Auth;
use JWTAuth;
use App\Model\User;

class ChatController extends Controller
{
   /**
     * Create a new controller instance.
     *
     * @return void
     */
  public function __construct()
  { 
    $name = \Illuminate\Support\Facades\Cookie::get('laravel_session');
      //$this->middleware('auth');
  }


  public function chat(){
   	return view('chat');
  }

  public function send(request $request, $id){
    $user = User::findOrFail($id);
    $this->saveToSession($request);
    event(new ChatEvent($request->message,$user));
  }
   
  public function saveToSession(request $request)
  {
    //print_r(session('chat'));
    session()->put('chat',$request->chat);
  }

  public function getOldMessage(){
    //print_r(session('chat'));
    return session('chat');
  }

  public function deleteSession()
  {
    session()->forget('chat');
  }
}
