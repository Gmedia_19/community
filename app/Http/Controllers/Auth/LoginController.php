<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\API\BaseController;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use App\Model\User;
use Auth;
use JWTAuth;


class LoginController extends BaseController
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home1';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
      
        $socialize_user = Socialite::driver($provider)->stateless()->user();
        $facebook_user_id = $socialize_user->getId();
        $user = User::where('provider_id', $facebook_user_id)->first();
        try{
            if (!$user) {
                $user = new User;
                $user->provider_id = $facebook_user_id;
                $user->provider = $provider;
                $user->first_name = $socialize_user->getName();
                //$user->email_id = $facebook_user_id;
                $user->email_id = $socialize_user->getEmail();
                $user->password = $facebook_user_id . 'gmedia';
                //$user->email_id = (new Token())->UniqueString('user', 'email_id', 9) . '@gmail.com';
                $user->is_verified = '1';
                $user->save();
            }
            $user->user_id;
            //login
            //Auth::loginUsingId($user->user_id);
            $token = JWTAuth::fromUser($user);
            //$user = JWTAuth::user();
                $profilePic = $user->profilePic ? config('app.url') . '/images/' . $user->profilePic->pic : null;
                $coverPic = $user->coverPic ? config('app.url') . '/images/' . $user->coverPic->cover_pic : null;
            return response()->json([
                'response' => 'success',
                'result' => [
                    'token' => $token,
                    'token_type' => 'bearer',
                    'expires_in' => JWTAuth::factory()->getTTL() * 480,
                    'userData' => [
                        'userId' => $user['user_id'],
                        'emailId' => $user['email_id'],
                        'firstName' => $user['first_name'],
                        'lastName' => $user['last_name'],
                        'username' => $user['username'],
                        'dob' => $user['dob'],
                        'gender' => $user['gender'],
                        'userPic' => $profilePic,
                        'coverPic' => $coverPic
                    ]
                ]
            ]);
        }
        catch (\Illuminate\Database\QueryException $ex){
            $errorCode = $ex->errorInfo[1];
            if($errorCode == '1062'){
                return $this->respondWithError("Duplicate Entry");
            }
            else return $this->respondWithError($ex->getMessage());
        }       
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }    
    }
}
