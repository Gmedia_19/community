<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController;
use App\Model\CareerTalent;

class CareerTalentController extends BaseController
{
   function addProfile(Request $request) {
        $data = $request->get('data');
        
        // $validator = \Validator::make($request->all(), [
        //     'data.cover_pic' => 'required',
        // ]);

        // if($validator->fails()){
        //     return $this->respondWithValidationFail($validator->errors()->messages());
        // }

        $file_name = 'image_' . time() . '.png';
        $file_path = public_path() . '/images/' . $file_name;
        if($data['image'] != "") {
            file_put_contents($file_path, base64_decode($data['image']));
        }

        $file_name1 = 'resume_' . time() . '.png';
        $file_path = public_path() . '/images/' . $file_name1;
         if(isset($data['resume']) && !empty($data['resume']) ? $data['resume'] : null) {
            file_put_contents($file_path, base64_decode($data['resume']));
        }

        $fatherName = 
    		isset($data['fatherName']) && !empty($data['fatherName']) ? $data['fatherName'] : null;
    	$industryType = 
    		isset($data['industryType']) && !empty($data['industryType']) ? $data['industryType'] : null;	 
    	$contactPesron = 
    		isset($data['contactPesron']) && !empty($data['contactPesron']) ? $data['contactPesron'] : null;
    	$hiringFor = 
    		isset($data['hiringFor']) && !empty($data['hiringFor']) ? $data['hiringFor'] : null;
    	$applyingFor = 
    		isset($data['applyingFor']) && !empty($data['applyingFor']) ? $data['applyingFor'] : null;
    	$jobExperience = 
    		isset($data['jobExperience']) && !empty($data['jobExperience']) ? $data['jobExperience'] : null;
    	$previousCompany = 
    		isset($data['previousCompany']) && !empty($data['previousCompany']) ? $data['previousCompany'] : null;	
    	//$file_name1 = 
    	 	// isset($data['resume']) && !empty($data['resume']) ? $data['resume'] : null;	  	 	 	 
        try{
            $profile = new CareerTalent;
            $profile->user_id =$data['userId'];
            $profile->profile_for =$data['profileFor'];
            $profile->name =$data['name'];
            $profile->father_name = $fatherName;
            $profile->email_id =$data['emailId'];
            $profile->mobile_number =$data['mobileNumber'];
            $profile->image = is_null($data['image']) || empty($data['image']) ? null : $file_name;
            $profile->industry_type =$industryType;
            $profile->contact_person = $contactPesron;
            $profile->address = $data['address'];
            $profile->hiring_for = $hiringFor;
            $profile->applying_for = $applyingFor;
            $profile->keywords = $data['keywords'];
            $profile->job_experience = $jobExperience;
            $profile->previous_company = $previousCompany;
            $profile->resume = is_null($data['resume']) || empty($data['resume']) ? null : $file_name1;
            $profile->status = 'active';
            $profile->save();

            return $this->respondWithSuccess('Details added');
        } 
    
        catch (\Illuminate\Database\QueryException $ex){
            $errorCode = $ex->errorInfo[1];
            if($errorCode == '1062'){
             return $this->respondWithError("Duplicate emailId or Mobile Number");
            }
            else return $this->respondWithError($ex->getMessage());
        }       
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    }

    function editProfile(Request $request, $id) {
        
        $data = $request->get('data');

        $file_name = 'image_' . time() . '.png';
        $file_path = public_path() . '/images/' . $file_name;
        if($data['image'] != "") {
            file_put_contents($file_path, base64_decode($data['image']));
        }

        $file_name1 = 'resume_' . time() . '.png';
        $file_path = public_path() . '/images/' . $file_name1;
         if(isset($data['resume']) && !empty($data['resume']) ? $data['resume'] : null) {
            file_put_contents($file_path, base64_decode($data['resume']));
        }

        $fatherName = 
            isset($data['fatherName']) && !empty($data['fatherName']) ? $data['fatherName'] : null;
        $industryType = 
            isset($data['industryType']) && !empty($data['industryType']) ? $data['industryType'] : null;   
        $contactPesron = 
           isset($data['contactPesron']) && !empty($data['contactPesron']) ? $data['contactPesron'] : null;
        $hiringFor = 
            isset($data['hiringFor']) && !empty($data['hiringFor']) ? $data['hiringFor'] : null;
        $applyingFor = 
            isset($data['applyingFor']) && !empty($data['applyingFor']) ? $data['applyingFor'] : null;
        $jobExperience = 
           isset($data['jobExperience']) && !empty($data['jobExperience']) ? $data['jobExperience'] : null;
        $previousCompany = 
           isset($data['previousCompany']) && !empty($data['previousCompany']) ? $data['previousCompany'] : null; 

        try{
            $profile = CareerTalent::findOrFail($id);
            $profile->user_id =$data['userId'];
            $profile->profile_for =$data['profileFor'];
            $profile->name =$data['name'];
            $profile->father_name = $fatherName;
            $profile->email_id =$data['emailId'];
            $profile->mobile_number =$data['mobileNumber'];
            $profile->image = is_null($data['image']) || empty($data['image']) ? null : $file_name;
            $profile->industry_type =$industryType;
            $profile->contact_person = $contactPesron;
            $profile->address = $data['address'];
            $profile->hiring_for = $hiringFor;
            $profile->applying_for = $applyingFor;
            $profile->keywords = $data['keywords'];
            $profile->job_experience = $jobExperience;
            $profile->previous_company = $previousCompany;
            $profile->resume = is_null($data['resume']) || empty($data['resume']) ? null : $file_name1;
            $profile->status = 'active';
            $profile->save();

            return $this->respondWithSuccess('Details Updated');
        } 
        catch (\Illuminate\Database\QueryException $ex){
            $errorCode = $ex->errorInfo[1];
            if($errorCode == '1062'){
                return $this->respondWithError("Duplicate emailId or Mobile Number");
            }
            else return $this->respondWithError($ex->getMessage());
        }       
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    }


    function getJobSeeker() {

    $response=[];

        try{
            $profiles = CareerTalent::select('id', 'user_id', 'profile_for', 'name', 'father_name', 'email_id', 'mobile_number', 'image', 'industry_type', 'address', 'applying_for', 'keywords', 'job_experience', 'previous_company', 'resume')
              ->where('profile_for', '=', 'student')
              ->orderBy('id', 'DESC')
              ->where('status', '=', 'active')
              ->get();

            foreach ($profiles as $index => $profile) {
              array_push($response, [
              	"id" => $profile['id'],
                "userId" => $profile['user_id'],
                "profileFor" => $profile['profile_for'],
                "name" => $profile['name'],
                "fatherName" => $profile['father_name'],
                "mobileNumber" => $profile['mobile_number'],
                "emailId" => $profile['email_id'],
                "industryType" => $profile['industry_type'],
                "address" => $profile['address'],
                "applyingFor" => $profile['applying_for'],
                "keywords" => $profile['keywords'],
                "jobExperience" => $profile['job_experience'],
                "previousCompany" => $profile['previous_company'],
                "image" => is_null($profile['image']) || empty($profile['image']) ? "" : config('app.url') . '/images/' .$profile['image'],
                "resume" => is_null($profile['resume']) || empty($profile['resume']) ? "" : config('app.url') . '/images/' .$profile['resume']
              ]);
            } 
            return $this->respondWithSuccess($response);
        } 
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }   
    }

  function getCompany() {

        $response=[];

        try{
        	$profiles = CareerTalent::select('id', 'user_id', 'profile_for', 'name', 'email_id', 'mobile_number', 'image', 'industry_type', 'address', 'contact_person', 'hiring_for', 'keywords', 'job_experience')
            ->where('profile_for', '=', 'company')
            ->orderBy('id', 'DESC')
            ->where('status', '=', 'active')
            ->get();

            foreach ($profiles as $index => $profile) {
              array_push($response, [
              	"id" => $profile['id'],
           		"userId" => $profile['user_id'],
                "profileFor" => $profile['profile_for'],
                "name" => $profile['name'],
                "mobileNumber" => $profile['mobile_number'],
                "emailId" => $profile['email_id'],
                "industryType" => $profile['industry_type'],
                "address" => $profile['address'],
                "contactPesron" => $profile['contact_person'],
                "hiringFor" => $profile['hiring_for'],
                "keywords" => $profile['keywords'],
                "jobExperience" => $profile['job_experience'],
                "image" => is_null($profile['image']) || empty($profile['image']) ? "" : config('app.url') . '/images/' .$profile['image']
              ]);
            } 
           	return $this->respondWithSuccess($response);
        }
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    }

  function getCareerDetails($user_id) {

        $response=[];

        try{
        	$profiles = CareerTalent::select('id', 'user_id', 'profile_for', 'name', 'email_id', 'father_name', 'mobile_number', 'image', 'industry_type', 'address', 'contact_person', 'hiring_for', 'applying_for', 'keywords', 'job_experience', 'previous_company', 'resume')
            ->where('id', '=', $user_id)
            ->orderBy('id', 'DESC')
            ->where('status', '=', 'active')
            ->get();

            foreach ($profiles as $index => $profile) {
              array_push($response, [
              	"id" => $profile['id'],
           		"userId" => $profile['user_id'],
                "profileFor" => $profile['profile_for'],
                "name" => $profile['name'],
                "mobileNumber" => $profile['mobile_number'],
                "emailId" => $profile['email_id'],
                "industryType" => $profile['industry_type'],
                "contactPesron" => $profile['contact_person'],
                "address" => $profile['address'],
                "hiringFor" => $profile['hiring_for'],
                "applyingFor" => $profile['applying_for'],
                "keywords" => $profile['keywords'],
                "jobExperience" => $profile['job_experience'],
                "previousCompany" => $profile['previous_company'],
                "image" => is_null($profile['image']) || empty($profile['image']) ? "" : config('app.url') . '/images/' .$profile['image'],
                "resume" => is_null($profile['resume']) || empty($profile['resume']) ? "" : config('app.url') . '/images/' .$profile['resume'],
              ]);
            } 
           	return $this->respondWithSuccess($response);
        }
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    }

    function deleteCarrerAndTalent(Request $request, $id){

        try{
          $b2b = CareerTalent::findOrFail($id);
          $b2b->status = 'deleted';
          $b2b->save();
          return $this->respondWithSuccess('successfully deleted');
        } 
        catch (\Exception $exception){
          return $this->respondWithError($exception->getMessage());
        } 
    }
}
