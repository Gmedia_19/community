<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController;
use App\Model\Temple;
use App\Model\TempleEvent;


class TempleController extends BaseController
{
    function addTemple(Request $request) {
        $data = $request->get('data');

        $file_name = 'temple_' . time() . '.png';
        $file_path = public_path() . '/images/' . $file_name;
        if($data['image'] != "") {
            file_put_contents($file_path, base64_decode($data['image']));
        }
        try{
            $temple = new Temple();
            $temple->user_id = $data['userId'];
            $temple->temple_name = $data['templeName'];
            $temple->location = $data['location'];
            $temple->gods_name = $data['godsName'];
            $temple->about = $data['about'];
            $temple->history = $data['history'];
            $temple->contact_number = $data['contactNumber'];
            $temple->trust = $data['trust'];
            $temple->members = $data['members'];
            $temple->image = is_null($data['image']) || empty($data['image']) ? null : $file_name;
            $temple->status = 'active';

            $temple->save();
            return $this->respondWithSuccess('temple added');
            }
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    }

    function editTemple(Request $request, $id) {
        $data = $request->get('data');

        $file_name = 'temple_' . time() . '.png';
        $file_path = public_path() . '/images/' . $file_name;
        if($data['image'] != "") {
            file_put_contents($file_path, base64_decode($data['image']));
        }
        try{
            $temple = Temple::findOrFail($id);
            $temple->user_id = $data['userId'];
            $temple->temple_name = $data['templeName'];
            $temple->location = $data['location'];
            $temple->gods_name = $data['godsName'];
            $temple->about = $data['about'];
            $temple->history = $data['history'];
            $temple->contact_number = $data['contactNumber'];
            $temple->trust = $data['trust'];
            $temple->members = $data['members'];
            $temple->image = is_null($data['image']) || empty($data['image']) ? null : $file_name;
            $temple->status = 'active';

            $temple->save();
            return $this->respondWithSuccess('Field Updated');
        }
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    }  

    function getTemples() {

        $response=[];
        try{
            $temples = Temple::select('id', 'user_id', 'temple_name', 'location', 'gods_name', 'about', 'history', 'contact_number', 'trust', 'members', 'image')
            ->where('status', '=', 'active')
            ->get();

            foreach ($temples as $index => $temple) {
                array_push($response, [
                    "templeId" => $temple['id'],
                    "userId" => $temple['user_id'],
                    "templeName" => $temple['temple_name'],
                    "location" =>$temple['location'],
                    "godsName" => $temple['gods_name'],
                    "about" => $temple['about'],
                    "history" => $temple['history'],
                    "contactNumber" => $temple['contact_number'],
                    "trust" => $temple['trust'],
                    "members" => $temple['members'],
                    "image" =>is_null($temple['image']) || empty($temple['image']) ? "" : config('app.url') . '/images/' . $temple['image'] 
                ]);
            } 
            return $this->respondWithSuccess($response);
       }
       catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    }

  function getTemple($id) {

        $response=[];
        try{
            $temples = Temple::select('id', 'user_id', 'temple_name', 'location', 'gods_name', 'about', 'history', 'contact_number', 'trust', 'members', 'image')
            ->where('id', '=', $id)
            ->where('status', '=', 'active')
            ->get();

            foreach ($temples as $index => $temple) {
                 array_push($response, [
                    "templeId" => $temple['id'],
                    "userId" => $temple['user_id'],
                    "templeName" => $temple['temple_name'],
                    "location" =>$temple['location'],
                    "godsName" => $temple['gods_name'],
                    "about" => $temple['about'],
                    "history" => $temple['history'],
                    "contactNumber" => $temple['contact_number'],
                    "trust" => $temple['trust'],
                    "members" => $temple['members'],
                    "image" =>is_null($temple['image']) || empty($temple['image']) ? "" : config('app.url') . '/images/' . $temple['image']  
                ]);
            } 
            return $this->respondWithSuccess($response);
        }
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    }

    function deleteTemple(Request $request, $id){

        try{
          $b2b = Temple::findOrFail($id);
          $b2b->status = 'deleted';
          $b2b->save();
          return $this->respondWithSuccess('Temple deleted');
        } 
        catch (\Exception $exception){
          return $this->respondWithError($exception->getMessage());
        } 
    }

    function addTempleEvent(Request $request) {
        $data = $request->get('data');

        $file_name = 'temple_' . time() . '.png';
        $file_path = public_path() . '/images/' . $file_name;
        if($data['image'] != "") {
            file_put_contents($file_path, base64_decode($data['image']));
        }
        try{
            $temple = new TempleEvent();
            $temple->user_id = $data['userId'];
            $temple->temple_id = $data['templeId'];
            $temple->event_name = $data['eventName'];
            $temple->event_detail = $data['eventDetail'];
            $temple->date_time = $data['dateTime'];
            $temple->image = is_null($data['image']) || empty($data['image']) ? null : $file_name;
            $temple->status = 'active';

            $temple->save();
            return $this->respondWithSuccess('event added');
        }
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    }

    function editTempleEvent(Request $request, $id) {
        $data = $request->get('data');

        $file_name = 'temple_' . time() . '.png';
        $file_path = public_path() . '/images/' . $file_name;
        if($data['image'] != "") {
            file_put_contents($file_path, base64_decode($data['image']));
        }
        try{
            $temple = TempleEvent::findOrFail($id);
            $temple->user_id = $data['userId'];
            $temple->temple_id = $data['templeId'];
            $temple->event_name = $data['eventName'];
            $temple->event_detail = $data['eventDetail'];
            $temple->date_time = $data['dateTime'];
            $temple->image = is_null($data['image']) || empty($data['image']) ? null : $file_name;
            $temple->status = 'active';

            $temple->save();
            return $this->respondWithSuccess('event updated');
        }
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    }

    function getTempleEvent($id) {

        $response=[];
        try{
            $temples = TempleEvent::select('id', 'user_id', 'temple_id', 'event_name', 'event_detail', 'date_time', 'image')
            ->where('temple_id', '=', $id)
            ->where('status', '=', 'active')
            ->get();

            foreach ($temples as $index => $temple) {
                 array_push($response, [
                    "eventId" => $temple['id'],
                    "userId" => $temple['user_id'],
                    "templeId" => $temple['temple_id'],
                    "eventName" => $temple['event_name'],
                    "eventDetail" => $temple['event_detail'],
                    "dateTime" => $temple['date_time'],
                    "image" =>is_null($temple['image']) || empty($temple['image']) ? "" : config('app.url') . '/images/' . $temple['image']
                ]);
            } 
            return $this->respondWithSuccess($response);
        }
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    }

    function getSpecificTempleEvent($id) {

        $response=[];
        try{
            $temples = TempleEvent::select('id', 'user_id', 'temple_id', 'event_name', 'event_detail', 'date_time', 'image')
            ->where('id', '=', $id)
            ->where('status', '=', 'active')
            ->get();

            foreach ($temples as $index => $temple) {
                 array_push($response, [
                    "eventId" => $temple['id'],
                    "userId" => $temple['user_id'],
                    "templeId" => $temple['temple_id'],
                    "eventName" => $temple['event_name'],
                    "eventDetail" => $temple['event_detail'],
                    "dateTime" => $temple['date_time'],
                    "image" =>is_null($temple['image']) || empty($temple['image']) ? "" : config('app.url') . '/images/' . $temple['image']  
                ]);
            } 
            return $this->respondWithSuccess($response);
        }
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    }

    function deleteTempleEvent(Request $request, $id){

        try{
          $b2b = TempleEvent::findOrFail($id);
          $b2b->status = 'deleted';
          $b2b->save();
          return $this->respondWithSuccess('Event deleted');
        } 
        catch (\Exception $exception){
          return $this->respondWithError($exception->getMessage());
        } 
    }  
}
