<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController;
use Illuminate\Support\Facades\Input;
use App\Model\NewsEvents;

class NewsEventsController extends BaseController
{
   function addNews(Request $request) {

        $data = $request->get('data');
         
        $file_name = 'image_' . time() . '.png';
        $file_path = public_path() . '/images/' . $file_name;
        if($data['photos'] != "") {
            file_put_contents($file_path, base64_decode($data['photos']));
        }

        $content2 = 
          isset($data['content2']) && !empty($data['content2']) ? $data['content2'] : null;

        try{
            $news = new NewsEvents;
            $news->category =$data['category'];
            $news->heading =$data['heading'];
            if ($data['photos']==null) {
                $news->photos = null;
            }else{
                $news->photos = $file_name;
            }
            $news->content1 = $data['content1'];
            $news->content2 = $content2;
            $news->status = 'active';
            $news->save();

            
            return $this->respondWithSuccess('News added');
        }
        catch (\Exception $exception){
         return $this->respondWithError($exception->getMessage());
        }
    }

    function editNews(Request $request, $id) {

        $data = $request->get('data');

        $file_name = 'image_' . time() . '.png';
        $file_path = public_path() . '/images/' . $file_name;
        if($data['photos'] != "") {
            file_put_contents($file_path, base64_decode($data['photos']));
        }

        $content2 = isset($data['content2']) && !empty($data['content2']) ? $data['content2'] : null;

        try{
            $news = NewsEvents::findOrFail($id);
            $news->category =$data['category'];
            $news->heading =$data['heading'];
            if ($data['photos']==null) {
                $news->photos = null;
            }else{
                $news->photos = $file_name;
            }
            $news->content1 = $data['content1'];
            $news->content2 = $data['content2'];
            $news->status = 'active';
            $news->save();
            
            return $this->respondWithSuccess('News added');
        }
        catch (\Exception $exception){
         return $this->respondWithError($exception->getMessage());
        }
    }    

    function getNews() {

        $response=[];

        try{
            $news = NewsEvents::select(
                    'id', 'category', 'heading', 'photos', 'created_at')
            ->where('status', '=', 'active')
            ->orderBy('news_events.created_at', 'desc')
            ->get();

            foreach ($news as $index => $new) {
              array_push($response, [
                    "id" => $new['id'],
                    "category" => $new['category'],
                    "heading" => $new['heading'],
                    "photos" =>  is_null($new['photos']) || empty($new['photos']) ? "" : config('app.url') . '/images/' .$new['photos'],
                    //"photos" => config('app.url') . '/images/' .$new['photos'],
                    //"dateTime" =>$new['created_at']->format('Y-m-d h:m')
                ]);
            } 
            return $this->respondWithSuccess($response);
        }
        catch (\Exception $exception){
         return $this->respondWithError($exception->getMessage());
        }
    }    

    function getCategoryNews($news) {

        $response=[];
        try{
            $news = NewsEvents::select(
                    'id', 'category', 'heading', 'photos','content1', 'content2' )
                ->where('category', '=', $news)
                ->where('status', '=', 'active')
                ->orderBy('news_events.created_at', 'desc')
                ->get();

            foreach ($news as $index => $new) {
              array_push($response, [
                    "id" => $new['id'],
                    "category" => $new['category'],
                    "heading" => $new['heading'],
                    "photos" =>  is_null($new['photos']) || empty($new['photos']) ? "" : config('app.url') . '/images/' .$new['photos'],
                    "content1" =>$new['content1'],
                    "content2" =>$new['content2']   
                ]);
            } 
            return $this->respondWithSuccess($response);
        }
        catch (\Exception $exception){
         return $this->respondWithError($exception->getMessage());
        }    
    }

    function getSpecificNews($id) {

        $response=[];

        try{
            $news = NewsEvents::select(
                    'id', 'category', 'heading', 'photos','content1', 'content2' )
            	->where('id', '=', $id)
                ->where('status', '=', 'active')
            	->get();

            foreach ($news as $index => $new) {
              array_push($response, [
                    "id" => $new['id'],
                    "category" => $new['category'],
                    "heading" => $new['heading'],
                    "photos" =>  is_null($new['photos']) || empty($new['photos']) ? "" : config('app.url') . '/images/' .$new['photos'],
                    "content1" =>$new['content1'],
                    "content2" =>$new['content2']	
                ]);
            } 
            return $this->respondWithSuccess($response);
        }
        catch (\Exception $exception){
         return $this->respondWithError($exception->getMessage());
        }
    }
    
    function deleteNews(Request $request, $id){

        try{
          $b2b = NewsEvents::findOrFail($id);
          $b2b->status = 'deleted';
          $b2b->save();
          return $this->respondWithSuccess('Product deleted');
        } 
        catch (\Exception $exception){
          return $this->respondWithError($exception->getMessage());
        } 
    }
}
