<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\API\BaseController;
use App\Model\Friends;
use DB;

class FriendController extends BaseController
{
  function sendRequest(Request $request) {
    
    $data = $request->get('data');
    
    $results = Friends::select()->where([['user_id', $data['userId']], ['friend_id', $data['friendId']]])
    ->orWhere([['user_id', $data['friendId']], ['friend_id', $data['userId']]])
    ->count();

    if($results == 1){
     return $this->respondWithError('u both are already friend');
    }
    else{
      try{
        $friend = new Friends();
        $friend->user_id = $data['userId'];
        $friend->friend_id = $data['friendId'];
        $friend->status = 'requesting';
        $friend->save();
        return $this->respondWithSuccess('Request Sent');
      }
      catch (\Exception $ex){
        return $this->respondWithError($ex->getMessage());
      }
    }  
  }

  function getRequest($id) {
    $response=[];
    try{
      $friends = Friends::select('user.first_name as firstName', 'user.last_name as lastName', 'friends.id as id', 'friends.user_id as userId', 'friends.friend_id as friendId', 'user_pic.pic as profilePic')
      ->join('user', 'friends.user_id', '=', 'user.user_id')
      ->leftjoin('user_pic','user.user_pic_id', '=', 'user_pic.pic_id')
      ->where('friends.friend_id', '=', $id)
      ->where('friends.status', 'like', 'req' . '%')
      ->get();
      foreach ($friends as $index => $friend) {
        array_push($response, [
          "id" => $friend['id'],
          "userId" => $friend['userId'],
          "friendId" => $friend['friendId'],
          "firstName" => $friend['firstName'],
          "lastName" => $friend['lastName'],
          "profilePic" => is_null($friend['profilePic']) || empty($friend['profilePic']) ? config('app.url') . '/images/default_pic.jpg' : config('app.url') . '/images/' . $friend['profilePic'] 
        ]);
      } 
      return $this->respondWithSuccess($response);
    }
    catch (\Exception $exception){
      return $this->respondWithError($exception->getMessage());
    }
  }

  function sentRequest($id) {
    $response=[];
    try{
      $friends = Friends::select('user.first_name as firstName', 'user.last_name as lastName', 'friends.id as id', 'friends.user_id as userId', 'friends.friend_id as friendId', 'user_pic.pic as profilePic')
      ->join('user', 'friends.friend_id', '=', 'user.user_id')
      ->leftjoin('user_pic','user.user_pic_id', '=', 'user_pic.pic_id')
      ->where('friends.user_id', '=', $id)
      ->where('friends.status', 'like', 'req' . '%')
      ->get();

      foreach ($friends as $index => $friend) {
        array_push($response, [
          "id" => $friend['id'],
          "userId" => $friend['userId'],
          "friendId" => $friend['friendId'],
          "firstName" => $friend['firstName'],
          "lastName" => $friend['lastName'],
          "profilePic" => is_null($friend['profilePic']) || empty($friend['profilePic']) ? config('app.url') . '/images/default_pic.jpg' : config('app.url') . '/images/' . $friend['profilePic'] 
        ]);
      } 
      return $this->respondWithSuccess($response);
    }
    catch (\Exception $exception){
      return $this->respondWithError($exception->getMessage());
    }
  }

  // function cancelFriendRequest(Request $request, $id){
  //   $data = $request->get('data');

  //   try{
  //     $friend = Friends::findOrFail($id);
  //     $friend->status = 'cancelled';
  //     $friend->save();
  //     return $this->respondWithSuccess('Request Removed');
  //   } 
  //   catch (\Exception $exception){
  //     return $this->respondWithError($exception->getMessage());
  //   } 
  // }

  function acceptFriendRequest(Request $request, $id){

    try{
      $friend = Friends::findOrFail($id);
      $friend->status = 'friends';
      $friend->friends_from = date('Y-m-d H:i:s', strtotime('+5 hours +30 minutes'));
      $friend->save();
      return $this->respondWithSuccess('You both are now fiends');
    } 
    catch (\Exception $exception){
      return $this->respondWithError($exception->getMessage());
    } 
  }

  function getFriends($user_id) {
    $response=[];

    try{

     $friends = DB::select("SELECT u.first_name as firstName, u.last_name as lastName, f.id as id, u.user_id as friendId, up.pic as profilePic, f.status as status
      FROM friends AS f 
      LEFT JOIN user AS u ON f.friend_id = u.user_id
      LEFT JOIN  user_pic AS up ON u.user_pic_id = up.pic_id
      WHERE f.user_id = $user_id AND f.status = 'friends'

      UNION

      SELECT u.first_name as firstName, u.last_name as lastName, f.id as id, u.user_id as friendId, up.pic as profilePic , f.status as status
      FROM friends AS f
      LEFT JOIN user AS u ON f.user_id = u.user_id
      LEFT JOIN  user_pic AS up ON u.user_pic_id = up.pic_id
      WHERE  f.friend_id = $user_id AND f.status = 'friends'");

      foreach ($friends as $index => $profile) {
        array_push($response, [
          "id" => $profile->id,
          "friendId" => $profile->friendId,
          "firstName" => $profile->firstName,
          "lastName" => $profile->lastName,
          "status" => $profile->status,
          "profilePic" => is_null($profile->profilePic) || empty($profile->profilePic) ? config('app.url') . '/images/default_pic.jpg' : config('app.url') . '/images/' . $profile->profilePic
        ]);
      } 
     
      return $this->respondWithSuccess($response);
    }
    catch (\Exception $exception){
      return $this->respondWithError($exception->getMessage());
    }
  }


  function getFriendSuggestion($user_id) {
    $response=[];

    try{

      $friends = DB::select("SELECT u.user_id as friendId, u.first_name as firstName, u.last_name as lastName, up.pic as profilePic
      FROM user As u 
      LEFT JOIN  user_pic AS up ON u.user_pic_id = up.pic_id
      WHERE u.user_id NOT IN (
        SELECT u.user_id
        FROM friends AS f 
        LEFT JOIN user AS u ON f.friend_id = u.user_id
        WHERE f.user_id = $user_id

        UNION

        SELECT u.user_id
        FROM friends AS f 
        LEFT JOIN user AS u ON f.user_id = u.user_id
        WHERE  f.friend_id = $user_id
      )
      AND u.status = 'active'
      AND u.user_id != $user_id");

      foreach ($friends as $index => $profile) {
        array_push($response, [
          "friendId" => $profile->friendId,
          "firstName" => $profile->firstName,
          "lastName" => $profile->lastName,
          "profilePic" => is_null($profile->profilePic) || empty($profile->profilePic) ? config('app.url') . '/images/default_pic.jpg' : config('app.url') . '/images/' . $profile->profilePic
        ]);
      } 
     
      return $this->respondWithSuccess($response);
    }
    catch (\Exception $exception){
      return $this->respondWithError($exception->getMessage());
    }
  }

  function getFriendStatus($user_id, $friend_id) {

    try{
      $users = Friends::select('id as id', 'user_id as userId', 'friend_id as friendId', 'status')
      ->where([['user_id', $user_id], ['friend_id', $friend_id]])
      ->orWhere([['user_id', $friend_id], ['friend_id', $user_id]])
      ->get();
      return $this->respondWithSuccess($users);
    }
    catch (\Exception $exception){
      return $this->respondWithError($exception->getMessage());
    }
  } 

  function deleteFriend($id) {
    try{
      $todo = Friends::findOrFail($id);

      $todo->delete();
      return $this->respondWithSuccess('No more Friends');
    }
    catch (\Exception $exception){
      return $this->respondWithError($exception->getMessage());
    }
  }  
}
