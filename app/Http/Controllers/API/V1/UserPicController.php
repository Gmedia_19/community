<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController;
use Illuminate\Support\Facades\Input;
use App\Model\User_Pic;
use App\Model\PicComment;
use App\Model\User;
use App\Model\UserPicLike;

class UserPicController extends BaseController
{
    function updateProfilePic(Request $request) {
        
        $data = $request->get('data');

        $validator = \Validator::make($request->all(), [
            'data.pic' => 'required',
        ]);

        if($validator->fails()){
            return $this->respondWithValidationFail($validator->errors()->messages());
        }

        $file_name = 'image_' . time() . '.png';
        $file_path = public_path() . '/images/' . $file_name;
        if($data['pic'] != "") {
            file_put_contents($file_path, base64_decode($data['pic']));
        }
        
        try{
            $pic = new User_Pic;
            $pic->user_id =$data['userId'];
            $pic->pic = $file_name;
            $pic->status = 'active';
            $pic->save();

            $pic->pic_id;
            
            //updating column user_pic_id of user table 

            $user = User::find($pic->user_id);
            $user->user_pic_id = $pic->pic_id;
            $user->save();
            
            return $this->respondWithSuccess("pic added");
        }
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    }
    
    function getAllProfilePic($user_id) {
       
       $response=[];
       try{
           $pics = User_Pic::select(['pic_id', 'user_id', 'pic'])
            ->where('user_id', '=', $user_id)
            ->where('status', '=', 'active')
            ->get();
                            
            foreach ($pics as $index => $pic) {
                array_push($response, [
                    "picId" => $pic['pic_id'],
                    "userId" => $pic['user_id'],
                    "pic" =>config('app.url') . '/images/' . $pic['pic']
                ]);
            } 
             
            return $this->respondWithSuccess($response);
        }
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    }

    function deleteProfilePic(Request $request, $id){
        try{
          $friend = User_Pic::findOrFail($id);
          $friend->status = 'deleted';
          $friend->save();
          return $this->respondWithSuccess('Profile image Deleted');
        } 
        catch (\Exception $exception){
          return $this->respondWithError($exception->getMessage());
        } 
    }

    function profilePicComment(Request $request) {
        $data = $request->get('data');
        try{
            $comment = new PicComment();
            $comment->pic_id = $data['picId'];
            $comment->user_id = $data['userId'];
            $comment->comment = $data['comment'];
            $comment->status = 'active';

            $comment->save();
            return $this->respondWithSuccess('Comment added');
        }
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    }

    function editProfilePicComment(Request $request, $pic_comment_id) {
        $data = $request->get('data');
        try{
            $comment = PicComment::findOrFail($pic_comment_id);
            $comment->pic_comment_id = $data['commentId'];
            $comment->pic_id = $data['picId'];
            $comment->user_id = $data['userId'];
            $comment->comment = $data['comment'];
            $comment->status = 'active';

            $comment->save();
            return $this->respondWithSuccess('Comment Updated');
        }
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    }  

    function getPicComment($pic_id) {
       
       $response=[];
       try{
            $comments = PicComment::select(['user.first_name as firstName', 'user.last_name as lastName','userpic_comment.pic_comment_id as commentId', 'userpic_comment.pic_id as picId', 'userpic_comment.user_id as userId', 'userpic_comment.comment as comment', 
                'userpic_comment.created_at as postingTime', 'profile.pic as profilePic', 'pic.pic as pic'])
            ->join('user', 'userpic_comment.user_id', '=', 'user.user_id')
            ->join('user_pic as pic' , 'userpic_comment.pic_id', '=', 'pic.pic_id')
            ->leftjoin('user_pic as profile','user.user_pic_id', '=', 'profile.pic_id')
            ->where('userpic_comment.pic_id', '=', $pic_id)
            ->where('userpic_comment.status', '=', 'active')
            ->get();

            foreach ($comments as $index => $comment) {
                array_push($response, [
                    "commentId" => $comment['commentId'],
                    "picId" => $comment['picId'],
                    "userId" => $comment['userId'],
                    "firstName" => $comment['firstName'],
                    "lastName" => $comment['lastName'],
                    "profilePic" =>  is_null($comment['profilePic']) || empty($comment['profilePic']) ? config('app.url') . '/images/default_pic.jpg' : config('app.url') . '/images/' .$comment['profilePic'],
                    "pic" => is_null($comment['profilePic']) || empty($comment['profilePic']) ? config('app.url') . '/images/default_pic.jpg' :  config('app.url') . '/images/' .$comment['pic'],
                    "comment" =>$comment['comment'],
                    "postingTime" =>$comment['postingTime']
                ]);
            } 
            return $this->respondWithSuccess($response);
        }
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    }

    function deleteProfilePicComment($pic_comment_id) {

        try{
            $comment = PicComment::findOrFail($pic_comment_id);
            $comment->status = 'deleted';
            $comment->save();
            return $this->respondWithSuccess('Comment Deleted');
        } 
        catch (\Exception $exception){
          return $this->respondWithError($exception->getMessage());
        } 
    }

    function profilePicLike(Request $request) {

        $data = $request->get('data');
        try{
            $user = UserPicLike::where([['user_id', $data['userId']], ['pic_id', $data['picId']]])->first();
            if (!is_null($user) && $user->status == 'liked'){
                $user->status = 'unliked';
                $user->save();
                return $this->respondWithSuccess('pic unLiked');
            }
            else if(!is_null($user) && $user->status == 'unliked'){
                $user->status = 'liked';
                $user->save();
                return $this->respondWithSuccess('pic Liked');
            }
            else{
                $user = new UserPicLike();
                $user->pic_id = $data['picId'];
                $user->user_id = $data['userId'];
                $user->status = 'liked';
                $like->save();
                return $this->respondWithSuccess('pic Liked');
            }
        }catch(\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    }

    function countProfilePicLike($id) {
        
        try{
            $wordlist = UserPicLike::where('pic_id', '=', $id)
            ->where('status', '=', 'liked')
            ->count();
            return $this->respondWithSuccess($wordlist);
        }
         catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    }

    function getNameOfProfilePic($pic_id) {
       
       $response=[];
       try{
            $comments = UserPicLike::select(['user.first_name as firstName', 'user.last_name as lastName', 'userpic_like.user_id as userId', 'userpic_like.pic_id as picId'])
            ->join('user', 'userpic_like.user_id', '=', 'user.user_id')
            ->where('userpic_like.pic_id', '=', $pic_id)
            ->where('userpic_like.status', '=', 'liked')
            ->get();

            foreach ($comments as $index => $comment) {
                array_push($response, [
                    "picId" => $comment['picId'],
                    "userId" => $comment['userId'],
                    "firstName" => $comment['firstName'],
                    "lastName" => $comment['lastName']
                ]);
            } 
            return $this->respondWithSuccess($response);
        }
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    }
}
