<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\API\BaseController;
use App\Http\Middleware\ThrottleRequestsMiddleware;
use Illuminate\Support\Facades\Hash;
use Mail;
use Dirape\Token\Token;
use Auth;

use App\Model\User;
use App\Model\LastName;

class UserController extends BaseController
{  

    function registration(Request $request) {
         
        $validator = \Validator::make($request->all(), [
            'data.firstName' => 'required|string',
            'data.lastName' => 'required|string',
            'data.emailId' => 'required|email|unique:user,email_id',
            'data.password' => 'required',
            'data.confirmPassword' => 'required|same:data.password'
        ]);

        if($validator->fails()){
            return $this->respondWithValidationFail($validator->errors()->messages());
        }

        try{
            $data = $request->get('data');
            $user = new User;
            $user->first_name =$data['firstName'];
            $user->last_name = $data['lastName'];
            $user->email_id = $data['emailId'];
            $user->password = Hash::make($data['password']);
            $user->status = 'active';
            //$user->username = $user->first_name .(new Token())->UniqueNumber('user', 'username', 5); 
            $user->save();
            $user->user_id;

            $data = array('name'=> $user->first_name, 'userId' => $user->user_id, 'link' => config('app.url') . '/api/1.0/verify/'. $user->user_id );
            
            Mail::send('emails.mail', $data, function($message) use ($user){
                $message->to($user->email_id, 'Daivajnya Brahmin')
                ->subject('Verify email address');
                $message->from('noreply@daivajnyabrahmin.com','Daivajnya Brahmin');
            });
            
            return $this->respondWithSuccess('Thanks for signing up! Please check your email to complete your registration');
        }
        catch (\Illuminate\Database\QueryException $ex){
            $errorCode = $ex->errorInfo[1];
            if($errorCode == '1062'){
                return $this->respondWithError("Duplicate Entry");
            }
            else return $this->respondWithError($ex->getMessage());
        }
        catch (\App\Http\Middleware\ThrottleRequestsMiddleware $exc){
            return $this->respondWithException($exc->getMessage());
        }       
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    } 

    function lastName() {

        $response=[];
        try{
            $lastName = LastName::select('last_name')
            ->get();
            
            return $this->respondWithSuccess($lastName);
        }
       catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    }

    function addUserDetails(Request $request, $user_id) {
         
        $validator = \Validator::make($request->all(), [
            'data.firstName' => 'required|string',
            'data.lastName' => 'required|string',
            //'data.emailId' => 'required|email|unique:user,email_id,'.$user->id.',user_id',
            'data.emailId' => 'required|email',
            'data.password' => 'required',
            'data.alternateEmailId' =>'email',
            'data.dob' => 'date',
            'data.gender' => 'required|string',
           // 'data.mobileNumber' => 'required|regex:/^[6789]\d{9}$/|unique:user,mobile_number',
            //'data.mobileNumber' => 'regex:/^[6789]\d{9}$/', 
            'data.alternateMobileNumber' => 'regex:/^[6789]\d{9}$/', 
            'data.workingIn' => 'string',
            'data.highSchool' => 'string',
            'data.graduation' => 'string',
            'data.currentCity' => 'string',
            'data.permanentCity' => 'string'
        ]);
        if($validator->fails()){
            return $this->respondWithValidationFail($validator->errors()->messages());
        }

        $data = $request->get('data');
        $lastName = 
          isset($data['lastName']) && !empty($data['lastName']) ? $data['lastName'] : null;

        $alternateEmailId = 
            isset($data['alternateEmailId'])
            && !empty($data['alternateEmailId']) ? $data['alternateEmailId'] : null;

        $alternateMobileNumber = 
            isset($data['alternateMobileNumber'])
            && !empty($data['alternateMobileNumber']) ? $data['alternateMobileNumber'] : null;

        $workingIn = 
          isset($data['workingIn']) && !empty($data['workingIn']) ? $data['workingIn'] : null;

        $highSchool = 
          isset($data['highSchool']) && !empty($data['highSchool']) ? $data['highSchool'] : null;

        $graduation = 
          isset($data['graduation']) && !empty($data['graduation']) ? $data['graduation'] : null;

        $currentCity = 
          isset($data['currentCity']) && !empty($data['currentCity']) ? $data['currentCity'] : null;
          
        $permanentCity = 
          isset($data['permanentCity']) && !empty($data['permanentCity']) ? $data['permanentCity'] : null;
        try{  
            // $user_id = Auth::user()->user_id;
            $user = User::findOrFail($user_id);
            //$user->username =$data['username'];
            $user->first_name =$data['firstName'];
            $user->last_name = $lastName;
            $user->dob = $data['dob'];
            $user->gender = $data['gender'];
            $user->mobile_number = $data['mobileNumber'];
            $user->email_id = $data['emailId'];
            $user->alternate_mobile_number =$alternateMobileNumber;
            $user->alternate_email_id =$alternateEmailId;
            $user->password = $data['password'];
            $user->working_in =$workingIn;
            $user->high_school = $highSchool;
            $user->graduation = $graduation;
            $user->current_city = $currentCity;
            $user->permanent_city = $permanentCity;

            $user->save();
            return $this->respondWithSuccess('User updated');
        }
        catch (\Illuminate\Database\QueryException $ex){
            $errorCode = $ex->errorInfo[1];
            if($errorCode == '1062'){
                return $this->respondWithError("Duplicate Entry");
            }
            else return $this->respondWithError($ex->getMessage());
        } 

        catch (\App\Http\Middleware\ThrottleRequestsMiddleware $exc){
            return $this->respondWithException($exc->getMessage());
        }           
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }

    } 

    function getUser($user_id) {

        $response=[];

        try{
            $users = User::select(
            'user.user_id as userId', 'user.first_name as firstName' ,'user.last_name as lastName','user.username as username', 'user.mobile_number as mobileNumber', 'user.graduation as graduation', 'user.dob as dob','user.email_id as emailId', 'user.working_in as workingIn', 'user.high_school as highSchool','user.current_city as currentCity', 
            'user.permanent_city as permanentCity','user.gender as gender', 
            'user_pic.pic as profilePic', 'cover_pic.cover_pic as coverPic')

            ->leftjoin('user_pic', 'user.user_pic_id', '=', 'user_pic.pic_id')
            ->leftjoin('cover_pic', 'user.cover_pic_id', '=', 'cover_pic.cover_id')
            ->where('user.user_id', '=', $user_id)
            ->get();

            foreach ($users as $index => $user) {
              array_push($response, [
                    "userId" => $user['userId'],
                    "firstName" => $user['firstName'],
                    "lastName" => $user['lastName'],
                    "username" => $user['username'],
                    "mobileNumber" => $user['mobileNumber'],
                    "emailId" => $user['emailId'],
                    "workingIn" => $user['workingIn'],
                    "highSchool" => $user['highSchool'],
                    "graduation" => $user['graduation'],
                    "currentCity" => $user['currentCity'],
                    "permanentCity" => $user['permanentCity'],
                    "gender" => $user['gender'],
                    "dob" => $user['dob'],
                    "profilePic" =>is_null($user['profilePic']) || empty($user['profilePic']) ? config('app.url') . '/images/default_pic.jpg' : config('app.url') . '/images/' . $user['profilePic'],
                    "coverPic" =>is_null($user['coverPic']) || empty($user['coverPic']) ? config('app.url') . '/images/default_cover_pic2.jpg' :  config('app.url') . '/images/' . $user['coverPic'],
                ]);
            } 
            return $this->respondWithSuccess($response);
        }
        catch (\App\Http\Middleware\ThrottleRequestsMiddleware $exc){
            return $this->respondWithException($exc->getMessage());
        }     
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    } 
    

    function getAllUser() {

        $response=[];
        try{
            $users = User::select(
            'user.user_id as userId', 'user.first_name as firstName' ,'user.last_name as lastName',
            'user.email_id as emailId', 'user.mobile_number as mobileNumber', 'user_pic.pic as profilePic'
            )
            ->leftjoin('user_pic', 'user.user_pic_id', '=', 'user_pic.pic_id')
            ->leftjoin('cover_pic', 'user.cover_pic_id', '=', 'cover_pic.cover_id')
            ->get();
            
            foreach ($users as $index => $user) {
              array_push($response, [
                    "userId" => $user['userId'],
                    "firstName" => $user['firstName'],
                    "lastName" => $user['lastName'],
                    "mobileNumber" => $user['mobileNumber'],
                    "emailId" => $user['emailId'],
                    "profilePic" =>is_null($user['profilePic']) || empty($user['profilePic']) ? config('app.url') . '/images/default_pic.jpg' : config('app.url') . '/images/' . $user['profilePic'],
                ]);
            } 
            return $this->respondWithSuccess($response);
        }
        catch (App\Http\Middleware\ThrottleRequestsMiddleware $exc){
            return $this->respondWithException($exc->getMessage());
        } 
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        } 
    }
}
