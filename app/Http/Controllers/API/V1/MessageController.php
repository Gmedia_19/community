<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\API\BaseController;
use App\Model\Message;

class MessageController extends BaseController
{
    function sendMessage(Request $request) {
     	$data = $request->get('data');
        $file_name = 'image_' . time() . '.png';
        $file_path = public_path() . '/images/' . $file_name;
        if($data['photos'] != "") {
            file_put_contents($file_path, base64_decode($data['photos']));
        }    
        $message = new Message();
        $message->sender_id = $data['senderId'];
        $message->recipient_id = $data['recipientId'];
        $message->photos = $file_name;
        $message->message = $data['message'];
        $message->sender_status = 'send';
        $message->recipient_status = 'received';

        $message->save();
        return $this->respondWithSuccess('Message send');
    }   

    function getMessage($sender_id, $recipient_id) {
       
       $response=[];

       $messages = Message::select(['message.sender_id as senderId', 'message.recipient_id as recipientId', 'message.message as message', 'message.created_at as time', 'message.photos as photos'])
       
       // select `message`.`sender_id` as `senderId`, `message`.`recipient_id` as `recipientId`, `message`.`message` as `message`, `message`.`created_at` as `time`, `message`.`photos` as `photos` from `message` where (`message`.`sender_id` = 2 or `message`.`recipient_id` = 2) and  (`message`.`recipient_id`= 3 or `message`.`sender_id` = 3)
        
        ->where ('message.sender_id', '=', $sender_id)
        ->orWhere ('message.recipient_id', '=', $recipient_id)
        //->get();
        
        ->toSql();
        dd($messages);
        print_r($messages); exit;

        foreach ($messages as $index => $message) {
            array_push($response, [
                "senderId" =>$message['senderId'],
                "recipientId" =>$message['recipientId'],
                "firstName" =>$message['firstName'],
                "lastName" => $message['lastName'],
                "message" =>  $message['message'],
                "photos" => config('app.url') . '/images/' .$message['photos'],
                "time" => $message['time']
            ]);
        } 
      
        return $this->respondWithSuccess($response);
    }
}
