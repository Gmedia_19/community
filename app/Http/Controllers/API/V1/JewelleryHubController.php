<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController;
use Illuminate\Support\Facades\Input;
use App\Model\JewelleryHub;
use App\Model\JewelleryImage;
use Symfony\Component\Debug\Exception\FatalThrowableError;


class JewelleryHubController extends BaseController
{
  function addJewelleryHubDetails(Request $request) {
        $data = $request->get('data');

        $validator = \Validator::make($request->all(), [
            'data.name' => 'string|max:99',
            'data.shopName' => 'string|max:99',
            'data.address' => 'string|max:190',
            'data.fatherName' => 'string|max:99',
            'data.quality' => 'string|max:99',
           // 'data.contactNumber' => 'required|regex:/^[6789]\d{9}$/',
            'data.content' => 'string|max:190',
            'data.jewelleryType' => 'string',
            'data.stockDetails' => 'required',
        ]);

        if($validator->fails()){
            return $this->respondWithValidationFail($validator->errors()->messages());
        }
        $name = 
          isset($data['name']) && !empty($data['name']) ? $data['name'] : null;  

        $fatherName = 
          isset($data['fatherName']) && !empty($data['fatherName']) ? $data['fatherName'] : null;

        $shopName = 
          isset($data['shopName']) && !empty($data['shopName']) ? $data['shopName'] : null;  
          
        $contactNumber = 
            isset($data['contactNumber']) && !empty($data['contactNumber']) ? $data['contactNumber'] : null;
        $homeNumber = 
          isset($data['homeNumber']) && !empty($data['homeNumber']) ? $data['homeNumber'] : null;

        $address = 
          isset($data['address']) && !empty($data['address']) ? $data['address'] : null;
          
        $content = 
          isset($data['content']) && !empty($data['content']) ? $data['content'] : null; 

        $quantity = 
          isset($data['quantity']) && !empty($data['quantity']) ? $data['quantity'] : null;

        $quality = 
          isset($data['quality']) && !empty($data['quality']) ? $data['quality'] : null;
        
        $jewelleryType = 
          isset($data['jewelleryType']) && !empty($data['jewelleryType']) ? $data['jewelleryType'] 
            : null; 
        $type = 
          isset($data['type']) && !empty($data['type']) ? $data['type'] : null;
        $stockDetails = 
          isset($data['stockDetails']) && !empty($data['stockDetails']) ? $data['stockDetails'] : null;
        
        $file_name = 'jhub_' . time() . '.png';
        $file_path = public_path() . '/images/' . $file_name;
        if($data['image'] != "") {
            file_put_contents($file_path, base64_decode($data['image']));
        }

        $file_name1 = 'jhub1_' . time() . '.png';
        $file_path = public_path() . '/images/' . $file_name1;
        if($data['image1'] != "") {
            file_put_contents($file_path, base64_decode($data['image1']));
        }

        $file_name2 = 'jhub2_' . time() . '.png';
        $file_path = public_path() . '/images/' . $file_name2;
        if($data['image2'] != "") {
            file_put_contents($file_path, base64_decode($data['image2']));
        }

        $file_name3 = 'jhub3_' . time() . '.png';
        $file_path = public_path() . '/images/' . $file_name3;
        if($data['image3'] != "") {
            file_put_contents($file_path, base64_decode($data['image3']));
        }

        try{
            $hub = new JewelleryHub;
            $hub->user_id = $data['userId'];
            $hub->name = $name;
            $hub->shop_name = $shopName;
            $hub->contact_number = $contactNumber;
            $hub->home_number = $homeNumber;
            $hub->address = $address;
            $hub->father_name = $fatherName;
            $hub->quantity = $quantity;
            $hub->quality = $quality;
            $hub->content = $content;
            $hub->image = is_null($data['image']) || empty($data['image']) ? null : $file_name;
            $hub->jewellery_type = $jewelleryType;
            $hub->type = $type;
            $hub->stock_details = $stockDetails;
            $hub->image1 = is_null($data['image1']) || empty($data['image1']) ? null : $file_name1;
            $hub->image2 = is_null($data['image2']) || empty($data['image2']) ? null : $file_name2;
            $hub->image3 = is_null($data['image3']) || empty($data['image3']) ? null : $file_name3;
            $hub->status = 'active';

            $hub->save();
            return $this->respondWithSuccess('Details added');
        }
        catch (\Illuminate\Database\QueryException $ex){
            $errorCode = $ex->errorInfo[1];
            if($errorCode == '1062'){
                return $this->respondWithError("Duplicate Entry");
            }
            else return $this->respondWithError($ex->getMessage());
        }       
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }      
    }

    function editJewelleryDetails(Request $request, $j_id) {
        $data = $request->get('data');

        $validator = \Validator::make($request->all(), [
            'data.name' => 'string|max:99',
            'data.shopName' => 'string|max:99',
            'data.address' => 'string|max:190',
            'data.fatherName' => 'string|max:99',
            'data.quality' => 'string|max:99',
            //'data.contactNumber' => 'required|regex:/^[6789]\d{9}$/',
            'data.content' => 'string|max:190',
            'data.jewelleryType' => 'string',
            'data.stockDetails' => 'required',
        ]);

        if($validator->fails()){
            return $this->respondWithValidationFail($validator->errors()->messages());
        }
        $name = 
          isset($data['name']) && !empty($data['name']) ? $data['name'] : null;  

        $fatherName = 
          isset($data['fatherName']) && !empty($data['fatherName']) ? $data['fatherName'] : null;

        $shopName = 
          isset($data['shopName']) && !empty($data['shopName']) ? $data['shopName'] : null;  
          
        $contactNumber = 
            isset($data['contactNumber']) && !empty($data['contactNumber']) ? $data['contactNumber'] : null;
         $homeNumber = 
          isset($data['homeNumber']) && !empty($data['homeNumber']) ? $data['homeNumber'] : null;

        $address = 
          isset($data['address']) && !empty($data['address']) ? $data['address'] : null;
          
        $content = 
          isset($data['content']) && !empty($data['content']) ? $data['content'] : null; 

        $quantity = 
          isset($data['quantity']) && !empty($data['quantity']) ? $data['quantity'] : null;

        $quality = 
          isset($data['quality']) && !empty($data['quality']) ? $data['quality'] : null;

        $image = 
          isset($data['image']) && !empty($data['image']) ? $data['image'] : null; 
          
        $jewelleryType = 
          isset($data['jewelleryType']) && !empty($data['jewelleryType']) ? $data['jewelleryType'] 
            : null; 
        $type = 
          isset($data['type']) && !empty($data['type']) ? $data['type'] : null;
        $stockDetails = 
          isset($data['stockDetails']) && !empty($data['stockDetails']) ? $data['stockDetails'] : null;

       $file_name = 'jhub_' . time() . '.png';
        $file_path = public_path() . '/images/' . $file_name;
        if($data['image'] != "") {
            file_put_contents($file_path, base64_decode($data['image']));
        }

        $file_name1 = 'jhub1_' . time() . '.png';
        $file_path = public_path() . '/images/' . $file_name1;
        if($data['image1'] != "") {
            file_put_contents($file_path, base64_decode($data['image1']));
        }

        $file_name2 = 'jhub2_' . time() . '.png';
        $file_path = public_path() . '/images/' . $file_name2;
        if($data['image2'] != "") {
            file_put_contents($file_path, base64_decode($data['image2']));
        }

        $file_name3 = 'jhub3_' . time() . '.png';
        $file_path = public_path() . '/images/' . $file_name3;
        if($data['image3'] != "") {
            file_put_contents($file_path, base64_decode($data['image3']));
        }

        
        try{
            $hub = JewelleryHub::findOrFail($j_id);
            $hub->j_id = $data['jId'];
            $hub->user_id = $data['userId'];
            $hub->name = $name;
            $hub->shop_name = $shopName;
            $hub->contact_number = $contactNumber;
            $hub->home_number = $homeNumber;
            $hub->address = $address;
            $hub->father_name = $fatherName;
            $hub->quantity = $quantity;
            $hub->quality = $quality;
            $hub->content = $content;
            $hub->image = is_null($data['image']) || empty($data['image']) ? null : $file_name;
            $hub->jewellery_type = $jewelleryType;
            $hub->type = $type;
            $hub->stock_details = $stockDetails;
            $hub->image1 = is_null($data['image1']) || empty($data['image1']) ? null : $file_name1;
            $hub->image2 = is_null($data['image2']) || empty($data['image2']) ? null : $file_name2;
            $hub->image3 = is_null($data['image3']) || empty($data['image3']) ? null : $file_name3;
            $hub->status = 'active';

            $hub->save();
            return $this->respondWithSuccess('Details Updated');
        }
        catch (\Illuminate\Database\QueryException $ex){
            $errorCode = $ex->errorInfo[1];
            if($errorCode == '1062'){
                return $this->respondWithError("Duplicate Entry");
            }
            else return $this->respondWithError($ex->getMessage());
        }       
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }      
    }      

    function getJewelleryByType($type) {

        $response=[];
        try{
        $hubs = JewelleryHub::select( 'j_id','user_id','name', 'shop_name', 'home_number', 'address', 'contact_number','quantity', 'quality', 'content', 'jewellery_type', 'stock_details', 'image')
    	->where('type', '=', $type)
        ->where('status', '=', 'active')
        ->orderBy('j_id', 'DESC')
    	->get();

        foreach ($hubs as $index => $hub) {
          array_push($response, [
                "jId" => $hub['j_id'],
                "userId" => $hub['user_id'],
                "name" => $hub['name'],
                "shopName" => $hub['shop_name'],
                "homeNumber" => $hub['home_number'],
                "address" => $hub['address'],
                "contactNumber" => $hub['contact_number'],
                "quantity" => $hub['quantity'],
                "quality" => $hub['quality'],
                "content" => $hub['content'],
                "jewelleryType" => $hub['jewellery_type'],
                "stockDetails" => $hub['stock_details'],
                "image" => is_null($hub['image']) || empty($hub['image']) ? "" : config('app.url') . '/images/' .$hub['image'],
            ]);
        } 
        return $this->respondWithSuccess($response);
        }
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }   
    }
    
    function myStock($id) {

        $response=[];
        
        try{
            $hubs = JewelleryHub::select(
                'j_id', 'user_id', 'name', 'shop_name','home_number', 'address', 'father_name', 'contact_number', 'quantity', 'quality', 'content', 'jewellery_type', 'stock_details', 'type', 'image', 'image1', 'image2', 'image3')
            ->where('user_id', '=', $id)
            ->where('status', '=', 'active')
            ->orderBy('j_id', 'DESC')
            ->get();

            foreach ($hubs as $index => $hub) {
              array_push($response, [
                    "jId" => $hub['j_id'],
                    "userId" => $hub['user_id'],
                    "name" => $hub['name'],
                    "shopName" => $hub['shop_name'],
                    "homeNumber" =>$hub['home_number'],
                    "address" => $hub['address'],
                    "fatherName" => $hub['father_name'],
                    "contactNumber" => $hub['contact_number'],
                    "quantity" => $hub['quantity'],
                    "quality" => $hub['quality'],
                    "content" => $hub['content'],
                    "jewelleryType" => $hub['jewellery_type'],
                    "type" => $hub['type'],
                    "stockDetails" => $hub['stock_details'],
                    "image" => is_null($hub['image']) || empty($hub['image']) ? "" : config('app.url') . '/images/' .$hub['image'],
                    "image1" => is_null($hub['image1']) || empty($hub['image1']) ? "" : config('app.url') . '/images/' .$hub['image1'],
                    "image2" => is_null($hub['image2']) || empty($hub['image2']) ? "" : config('app.url') . '/images/' .$hub['image2'],
                    "image3" => is_null($hub['image3']) || empty($hub['image3']) ? "" : config('app.url') . '/images/' .$hub['image3'],
                ]);
            } 
            return $this->respondWithSuccess($response);
        }
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }     
    }

    function getSpecificDetails($id) {

        $response=[];
        try{
            $hubs = JewelleryHub::select(
                'j_id', 'user_id', 'name', 'shop_name','home_number', 'address', 'father_name', 'contact_number', 'quantity', 'quality', 'content', 'jewellery_type', 'stock_details', 'type', 'image', 'image1', 'image2', 'image3')
                ->where('j_id', '=', $id)
                ->where('status', '=', 'active')
                ->orderBy('j_id', 'DESC')
                ->get();

            foreach ($hubs as $index => $hub) {
              array_push($response, [
                    "jId" => $hub['j_id'],
                    "userId" => $hub['user_id'],
                    "name" => $hub['name'],
                    "shopName" => $hub['shop_name'],
                    "homeNumber" =>$hub['home_number'],
                    "address" => $hub['address'],
                    "fatherName" => $hub['father_name'],
                    "contactNumber" => $hub['contact_number'],
                    "quantity" => $hub['quantity'],
                    "quality" => $hub['quality'],
                    "content" => $hub['content'],
                    "jewelleryType" => $hub['jewellery_type'],
                    "type" => $hub['type'],
                    "stockDetails" => $hub['stock_details'],
                    "image" => is_null($hub['image']) || empty($hub['image']) ? "" : config('app.url') . '/images/' .$hub['image'],
                    "image1" => is_null($hub['image1']) || empty($hub['image1']) ? "" : config('app.url') . '/images/' .$hub['image1'],
                    "image2" => is_null($hub['image2']) || empty($hub['image2']) ? "" : config('app.url') . '/images/' .$hub['image2'],
                    "image3" => is_null($hub['image3']) || empty($hub['image3']) ? "" : config('app.url') . '/images/' .$hub['image3'],
                ]);
            } 
            return $this->respondWithSuccess($response);
        }
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }      
    }

    function deleteJewellery(Request $request, $id){

        try{
          $hub = JewelleryHub::findOrFail($id);
          $hub->status = 'deleted';
          $hub->save();
          return $this->respondWithSuccess('successfully deleted');
        } 
        catch (\Exception $exception){
          return $this->respondWithError($exception->getMessage());
        } 
    } 

    function addJewelleryImage(Request $request)
    {
        $rows = $request->get('data');
       
        foreach ($rows as $file) {
            $file_name = 'jewellery_' . time() . '.png';
            $file_path = public_path() . '/images/' . $file_name;

            if($file['image'] != "") {
                file_put_contents($file_path, base64_decode($file['image']));
            }
        }
        
        foreach ($rows as $row)
        {
            $pic[] = [
                'user_id' => $row['user_id'],
                'image' => $file_name,
                'text' => $row['text'],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'status' => 'active',
            ];
        }
        
        JewelleryImage::insert($pic);
        return $this->respondWithSuccess("pic added");
    }            
}
