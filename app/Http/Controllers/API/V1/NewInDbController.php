<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController;
use App\Model\NewInDB;

class NewInDbController extends BaseController
{
   function postNewInDB(Request $request){

        $data = $request->get('data');
       
        $file_name = 'newdb_' . time() . '.png';
        $file_path = public_path() . '/images/' . $file_name;
         if(isset($data['image']) && !empty($data['image']) ? $data['image'] : null) {
            file_put_contents($file_path, base64_decode($data['image']));
        }
        try{
	        $pic = new NewInDB;
	        $pic->post = $data['post'];
	        $pic->image = is_null($data['image']) || empty($data['image']) ? null : $file_name;
	        $pic->status = 'active';
	        $pic->save();
	    	return $this->respondWithSuccess('Details added');
        } 
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    } 

    function getNewInDB() {
        $response=[];

        try{
            $users = NewInDB::select('id', 'post', 'image')
            ->where('status', '=', 'active')
            ->orderBy('id', 'DESC')
            ->paginate(1);

            foreach ($users as $index => $user) {
              array_push($response, [
                    "id" => $user['id'],
                    "post" => $user['post'],
                    "image" => is_null($user['image']) || empty($user['image']) ? "" : config('app.url') . '/images/' .$user['image'],
                ]);
            } 
            return $this->respondWithSuccess($response);
        }
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    } 
}
