<?php

namespace App\Http\Controllers\API\V1;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController;
use App\Http\Requests;
use JWTAuthException;
use App\Model\User;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Mail;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Auth\Authenticatable;


class ApiController extends BaseController
{
	/**
     * API Register
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */


    function login(Request $request){
        $requestData = $request->all();
        $credentials['email_id'] = $requestData['data']['emailId'];
        $credentials['password'] = $requestData['data']['password'];
        $credentials['is_verified'] = 1;

        $token = null;
        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json([
                    'response' => 'error',
                    'message' => 'invalid_email_or_password_or_verify_your_email_address',
                ]);
            }
        } catch (JWTAuthException $e) {
            return response()->json([
                'response' => 'error',
                'message' => 'failed_to_create_token',
            ]);
        }
        $user = JWTAuth::user();
        $profilePic = $user->profilePic ? config('app.url') . '/images/' . $user->profilePic->pic : config('app.url') . '/images/default_pic.jpg';
        $coverPic = $user->coverPic ? config('app.url') . '/images/' . $user->coverPic->cover_pic : config('app.url') . '/images/default_cover_pic2.jpg';
        return response()->json([
            'response' => 'success',
            'result' => [
                'token' => $token,
                'token_type' => 'bearer',
                'expires_in' => JWTAuth::factory()->getTTL() * 1920,
                'userData' => [
                    'userId' => $user['user_id'],
                    'emailId' => $user['email_id'],
                    'firstName' => $user['first_name'],
                    'lastName' => $user['last_name'],
                    'username' => $user['username'],
                    'dob' => $user['dob'],
                    'gender' => $user['gender'],
                    'userPic' => $profilePic,
                    'coverPic' => $coverPic
                ]
            ]
        ]);
    }
    
    function logout(Request $request) {
        
        $user = JWTAuth::toUser($request->token);  
        try {
            JWTAuth::invalidate($request->input('token'));
            return response()->json(['success' => true, 'message'=> "You have successfully logged out."]);
        }catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['success' => false, 'error' => 'Failed to logout, please try again.'], 500);
        }
    }

    function verifyUser(Request $request, $user_id)
    {
        if(!is_null($user_id)){
            $user = User::findOrFail($user_id);
            if($user->is_verified == 1){
                return response()->json([
                    'success'=> true,
                    'message'=> 'Account already verified..'
                ]);
            }
            $user->update(['is_verified' => 1]);
            return response()->json([
                'success'=> true,
                'message'=> 'You have successfully verified your email address.'
            ]);
        }
        return response()->json(['success'=> false, 'error'=> "Verification code is invalid."]);
    }

    function forgotPassword(Request $request){
       // $this->validate($request, ['email' => 'required|email|exists:users']);
        $data = $request->get('data');
        $user = User::whereemail_id($data['emailId'])->first();

        $dataa = array('name'=> $user->first_name, 'userId' => $user->user_id, 'link' => config('app.url') . '/api/1.0/resetPassword/'. $user->user_id );
        
        Mail::send('emails.forgotPassword', $dataa, function($message) use ($user){
            $message->to($user->email_id)
            ->subject('Reset your Password');
            $message->from('noreply@daivajnyabrahmin.com','Daivajnya Brahmin');
        });
        return $this->respondWithSuccess('Please check your email Id for resetting your password');
    }

    function resetPassword($user_id){

        $user = User::findOrFail($user_id);
        $dataa = array('userId' => $user->user_id );
        return view('emails.password', $dataa);
    }

    function postResetPassword(Request $request){

        $validator = \Validator::make($request->all(), [
            'password' => 'required',
            'confirmPassword' => 'required|same:password'
        ]);

        if($validator->fails()){
            return $this->respondWithValidationFail($validator->errors()->messages());
        }
       $rdata = $request->all();
       $user = User::whereuser_id($rdata['userId'])->first();
       $user->password = Hash::make($rdata['password']);
       $user->save();

        return view('emails.successResetPassword');
    }

    public function getAuthUser(Request $request){
        $user = JWTAuth::toUser($request->token);        
        return response()->json(['result' => $user]);
    }
}