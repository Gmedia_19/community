<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController;
use App\Model\MatchFound;
use App\Model\Matrimony;

class MatchFoundController extends BaseController
{
  function matchFound(Request $request) {
    
    $data = $request->get('data');
    
    $results = MatchFound::select()->where([['user_id', $data['groomId']], ['matched_id', $data['brideId']]])
    ->orWhere([['user_id', $data['brideId']], ['matched_id', $data['groomId']]])
    ->count();

    if($results == 1){
     return $this->respondWithError('your data is already with us..');
    }
    else{
      $file_name = 'image_' . time() . '.png';
      $file_path = public_path() . '/images/' . $file_name;
      if($data['image'] != "") {
        file_put_contents($file_path, base64_decode($data['image']));
      } 
      try{
        $match = new MatchFound();
        $match->user_id = $data['groomId'];
        $match->matched_id = $data['brideId'];
        $match->commitment_date = $data['commitmentDate'];
        $match->place = $data['place'];
        $match->feedback = $data['feedback'];
        $match->image = is_null($data['image']) || empty($data['image']) ? null : $file_name;
        $match->status = 'matched';
        $match->save();

        $comment = Matrimony::findOrFail($match->user_id);
        $comment->status = 'matched';
        $comment->save();

        $comment = Matrimony::findOrFail($match->matched_id);
        $comment->status = 'matched';
        $comment->save();

        return $this->respondWithSuccess('Thanku for your feedback. Wishing you a great future ahead');
      }
      catch (\Illuminate\Database\QueryException $ex){
        $errorCode = $ex->errorInfo[1];
        if($errorCode == '1062'){
          return $this->respondWithError("your data is already with us..");
        }
        else return $this->respondWithError($ex->getMessage());
      }    
      
      catch (\Exception $exception){
        return $this->respondWithError($exception->getMessage());
      }
    }  
  }

  function getMatchedFound() {

    $response=[];

    try{
      $matrimony = MatchFound::select(
      'match_found.user_id as groomId','match_found.matched_id as brideId', 'match_found.commitment_date as commitmentDate', 'match_found.place as place', 'match_found.feedback as feedback', 'match_found.image as image', 'matrig.first_name as firstNameGroom' ,'matrig.last_name as lastNameGroom', 'matrib.first_name as firstNameBride' ,'matrib.last_name as lastNameBride')
      ->leftjoin('matrimony as matrig', 'match_found.user_id', '=', 'matrig.id')
      ->leftjoin('matrimony as matrib', 'match_found.matched_id', '=', 'matrib.id')
      ->get();
      //print_r($users); exit;

      foreach ($matrimony as $index => $match) {
        array_push($response, [
          "groomId" => $match['groomId'],
          "brideId" => $match['brideId'],
          "firstNameGroom" => $match['firstNameGroom'],
          "lastNameGroom" => $match['lastNameGroom'],
          "firstNameBride" => $match['firstNameBride'],
          "lastNameBride" => $match['lastNameBride'],
          "commitmentDate" => $match['commitmentDate'],
          "place" => $match['place'],
          "feedback" => $match['feedback'],
          "image" => is_null($match['image']) || empty($match['image']) ? "" : config('app.url') . '/images/' . $match['image'],
        ]);
      } 
      return $this->respondWithSuccess($response);
    }
    catch (\Exception $exception){
      return $this->respondWithError($exception->getMessage());
    }
  } 
}
