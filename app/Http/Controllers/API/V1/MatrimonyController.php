<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController;
use Illuminate\Support\Facades\Input;
use App\Model\User;
use App\Model\Matrimony;
use App\Model\MatrimonyRequest;
use App\Model\MatrimonyBanner;
use DB;
use Response;

class MatrimonyController extends BaseController
{
  function addMatrimony(Request $request, $id) {

    $validator = \Validator::make($request->all(), [
      'data.maritalStatus' => 'bail|required|string|max:30',
      'data.profileFor' => 'bail|required|string',
      'data.firstName' => 'bail|required|string|max:50',
      'data.lastName' => 'bail|required|string|max:50',
      'data.gender' => 'required|string',
      'data.userPic' => 'required',
      'data.age' => 'required',
      'data.height' => 'required|string',
      'data.colour' => 'string',
      'data.dob' => 'required|date',
      'data.birthPlace' => 'required|string',
      'data.gothra' => 'string|max:30',
      'data.nakshatra' => 'string|max:30',
      'data.ganna' => 'string|max:20',
      'data.rashi' => 'string|max:30',
      'data.highestDegree' => 'required|string|max:90',
      'data.educationField' => 'string|max:90',
      'data.income' => 'string',
      'data.workingIn' => 'string',
      'data.workingAs' => 'string',
      'data.ambition' => 'string|max:200',
      'data.expectation' => 'string|max:150',
      'data.presentCity' => 'string|max:30',
      'data.permanentCity' => 'string|max:30',
      'data.country' => 'string|max:20',
      'data.fatherName' => 'required|string',
      'data.fatherProfession' => 'required|string',
      'data.motherName' => 'required|string',
      'data.motherProfession' => 'required|string'
    ]);

    if($validator->fails()){
      return $this->respondWithValidationFail($validator->errors()->messages());
    }
    $data = $request->get('data');

    $file_name = 'image_' . time() . '.png';
    $file_path = public_path() . '/images/' . $file_name;
    if($data['userPic'] != "") {
      file_put_contents($file_path, base64_decode($data['userPic']));
    } 

    $file_name1 = 'kundli_' . time() . '.png';
    $file_path1 = public_path() . '/images/' . $file_name1;
    
    if(isset($data['kundliPic']) && !empty($data['kundliPic']) ? $data['kundliPic'] : null) {
      file_put_contents($file_path1, base64_decode($data['kundliPic']));
    }

    $lastName = 
     isset($data['lastName']) && !empty($data['lastName']) ? $data['lastName'] : null;
    $emailId = 
     isset($data['emailId']) && !empty($data['emailId']) ? $data['emailId'] : null;
    $altNumber = 
     isset($data['altNumber']) && !empty($data['altNumber']) ? $data['altNumber'] : null;  
    $bloodGroup = 
     isset($data['bloodGroup']) && !empty($data['bloodGroup']) ? $data['bloodGroup'] : null;
    $birthPlace = 
    $colour = 
     isset($data['colour']) && !empty($data['colour']) ? $data['colour'] : null;
    $birthPlace = 
     isset($data['birthPlace']) && !empty($data['birthPlace']) ? $data['birthPlace'] : null;
    $birthTime = 
     isset($data['birthTime']) && !empty($data['birthTime']) ? $data['birthTime'] : null;
    $gothra = 
     isset($data['gothra']) && !empty($data['gothra']) ? $data['gothra'] : null;
    $nakshatra = 
     isset($data['nakshatra']) && !empty($data['nakshatra']) ? $data['nakshatra'] : null;
    $ganna = 
     isset($data['ganna']) && !empty($data['ganna']) ? $data['ganna'] : null;
    $rashi = 
     isset($data['rashi']) && !empty($data['rashi']) ? $data['rashi'] : null;
    $highestDegree = 
     isset($data['highestDegree']) && !empty($data['highestDegree']) ? $data['highestDegree'] : null;
    $educationField = 
     isset($data['educationField']) && !empty($data['educationField']) ? $data['educationField'] : null;
    $income = 
     isset($data['income']) && !empty($data['income']) ? $data['income'] : null;
    $workingIn = 
     isset($data['workingIn']) && !empty($data['workingIn']) ? $data['workingIn'] : null;
    $workingAs = 
     isset($data['workingAs']) && !empty($data['workingAs']) ? $data['workingAs'] : null;
    $ambition = 
     isset($data['ambition']) && !empty($data['ambition']) ? $data['ambition'] : null;
    $hobbies = 
     isset($data['hobbies']) && !empty($data['hobbies']) ? $data['hobbies'] : null; 
    $expectation = 
     isset($data['expectation']) && !empty($data['expectation']) ? $data['expectation'] : null;
    $presentCity = 
     isset($data['presentCity']) && !empty($data['presentCity']) ? $data['presentCity'] : null;
    $permanentCity = 
     isset($data['permanentCity']) && !empty($data['permanentCity']) ? $data['permanentCity'] : null;
    $country = 
     isset($data['country']) && !empty($data['country']) ? $data['country'] : null;  
    $fatherName = 
     isset($data['fatherName']) && !empty($data['fatherName']) ? $data['fatherName'] : null;   
    $fatherProfession = 
     isset($data['fatherProfession']) && !empty($data['fatherProfession']) ? $data['fatherProfession'] : null;   
    $motherName = 
     isset($data['motherName']) && !empty($data['motherName']) ? $data['motherName'] : null;   
    $motherProfession = 
     isset($data['motherProfession']) && !empty($data['motherProfession']) ? $data['motherProfession'] : null;   
    $siblings = 
     isset($data['siblings']) && !empty($data['siblings']) ? $data['siblings'] : null;           

    try{
      $selectUser = User::select('user_id as userId', 'email_id as emailId')
      ->where('user_id', '=', $id)
      ->get();

      //print_r($selectUser); exit;
      foreach ($selectUser as $index => $matri) {
       	$user = new Matrimony;
        $user->user_id = $matri['userId'];
        $user->marital_status = $data['maritalStatus'];
        $user->profile_for = $data['profileFor'];
        $user->first_name = $data['firstName'];
        $user->last_name = $lastName;
        $user->gender = $data['gender'];
        $user->image = is_null($data['userPic']) || empty($data['userPic']) ? null : $file_name;
        $user->mobile_number = $data['mobileNumber'];
        $user->alt_number = $altNumber;
        $user->email_id = $matri['emailId'];
        $user->age = $data['age'];
        $user->blood_group = $bloodGroup;
        $user->colour = $colour;
        $user->height = $data['height'];
        $user->dob = $data['dob'];
        $user->birth_place = $birthPlace;
        $user->birth_time = $birthTime;
        $user->gothra = $gothra;
        $user->nakshatra = $nakshatra;
        $user->ganna = $ganna;
        $user->rashi = $rashi;
        $user->kundli_pic = is_null($data['kundliPic']) || empty($data['kundliPic']) ? null : $file_name1;
        $user->highest_degree = $highestDegree;
        $user->education_field = $educationField;
        $user->income = $income;
        $user->working_in = $workingIn;
        $user->working_as = $workingAs;
        $user->ambition = $ambition;
        $user->hobbies = $hobbies;
        $user->expectation = $expectation;
        $user->present_city = $presentCity;
        $user->permanent_city = $permanentCity;
        $user->country = $country;
        $user->father_name = $fatherName;
        $user->father_profession = $fatherProfession;
        $user->mother_name = $motherName;
        $user->mother_profession = $motherProfession;
        $user->siblings = $siblings;
        $user->status = 'active';
        $user->save();
      }  

      return $this->respondWithSuccess('User added');
    }
    catch (\Illuminate\Database\QueryException $ex){
      $errorCode = $ex->errorInfo[1];
      if($errorCode == '1062'){
        return $this->respondWithError("Duplicate Entry");
      }
      else return $this->respondWithError($ex->getMessage());
    }         
    catch (\Exception $exception){
      return $this->respondWithError($exception->getMessage());
    }
  }   

  function getAllMarti($gender) {

    $response=[];
    try{
      $profiles = Matrimony::select('matrimony.user_id as userId','matrimony.marital_status as maritalStatus','matrimony.id as profileId','matrimony.first_name as firstName','matrimony.last_name as lastName', 'matrimony.email_id as emailId', 'matrimony.father_name as fatherName', 'matrimony.mother_name as motherName',
       'matrimony.present_city as presentCity', 'matrimony.working_as as workingAs', 'matrimony.image as userPic')
        ->where('matrimony.gender', '=', $gender)
        ->where('matrimony.status', '=', 'active')
        ->get();

      foreach ($profiles as $index => $profile) {
        array_push($response, [
          "profileId" => $profile['profileId'],
          "userId" => $profile['userId'],
          "maritalStatus" => $profile['maritalStatus'],
          "firstName" => $profile['firstName'],
          "lastName" => $profile['lastName'],
          "emailId" => $profile['emailId'],
          "fatherName" => $profile['fatherName'],
          "motherName" => $profile['motherName'],
          "presentCity" => $profile['presentCity'],
          "workingAs" => $profile['workingAs'],
          "userPic" =>is_null($profile['userPic']) || empty($profile['userPic']) ? "" : config('app.url') . '/images/' . $profile['userPic']	
        ]);
      } 
      return $this->respondWithSuccess($response);
    }
    catch (\Exception $exception){
      return $this->respondWithError($exception->getMessage());
    }
  }

  function getDetails($user_id) {

    $response=[];

    try{
      $profiles = Matrimony::select('matrimony.user_id as userId', 'matrimony.id as profileId','matrimony.marital_status as maritalStatus', 'matrimony.profile_for as profileFor','matrimony.first_name as firstName', 'matrimony.last_name as lastName', 'matrimony.gender as gender', 'matrimony.image as userPic', 'matrimony.mobile_number as mobileNumber', 'matrimony.alt_number as altNumber','matrimony.email_id as emailId', 'matrimony.age as age', 'matrimony.blood_group as bloodGroup','matrimony.colour as colour', 'matrimony.height as height', 'matrimony.dob as dob', 'matrimony.birth_place as birthPlace', 'matrimony.birth_time as birthTime','matrimony.gothra as gothra', 'matrimony.nakshatra as nakshatra','matrimony.ganna as ganna', 'matrimony.rashi as rashi', 'matrimony.kundli_pic as kundliPic', 'matrimony.highest_degree as highestDegree', 'matrimony.education_field as educationField', 'matrimony.income as income', 'matrimony.working_in as workingIn', 'matrimony.working_as as workingAs', 'matrimony.ambition as ambition','matrimony.hobbies as hobbies','matrimony.expectation as expectation', 'matrimony.present_city as presentCity', 'matrimony.permanent_city as permanentCity', 'matrimony.country as country', 'matrimony.father_name as fatherName', 'matrimony.mother_name as motherName', 'matrimony.father_profession as fatherProfession', 'matrimony.mother_profession as motherPofession', 'matrimony.siblings as siblings' )
      ->where('matrimony.user_id', '=', $user_id)
      ->where('matrimony.status', '=', 'active')
      ->get();

      foreach ($profiles as $index => $profile) {
        array_push($response, [
          "profileId" => $profile['profileId'],
          "userId" => $profile['userId'],
          "maritalStatus" => $profile['maritalStatus'],
          "profileFor" => $profile['profileFor'],
          "firstName" => $profile['firstName'],
          "lastName" => $profile['lastName'],
          "gender" => $profile['gender'],
          "userPic" =>is_null($profile['userPic']) || empty($profile['userPic']) ? "" : config('app.url') . '/images/' . $profile['userPic'],
          "mobileNumber" => $profile['mobileNumber'],
          "altNumber" => $profile['altNumber'],
          "emailId" => $profile['emailId'],
          "age" => $profile['age'],
          "bloodGroup" => $profile['bloodGroup'],
          "colour" => $profile['colour'],
          "height" => $profile['height'],
          "dob" => $profile['dob'],
          "birthPlace" => $profile['birthPlace'],
          "birthTime" => $profile['birthTime'],
          "gothra" => $profile['gothra'],
          "nakshatra" => $profile['nakshatra'],
          "ganna" => $profile['ganna'],
          "rashi" =>  $profile['rashi'],
          "kundliPic" => is_null($profile['kundliPic']) || empty($profile['kundliPic']) ? "" : config('app.url') . '/images/' . $profile['kundliPic'],
          "highestDegree" => $profile['highestDegree'],
          "educationField" => $profile['educationField'],
          "income" => $profile['income'],
          "workingIn" => $profile['workingIn'],
          "workingAs" => $profile['workingAs'],
          "ambition" => $profile['ambition'],
          "hobbies" => $profile['hobbies'],
          "expectation" => $profile['expectation'],
          "presentCity" => $profile['presentCity'],
          "permanentCity" => $profile['permanentCity'],
          "country" => $profile['country'],
          "fatherName" => $profile['fatherName'],
          "fatherProfession" => $profile['fatherProfession'],
          "motherName" => $profile['motherName'],
          "motherPofession" => $profile['motherPofession'],
          "siblings" => $profile['siblings']	
        ]);
      }
      return $this->respondWithSuccess($response);
    }
    catch (\Exception $exception){
      return $this->respondWithError($exception->getMessage());
    }
  }

  
  function updateMatrimony(Request $request, $id) {
     $validator = \Validator::make($request->all(), [
      'data.userId' => 'required',
      'data.maritalStatus' => 'bail|required|string|max:30',
      'data.profileFor' => 'bail|required|string',
      'data.firstName' => 'bail|required|string|max:50',
      'data.lastName' => 'bail|required|string|max:50',
      'data.gender' => 'required|string',
      'data.userPic' => 'required',
      'data.emailId' => 'bail|required|email',
      'data.age' => 'required',
      'data.height' => 'required|string',
      'data.colour' => 'string',
      'data.dob' => 'required|date',
      'data.birthPlace' => 'required|string',
      'data.gothra' => 'string|max:30',
      'data.nakshatra' => 'string|max:30',
      'data.ganna' => 'string|max:20',
      'data.rashi' => 'string|max:30',
      'data.highestDegree' => 'required|string|max:90',
      'data.educationField' => 'string|max:90',
      'data.income' => 'string',
      'data.workingIn' => 'string',
      'data.workingAs' => 'string',
      'data.ambition' => 'string|max:200',
      'data.expectation' => 'string|max:150',
      'data.presentCity' => 'string|max:30',
      'data.permanentCity' => 'string|max:30',
      'data.country' => 'string|max:20',
      'data.fatherName' => 'required|string',
      'data.fatherProfession' => 'required|string',
      'data.motherName' => 'required|string',
      'data.motherProfession' => 'required|string'
    ]);

    if($validator->fails()){
      return $this->respondWithValidationFail($validator->errors()->messages());
    }
    $data = $request->get('data');

    $file_name = 'image_' . time() . '.png';
    $file_path = public_path() . '/images/' . $file_name;
    if($data['userPic'] != "") {
      file_put_contents($file_path, base64_decode($data['userPic']));
    } 

    $file_name1 = 'kundli_' . time() . '.png';
    $file_path1 = public_path() . '/images/' . $file_name1;
    
    if(isset($data['kundliPic']) && !empty($data['kundliPic']) ? $data['kundliPic'] : null) {
      file_put_contents($file_path1, base64_decode($data['kundliPic']));
    }

    $lastName = 
     isset($data['lastName']) && !empty($data['lastName']) ? $data['lastName'] : null;
    $emailId = 
     isset($data['emailId']) && !empty($data['emailId']) ? $data['emailId'] : null;
    $altNumber = 
     isset($data['altNumber']) && !empty($data['altNumber']) ? $data['altNumber'] : null; 
    $colour = 
     isset($data['colour']) && !empty($data['colour']) ? $data['colour'] : null;
    $bloodGroup = 
     isset($data['bloodGroup']) && !empty($data['bloodGroup']) ? $data['bloodGroup'] : null; 
    $birthPlace = 
     isset($data['birthPlace']) && !empty($data['birthPlace']) ? $data['birthPlace'] : null;
    $birthTime = 
     isset($data['birthTime']) && !empty($data['birthTime']) ? $data['birthTime'] : null;
    $gothra = 
     isset($data['gothra']) && !empty($data['gothra']) ? $data['gothra'] : null;
    $nakshatra = 
     isset($data['nakshatra']) && !empty($data['nakshatra']) ? $data['nakshatra'] : null;
    $ganna = 
     isset($data['ganna']) && !empty($data['ganna']) ? $data['ganna'] : null;
    $rashi = 
     isset($data['rashi']) && !empty($data['rashi']) ? $data['rashi'] : null;
    $highestDegree = 
     isset($data['highestDegree']) && !empty($data['highestDegree']) ? $data['highestDegree'] : null;
    $educationField = 
     isset($data['educationField']) && !empty($data['educationField']) ? $data['educationField'] : null;
    $income = 
     isset($data['income']) && !empty($data['income']) ? $data['income'] : null;
    $workingIn = 
     isset($data['workingIn']) && !empty($data['workingIn']) ? $data['workingIn'] : null;
    $workingAs = 
     isset($data['workingAs']) && !empty($data['workingAs']) ? $data['workingAs'] : null;
    $ambition = 
     isset($data['ambition']) && !empty($data['ambition']) ? $data['ambition'] : null;
    $hobbies = 
     isset($data['hobbies']) && !empty($data['hobbies']) ? $data['hobbies'] : null; 
    $expectation = 
     isset($data['expectation']) && !empty($data['expectation']) ? $data['expectation'] : null;
    $presentCity = 
     isset($data['presentCity']) && !empty($data['presentCity']) ? $data['presentCity'] : null;
    $permanentCity = 
     isset($data['permanentCity']) && !empty($data['permanentCity']) ? $data['permanentCity'] : null;
    $country = 
     isset($data['country']) && !empty($data['country']) ? $data['country'] : null;  
    $fatherName = 
     isset($data['fatherName']) && !empty($data['fatherName']) ? $data['fatherName'] : null;   
    $fatherProfession = 
     isset($data['fatherProfession']) && !empty($data['fatherProfession']) ? $data['fatherProfession'] : null;   
    $motherName = 
     isset($data['motherName']) && !empty($data['motherName']) ? $data['motherName'] : null;   
    $motherProfession = 
     isset($data['motherProfession']) && !empty($data['motherProfession']) ? $data['motherProfession'] : null;   
    $siblings = 
     isset($data['siblings']) && !empty($data['siblings']) ? $data['siblings'] : null;

    try{
      $user = Matrimony::where('user_id', '=', $id)->first();
      $user->id = $data['profileId'];
      $user->user_id = $data['userId'];
      $user->marital_status = $data['maritalStatus'];
      $user->profile_for = $data['profileFor'];
      $user->first_name = $data['firstName'];
      $user->last_name = $lastName;
      $user->gender = $data['gender'];
      $user->image = is_null($data['userPic']) || empty($data['userPic']) ? null : $file_name;
      $user->mobile_number = $data['mobileNumber'];
      $user->alt_number = $data['altNumber'];
      $user->email_id = $emailId;
      $user->age = $data['age'];
      $user->blood_group = $bloodGroup;
      $user->colour = $colour;
      $user->height = $data['height'];
      $user->dob = $data['dob'];
      $user->birth_place = $birthPlace;
      $user->birth_time = $birthTime;
      $user->gothra = $gothra;
      $user->nakshatra = $nakshatra;
      $user->ganna = $ganna;
      $user->rashi = $rashi;
      $user->kundli_pic = is_null($data['kundliPic']) || empty($data['kundliPic']) ? null : $file_name1;
      $user->highest_degree = $highestDegree;
      $user->education_field = $educationField;
      $user->income = $income;
      $user->working_in = $workingIn;
      $user->working_as = $workingAs;
      $user->ambition = $ambition;
      $user->hobbies = $hobbies;
      $user->expectation = $expectation;
      $user->present_city = $presentCity;
      $user->permanent_city = $permanentCity;
      $user->country = $country;
      $user->father_name = $fatherName;
      $user->father_profession = $fatherProfession;
      $user->mother_name = $motherName;
      $user->mother_profession = $motherProfession;
      $user->siblings = $siblings;
      $user->status = 'active';
      $user->save();

      return $this->respondWithSuccess('Profile Updated');
    }
    catch (\Exception $exception){
      return $this->respondWithError($exception->getMessage());
    }
  }

  function deleteMatrimonyDetails(Request $request, $id){

    try{
      $b2b = Matrimony::where('user_id', '=', $id)->first();
      $b2b->status = 'deleted';
      $b2b->save();
      return $this->respondWithSuccess('Details deleted');
    } 
    catch (\Exception $exception){
      return $this->respondWithError($exception->getMessage());
    } 
  }


  function addBanner(Request $request) {
    $data = $request->get('data');

    $file_name1 = 'mbanner_' . time() . '.png';
    $file_path1 = public_path() . '/images/' . $file_name1;
    
    if(isset($data['banner1']) && !empty($data['banner1']) ? $data['banner1'] : null) {
      file_put_contents($file_path1, base64_decode($data['banner1']));
    }

    $file_name2 = 'mbanner_' . time() . '.png';
    $file_path2 = public_path() . '/images/' . $file_name2;
    if(isset($data['banner2']) && !empty($data['banner2']) ? $data['banner2'] : null) {
      file_put_contents($file_path2, base64_decode($data['banner2']));
    }

    $file_name3 = 'mbanner_' . time() . '.png';
    $file_path3 = public_path() . '/images/' . $file_name3;
    if(isset($data['banner3']) && !empty($data['banner3']) ? $data['banner3'] : null) {
      file_put_contents($file_path3, base64_decode($data['banner3']));
    }

    $text1 = isset($data['text1']) && !empty($data['text1']) ? $data['text1'] : null;
    $text2 = isset($data['text2']) && !empty($data['text2']) ? $data['text2'] : null;
    $text3 = isset($data['text3']) && !empty($data['text3']) ? $data['text3'] : null;

    try{
      $banner = new MatrimonyBanner();
      $file_name = 'image_' . time() . '.png';
      $file_path = public_path() . '/images/' . $file_name;
      if($data['banner1'] != "") {
        file_put_contents($file_path, base64_decode($data['banner1']));
      } 
      else if($data['banner2'] != "") {
        file_put_contents($file_path, base64_decode($data['banner2']));
      }
      else if($data['banner3'] != "") {
        file_put_contents($file_path, base64_decode($data['banner3']));
      } 
      $banner->banner1 = is_null($data['banner1']) || empty($data['banner1']) ? null : $file_name1;
      $banner->banner2 = is_null($data['banner2']) || empty($data['banner2']) ? null : $file_name2;
      $banner->banner3 = is_null($data['banner3']) || empty($data['banner3']) ? null : $file_name3;
      $banner->text1 = $text1;
      $banner->text2 = $text2;
      $banner->text3 = $text3;

      $banner->save();
      return $this->respondWithSuccess('Banner added');
    }
    catch (\Exception $exception){
      return $this->respondWithError($exception->getMessage());
    }  
  }

  function getBanners() {
       
    $response=[];
    try
    {
      $banners = MatrimonyBanner::select(['id' ,'banner1', 'banner2', 'banner3', 'text1', 'text2','text3'])
      ->orderBy('id', 'DESC')
      ->take(1)
      ->get();

      foreach ($banners as $index => $banner) {
        array_push($response, [
          "id" => $banner['id'],
          "banner1" =>is_null($banner['banner1']) || empty($banner['banner1']) ? "" : config('app.url') . '/images/' . $banner['banner1'],
          "banner2" => is_null($banner['banner2']) || empty($banner['banner2']) ? "" : config('app.url') . '/images/' . $banner['banner2'],
          "banner3" => is_null($banner['banner3']) || empty($banner['banner3']) ? "" : config('app.url') . '/images/' . $banner['banner3'],
          "text1" =>  $banner['text1'],
          "text2" => $banner['text2'],
          "text3" => $banner['text3'],
        ]);
      } 
      return $this->respondWithSuccess($response);
    }
    catch (\Exception $exception){
      return $this->respondWithError($exception->getMessage());
    }  
  }

  function sendInterest(Request $request) {
    
    $data = $request->get('data');
    
    $results = MatrimonyRequest::select()->where([['logged_id', $data['loggedId']], ['interested_in_id', $data['interestedId']]])
    ->orWhere([['logged_id', $data['interestedId']], ['interested_in_id', $data['loggedId']]])
    ->count();

    if($results == 1){
     return $this->respondWithError('already sent a interest');
    }
    else{
      try{
        $interest = new MatrimonyRequest();
        $interest->logged_id = $data['loggedId'];
        $interest->interested_in_id = $data['interestedId'];
        $interest->status = 'interested';
        $interest->seen = 'no';
        $interest->save();
        return $this->respondWithSuccess('Request Sent');
      }
      
      catch (\Exception $exception){
        return $this->respondWithError($exception->getMessage());
      }
    }  
  }

  function sentInterest($id) {
    $response=[];
    try{
      $friends = DB::table('matrimony_interest as mi')
      ->select('m.first_name as firstName', 'm.last_name as lastName', 'mi.id as id', 'mi.logged_id as loggedId', 'mi.interested_in_id as interestedId','mi.seen as seenStatus', 'm.image as pic')
      ->join('matrimony as m', 'mi.interested_in_id', '=', 'm.user_id')
      ->where('mi.logged_id', '=', $id)
      ->where('m.status', '=', 'active')
      ->get();

      foreach ($friends as $index => $friend) {
        array_push($response, [
          "id" => $friend->id,
          "loggedId" => $friend->loggedId,
          "interestedId" => $friend->interestedId,
          "firstName" => $friend->firstName,
          "lastName" => $friend->lastName,
          "seenStatus" => $friend->seenStatus,
          "profilePic" => is_null($friend->pic) || empty($friend->pic) ? "" : config('app.url') . '/images/' . $friend->pic 
        ]);
      } 
      return $this->respondWithSuccess($response);
    }
    catch (\Exception $exception){
      return $this->respondWithError($exception->getMessage());
    }
  }

  
  function getInterest($id) {
    $response=[];
    try{
      $friends = DB::table('matrimony_interest as mi')
      ->select('m.first_name as firstName', 'm.last_name as lastName', 'mi.id as id', 'mi.logged_id as loggedId', 'mi.interested_in_id as interestedId', 'mi.seen as seenStatus', 'm.image as pic')
      ->join('matrimony as m', 'mi.logged_id', '=', 'm.user_id')
      ->where('mi.interested_in_id', '=', $id)
      ->where('m.status', '=', 'active')
      ->get();

      foreach ($friends as $index => $friend) {
        array_push($response, [
          "id" => $friend->id,
          "loggedId" => $friend->interestedId,
          "interestedId" => $friend->loggedId,
          "firstName" => $friend->firstName,
          "lastName" => $friend->lastName,
          "seenStatus" => $friend->seenStatus,
          "profilePic" => is_null($friend->pic) || empty($friend->pic) ? "" : config('app.url') . '/images/' . $friend->pic 
        ]);
      } 
      return $this->respondWithSuccess($response);
    }
    catch (\Exception $exception){
      return $this->respondWithError($exception->getMessage());
    }
  }

  function acceptOrRejectInterest(Request $request, $loggedId, $interestedId) {
    $data = $request->get('data');
    try{
      $user = MatrimonyRequest::where([['logged_id', $loggedId], ['interested_in_id', $interestedId]])
      ->orWhere([['logged_id', $interestedId], ['interested_in_id', $loggedId]])
      ->first();
      
      $user->status = $data['status'];
      $user->save();
      return $this->respondWithSuccess('request '.$data['status'] );
    }
    catch (\Exception $exception){
      return $this->respondWithError($exception->getMessage());
    }        
  }

  function statusInMatrimonyRequest($loggedId, $interestedId) {

    try{
      $users = MatrimonyRequest::select('logged_id as loggedId', 'interested_in_id as interestedId', 'status')
      ->where([['logged_id', $loggedId], ['interested_in_id', $interestedId]])
      ->orWhere([['logged_id', $interestedId], ['interested_in_id', $loggedId]])
      ->get();
      return $this->respondWithSuccess($users);
    }
    catch (\Exception $exception){
      return $this->respondWithError($exception->getMessage());
    }
  }

  function matrimonySeenStatus(Request $request, $id){

    try{
      $b2b = MatrimonyRequest::findOrFail($id);
      $b2b->seen = 'yes';
      $b2b->save();
      return $this->respondWithSuccess('Notification seen');
    } 
    catch (\Exception $exception){
      return $this->respondWithError($exception->getMessage());
    } 
  } 

  function filter(Request $request) {

    $filterData = Input::get();
    $response = [];

    $profiles = Matrimony::select('matrimony.user_id as userId','matrimony.marital_status as maritalStatus','matrimony.id as profileId','matrimony.first_name as firstName','matrimony.last_name as lastName', 'matrimony.email_id as emailId', 'matrimony.father_name as fatherName', 'matrimony.mother_name as motherName', 'matrimony.present_city as presentCity', 'matrimony.working_as as workingAs', 'matrimony.image as userPic')
    ->where('status', '=', 'active');

    if (isset($filterData['gender']) && !empty($filterData['gender'])) {
      $profiles->where('gender', '=', $filterData['gender']);
    }
    if (isset($filterData['status']) && !empty($filterData['status'])) {
      $profiles->where('marital_status', '=', $filterData['status']);
    }
    if (isset($filterData['country']) && !empty($filterData['country'])) {
      $profiles->where('country', '=', $filterData['country']);
    }
    if (isset($filterData['ageFrom']) || isset($filterData['ageTo'])) {
      $profiles->whereBetween('age', [[$filterData['ageFrom']], [$filterData['ageTo']]] );
    }
    
    $profiles = $profiles->get();

    foreach ($profiles as $index => $profile) {
      array_push($response, [
        "profileId" => $profile['profileId'],
        "userId" => $profile['userId'],
        "maritalStatus" => $profile['maritalStatus'],
        "firstName" => $profile['firstName'],
        "lastName" => $profile['lastName'],
        "emailId" => $profile['emailId'],
        "fatherName" => $profile['fatherName'],
        "motherName" => $profile['motherName'],
        "presentCity" => $profile['presentCity'],
        "workingAs" => $profile['workingAs'],
        "userPic" =>is_null($profile['userPic']) || empty($profile['userPic']) ? "" : config('app.url') . '/images/' . $profile['userPic']  
      ]);
    } 
    return $this->respondWithSuccess($response);
  }
}