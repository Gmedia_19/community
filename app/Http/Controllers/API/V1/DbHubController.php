<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController;
use Illuminate\Support\Facades\Input;
use App\Model\DbHub;
use App\Model\BookHub;

class DbHubController extends BaseController
{
  function addHub(Request $request) {
    $data = $request->get('data');

    $file_name = 'hub_' . time() . '.png';
    $file_path = public_path() . '/images/' . $file_name;
    if($data['image'] != "") {
      file_put_contents($file_path, base64_decode($data['image']));
    }
    try{
      $hub = new DbHub;
      $hub->user_id = $data['userId'];
      $hub->hub_name = $data['hubName'];
      $hub->address = $data['address'];
      $hub->city = $data['city'];
      $hub->state = $data['state'];
      $hub->description = $data['description'];
      $hub->contact_number = $data['contactNumber'];
      $hub->members = $data['members'];
      $hub->image = is_null($data['image']) || empty($data['image']) ? null : $file_name;
      $hub->status = 'active';
      $hub->save();

      return $this->respondWithSuccess('Hub added'); 
    } 
    catch (\Exception $exception){
      return $this->respondWithError($exception->getMessage());
    } 
  }

  function editHub(Request $request, $id) {
    $data = $request->get('data');

    $file_name = 'hub_' . time() . '.png';
    $file_path = public_path() . '/images/' . $file_name;
    if($data['image'] != "") {
      file_put_contents($file_path, base64_decode($data['image']));
    }
    try{
      $hub = DbHub::findOrFail($id);
      $hub->user_id = $data['userId'];
      $hub->hub_name = $data['hubName'];
      $hub->address = $data['address'];
      $hub->city = $data['city'];
      $hub->state = $data['state'];
      $hub->description = $data['description'];
      $hub->contact_number = $data['contactNumber'];
      $hub->members = $data['members'];
      $hub->image = is_null($data['image']) || empty($data['image']) ? null : $file_name;
      $hub->status = 'active';
      $hub->save();

      return $this->respondWithSuccess('Hub updated'); 
    }
    catch (\Exception $exception){
      return $this->respondWithError($exception->getMessage());
    } 
  }

  function getAllHub() {

    $response=[];

    try{
      $hubs = DbHub::select(
        'id', 'user_id', 'hub_name', 'address', 'city', 'state', 'description', 'contact_number', 'members', 'image')
      ->where('status', '=', 'active')
      ->orderBy('hub_name', 'asc')
      ->get();

      foreach ($hubs as $index => $hub) {
        array_push($response, [
          "hubId" => $hub['id'],
          "userId" => $hub['user_id'],
          "hubName" => $hub['hub_name'],
          "address" => $hub['address'],
          "city" => $hub['city'],
          "state" => $hub['state'],
          "description" => $hub['description'],
          "contactNumber" => $hub['contact_number'],
          "members" => $hub['members'],
          "image" => is_null($hub['image']) || empty($hub['image']) ? "" : config('app.url') . '/images/' . $hub['image'] 
        ]);
      } 
      return $this->respondWithSuccess($response);
    }
    catch (\Exception $exception){
      return $this->respondWithError($exception->getMessage());
    } 
  } 

  function getCityHub($city) {

    $response=[];
    try{
      $hubs = DbHub::select(
        'id', 'user_id', 'hub_name', 'address', 'city', 'state', 'description', 'contact_number', 'members', 'image')
      ->where('city', '=', $city)
      ->where('status', '=', 'active')
      ->orderBy('hub_name', 'asc')
      ->get();

      foreach ($hubs as $index => $hub) {
        array_push($response, [
          "hubId" => $hub['id'],
          "userId" => $hub['user_id'],
          "hubName" => $hub['hub_name'],
          "address" => $hub['address'],
          "city" => $hub['city'],
          "state" => $hub['state'],
          "description" => $hub['description'],
          "contactNmber" => $hub['contact_number'],
          "members" => $hub['members'],
          "image" => is_null($hub['image']) || empty($hub['image']) ? "" : config('app.url') . '/images/' . $hub['image'] 
        ]);
      } 
      return $this->respondWithSuccess($response);
    }
    catch (\Exception $exception){
      return $this->respondWithError($exception->getMessage());
    } 
  }

  function getStateHub($state) {

    $response=[];
    try{
      $hubs = DbHub::select(
        'id', 'user_id', 'hub_name', 'address', 'city', 'state', 'description', 'contact_number', 'members', 'image')
      ->where('state', '=', $state)
      ->where('status', '=', 'active')
      ->orderBy('hub_name', 'asc')
      ->get();

      foreach ($hubs as $index => $hub) {
        array_push($response, [
          "hubId" => $hub['id'],
          "userId" => $hub['user_id'],
          "hubName" => $hub['hub_name'],
          "address" => $hub['address'],
          "city" => $hub['city'],
          "state" => $hub['state'],
          "description" => $hub['description'],
          "contactNmber" => $hub['contact_number'],
          "members" => $hub['members'],
          "image" => is_null($hub['image']) || empty($hub['image']) ? "" : config('app.url') . '/images/' . $hub['image'] 
        ]);
      } 
      return $this->respondWithSuccess($response);
    }
    catch (\Exception $exception){
      return $this->respondWithError($exception->getMessage());
    } 
  }

  function getSpecificHub($id) {

    $response=[];
    try{
      $hubs = DbHub::select(
        'id', 'user_id', 'hub_name', 'address', 'city', 'state', 'description', 'contact_number', 'members', 'image')
      ->where('id', '=', $id)
      ->where('status', '=', 'active')
      ->orderBy('hub_name', 'asc')
      ->get();

      foreach ($hubs as $index => $hub) {
        array_push($response, [
          "hubId" => $hub['id'],
          "userId" => $hub['user_id'],
          "hubName" => $hub['hub_name'],
          "address" => $hub['address'],
          "city" => $hub['city'],
          "state" => $hub['state'],
          "description" => $hub['description'],
          "contactNmber" => $hub['contact_number'],
          "members" => $hub['members'],
          "image" => is_null($hub['image']) || empty($hub['image']) ? "" : config('app.url') . '/images/' . $hub['image'] 
        ]);
      } 
      return $this->respondWithSuccess($response);
    } 
    catch (\Exception $exception){
      return $this->respondWithError($exception->getMessage());
    }
  } 

  function deleteDbHub(Request $request, $id){

    try{
      $hub = DbHub::findOrFail($id);
      $hub->status = 'deleted';
      $hub->save();
      return $this->respondWithSuccess('successfully deleted');
    } 
    catch (\Exception $exception){
      return $this->respondWithError($exception->getMessage());
    } 
  }

  function addHubEvent(Request $request) {
    $data = $request->get('data');

    $file_name = 'temple_' . time() . '.png';
    $file_path = public_path() . '/images/' . $file_name;
    if($data['image'] != "") {
      file_put_contents($file_path, base64_decode($data['image']));
    }
    try{
      $temple = new BookHub();
      $temple->user_id = $data['userId'];
      $temple->hub_id = $data['hubId'];
      $temple->event_name = $data['eventName'];
      $temple->event_detail = $data['eventDetail'];
      $temple->date_time = $data['dateTime'];
      $temple->image = is_null($data['image']) || empty($data['image']) ? null : $file_name;
      $temple->status = 'active';

      $temple->save();
      return $this->respondWithSuccess('event added');
    }
    catch (\Exception $exception){
      return $this->respondWithError($exception->getMessage());
    }  
  }

  function editHubEvent(Request $request, $id) {
    $data = $request->get('data');

    $file_name = 'temple_' . time() . '.png';
    $file_path = public_path() . '/images/' . $file_name;
    if($data['image'] != "") {
      file_put_contents($file_path, base64_decode($data['image']));
    }
    try{
      $temple = BookHub::findOrFail($id);
      $temple->user_id = $data['userId'];
      $temple->hub_id = $data['hubId'];
      $temple->event_name = $data['eventName'];
      $temple->event_detail = $data['eventDetail'];
      $temple->date_time = $data['dateTime'];
      $temple->image = is_null($data['image']) || empty($data['image']) ? null : $file_name;
      $temple->status = 'active';

      $temple->save();
      return $this->respondWithSuccess('event Updated');
    }
    catch (\Exception $exception){
      return $this->respondWithError($exception->getMessage());
    }  
  }

  function getHubEvent($id) {

    $response=[];
    try{
      $temples = BookHub::select('id', 'user_id', 'hub_id', 'event_name', 'event_detail', 'date_time', 'image')
      ->where('status', '=', 'active')
      ->where('hub_id', '=', $id)
      ->get();

      foreach ($temples as $index => $temple) {
        array_push($response, [
          "eventId" => $temple['id'],
          "userId" => $temple['user_id'],
          "hubId" => $temple['hub_id'],
          "eventName" => $temple['event_name'],
          "eventDetail" => $temple['event_detail'],
          "dateTime" => $temple['date_time'],
          "image" =>is_null($temple['image']) || empty($temple['image']) ? "" : config('app.url') . '/images/' . $temple['image'] 
        ]);
      } 
      return $this->respondWithSuccess($response);
    }
    catch (\Exception $exception){
      return $this->respondWithError($exception->getMessage());
    }  
  }

  function getSpecificHubEvent($id) {

    $response=[];

    try{
      $temples = BookHub::select('id', 'user_id', 'hub_id', 'event_name', 'event_detail', 'date_time', 'image')
      ->where('id', '=', $id)
      ->where('status', '=', 'active')
      ->get();

      foreach ($temples as $index => $temple) {
        array_push($response, [
          "eventId" => $temple['id'],
          "userId" => $temple['user_id'],
          "hubId" => $temple['hub_id'],
          "eventName" => $temple['event_name'],
          "eventDetail" => $temple['event_detail'],
          "dateTime" => $temple['date_time'],
          "image" =>is_null($temple['image']) || empty($temple['image']) ? "" : config('app.url') . '/images/' . $temple['image'] 
        ]);
      } 
      return $this->respondWithSuccess($response);
    }
    catch (\Exception $exception){
      return $this->respondWithError($exception->getMessage());
    } 
  }

  function deleteBookedHub(Request $request, $id){

    try{
      $hub = BookHub::findOrFail($id);
      $hub->status = 'deleted';
      $hub->save();
      return $this->respondWithSuccess('successfully deleted');
    } 
    catch (\Exception $exception){
      return $this->respondWithError($exception->getMessage());
    } 
  } 
}

