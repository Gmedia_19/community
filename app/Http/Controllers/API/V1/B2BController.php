<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController;
use Illuminate\Support\Facades\Input;
use App\Model\B2B;

class B2BController extends BaseController
{
  function addProduct(Request $request) {
        $data = $request->get('data');

        $type = 
          isset($data['type']) && !empty($data['type']) ? $data['type'] : null;
        $image = 
          isset($data['image']) && !empty($data['image']) ? $data['image'] : null; 
        $content = 
          isset($data['content']) && !empty($data['content']) ? $data['content'] : null;
        $quantity = 
          isset($data['quantity']) && !empty($data['quantity']) ? $data['quantity'] : null;
        $emailId = 
          isset($data['emailId']) && !empty($data['emailId']) ? $data['emailId'] : null;
        $areaDescription = 
          isset($data['areaDescription']) && !empty($data['areaDescription']) ? $data['areaDescription'] : null;     

        
        $file_name = 'image_' . time() . '.png';
        $file_path = public_path() . '/images/' . $file_name;
        if($data['image'] != "") {
            file_put_contents($file_path, base64_decode($data['image']));
        } 
        try{
            $edu = new B2B;
            $edu->user_id = $data['userId'];
            $edu->product_name = $data['productName'];
            $edu->name = $data['name'];
            $edu->contact_number = $data['contactNumber'];
            $edu->emailId = $data['emailId'];
            $edu->address = $data['address'];
            $edu->content = $content;
            $edu->image = is_null($data['image']) || empty($data['image']) ? null : $file_name;
            $edu->quantity = $quantity;
            $edu->type = $type;
            $edu->status = 'active';

            $edu->save();
            return $this->respondWithSuccess('Details added');  
        }
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    } 

    function editProduct(Request $request, $id) {
        $data = $request->get('data');

        $type = 
          isset($data['type']) && !empty($data['type']) ? $data['type'] : null;
        $image = 
          isset($data['image']) && !empty($data['image']) ? $data['image'] : null; 
        $content = 
          isset($data['content']) && !empty($data['content']) ? $data['content'] : null;
        $quantity = 
          isset($data['quantity']) && !empty($data['quantity']) ? $data['quantity'] : null;
        $emailId = 
          isset($data['emailId']) && !empty($data['emailId']) ? $data['emailId'] : null;
        $areaDescription = 
          isset($data['areaDescription']) && !empty($data['areaDescription']) ? $data['areaDescription'] : null;        

        
        $file_name = 'image_' . time() . '.png';
        $file_path = public_path() . '/images/' . $file_name;
        if($data['image'] != "") {
            file_put_contents($file_path, base64_decode($data['image']));
        } 
        try{
            $edu = B2B::findOrFail($id);
            $edu->user_id = $data['userId'];
            $edu->product_name = $data['productName'];
            $edu->name = $data['name'];
            $edu->contact_number = $data['contactNumber'];
            $edu->emailId = $data['emailId'];
            $edu->address = $data['address'];
            $edu->content = $content;
            $edu->area_description = $areaDescription;
            $edu->image = is_null($data['image']) || empty($data['image']) ? null : $file_name;
            $edu->quantity = $quantity;
            $edu->type = $type;
            $edu->status = 'active';

            $edu->save();
            return $this->respondWithSuccess('Details Updated');  
        }
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    } 

    function getProducts() {

        $response=[];
        try{
            $hubs = B2B::select(
                'b_id', 'user_id', 'product_name', 'name', 'contact_number', 'emailId', 'address', 'content', 'image', 'quantity', 'type')
            ->where('type', '=', 'product')
            ->where('status', '=', 'active')
            ->orderBy('b_id', 'DESC')
            ->get();

            foreach ($hubs as $index => $hub) {
              array_push($response, [
                    "id" => $hub['b_id'],
                    "userId" => $hub['user_id'],
                    "productName" => $hub['product_name'],
                    "name" => $hub['name'],
                    "contactNumber" => $hub['contact_number'],
                    "emailId" => $hub['emailId'],
                    "address" => $hub['address'],
                    "content" => $hub['content'],
                    //"image" => is_null($hub['image']) || empty($hub['image']) ? "" : config('app.url') . '/images/' . $hub['image'],
                    "image" =>  is_null($hub['image']) || empty($hub['image']) ? "" : config('app.url') . '/images/' .$hub['image'],
                    "quantity" => $hub['quantity']	
                ]);
            } 
            return $this->respondWithSuccess($response);
        }
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }    
    }

    function getProperty() {

        $response=[];

        try{
            $hubs = B2B::select(
                'b_id', 'user_id', 'product_name', 'name', 'contact_number', 'emailId', 'address', 'content', 'area_description', 'image', 'quantity',
                'type')
            ->where('type', '=', 'property')
            ->where('status', '=', 'active')
            ->orderBy('b_id', 'DESC')
            ->get();

            foreach ($hubs as $index => $hub) {
              array_push($response, [
                    "id" => $hub['b_id'],
                    "userId" => $hub['user_id'],
                    "productName" => $hub['product_name'],
                    "name" => $hub['name'],
                    "contactNumber" => $hub['contact_number'],
                    "emailId" => $hub['emailId'],
                    "address" => $hub['address'],
                    "content" => $hub['content'],
                    "areaDescription" => $hub['areaDescription'],
                    "image" =>  is_null($hub['image']) || empty($hub['image']) ? "" : config('app.url') . '/images/' .$hub['image'],
                    "quantity" => $hub['quantity']	
                ]);
            } 
            return $this->respondWithSuccess($response);
        }
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }    
    } 

    function getSpecificProduct($id) {

        $response=[];

        try{
            $hubs = B2B::select(
            'b_id', 'user_id', 'product_name', 'name', 'contact_number', 'emailId', 'address', 'content','area_description', 'image', 'quantity','type')
            ->where('b_id', '=', $id)
            ->where('status', '=', 'active')
            ->orderBy('b_id', 'DESC')
            ->get();

            foreach ($hubs as $index => $hub) {
              array_push($response, [
                    "id" => $hub['b_id'],
                    "userId" => $hub['user_id'],
                    "productName" => $hub['product_name'],
                    "name" => $hub['name'],
                    "contactNumber" => $hub['contact_number'],
                    "emailId" => $hub['emailId'],
                    "address" => $hub['address'],
                    "content" => $hub['content'],
                    "areaDescription" => $hub['areaDescription'],
                    "image" =>  is_null($hub['image']) || empty($hub['image']) ? "" : config('app.url') . '/images/' .$hub['image'],
                    "quantity" => $hub['quantity']  
                ]);
            } 
            return $this->respondWithSuccess($response);
        }
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }    
    } 

    function deleteProduct(Request $request, $id){

        try{
          $b2b = B2B::findOrFail($id);
          $b2b->status = 'deleted';
          $b2b->save();
          return $this->respondWithSuccess('Product deleted');
        } 
        catch (\Exception $exception){
          return $this->respondWithError($exception->getMessage());
        } 
    }
}
