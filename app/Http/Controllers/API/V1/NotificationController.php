<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController;
use App\Model\Notification;

class NotificationController extends BaseController
{
    function sendNotification(Request $request){

	    $data = $request->get('data');
	        //generating unique file name;
	    $file_name = 'image_' . time() . '.png';
	    $file_path = public_path() . '/images/' . $file_name;
	    if($data['image'] != "") {
	        file_put_contents($file_path, base64_decode($data['image']));
	    }
	    try{
	        $notification = new Notification();
	        $notification->user_id = $data['userId'];
	        $notification->title = $data['title'];
	        $notification->content = $data['content'];
	        $notification->image = is_null($data['image']) || empty($data['image']) ? null : $file_name;
	        $notification->token = $data['token'];
	        $notification->status = 'no';

	        $notification->save();
	        return $this->respondWithSuccess('Notification send');
	    }
	    catch (\Exception $exception){
	        return $this->respondWithError($exception->getMessage());
	    }
	    catch (\Symfony\Component\Debug\Exception $e) {
	    	return $this->respondWithError($e->getMessage());
		}
	} 

	function getNotification($user_id) {

        $response=[];

        try{
            $users = Notification::select('id', 'user_id', 'title', 'content', 'image', 'token', 'status')
            ->where('user_id', '=', $user_id)
            ->get(); 

	        foreach ($users as $index => $user) {
	          array_push($response, [
	          		"notificationId" => $user['id'],
	                "userId" => $user['user_id'],
	                "title" => $user['title'],
	                "content" => $user['content'],
	                "image" =>is_null($user['image']) || empty($user['image']) ? "" : config('app.url') . '/images/' . $user['image'],
	                "token" => $user['token'],
	                "status" => $user['status'],
	            ]);
	        } 
	        return $this->respondWithSuccess($response);
	    }
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        } 
    } 

    function deleteNotification(Request $request, $id){

        try{
          $b2b = Notification::findOrFail($id);
          $b2b->status = 'deleted';
          $b2b->save();
          return $this->respondWithSuccess('Notification deleted');
        } 
        catch (\Exception $exception){
          return $this->respondWithError($exception->getMessage());
        } 
    }   
}
