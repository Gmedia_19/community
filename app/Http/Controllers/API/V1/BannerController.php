<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController;
use Illuminate\Support\Facades\Input;
use App\Model\Banner;

class BannerController extends BaseController
{
  function addBanner(Request $request) {
    $data = $request->get('data');

    $file_name = 'banner_' . time() . '.png';
    $file_path = public_path() . '/images/' . $file_name;
    if($data['banner'] != "") {
      file_put_contents($file_path, base64_decode($data['banner']));
    }
      
    try{
      $banner = new Banner();
      $banner->image = is_null($data['banner']) || empty($data['banner']) ? null : $file_name;
      $banner->description = $data['description'];
      $banner->status = 'active';
      $banner->save();
      return $this->respondWithSuccess('Banner added');
    }
    catch (\Exception $exception){
      return $this->respondWithError($exception->getMessage());
    }    
  }

  function getBanners() {
       
    $response=[];
   try{
       // $latestUser = Banner::select('banner_id')
       // ->orderBy('banner_id', 'DESC')
       // ->first();

      $banners = Banner::select(['id as bannerId', 'description', 'image as url'])
       //->where('banner_id', '=', $latestUser->banner_id)
      ->where('status', '=', 'active')
      ->orderBy('id', 'DESC')
      ->paginate(5);

      foreach ($banners as $index => $banner) {
        array_push($response, [
          "bannerId" =>  $banner['bannerId'],
          "description" => $banner['description'],
          "url" => is_null($banner['url']) || empty($banner['url']) ? "" : config('app.url') . '/images/' . $banner['url'],
        ]);
      } 
      return $this->respondWithSuccess($response);
    }
    catch (\Exception $exception){
      return $this->respondWithError($exception->getMessage());
    }    
  }

  function deleteBanner(Request $request, $id){

    try{
      $banners = Banner::findOrFail($id);
      $banners->status = 'deleted';
      $banners->save();
      return $this->respondWithSuccess('Banner deleted');
    } 
    catch (\Exception $exception){
      return $this->respondWithError($exception->getMessage());
    } 
  }
}
