<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\API\BaseController;
use Illuminate\Support\Facades\Input;
use App\Model\Timeline;
use App\Model\TimelineComment;
use App\Model\TimelineLike;
use App\Model\Friends;
use DB;


class TimelineController extends BaseController
{
 
    function addTimeline(Request $request) {
        $date = \Carbon\Carbon::now()->toDateTimeString();
        $data = $request->get('data');
        //generating unique file name;
        $file_name = 'image_' . time() . '.png';
        $file_path = public_path() . '/images/' . $file_name;
        if($data['photos'] != "") {
            file_put_contents($file_path, base64_decode($data['photos']));
        }
        try{
            $timeline = new Timeline();
            $timeline->user_id = $data['userId'];
            $timeline->post = $data['post'];
            $timeline->photos = is_null($data['photos']) || empty($data['photos']) ? null : $file_name;
            $timeline->status = 'active';
            $timeline->created_at = $date;

            $timeline->save();
            return $this->respondWithSuccess([
                "timelineId" => $timeline->timeline_id,
                "postingTime" => $date,
                "textContent" => $data['post'],
                "photos" => is_null($data['photos']) || empty($data['photos']) ? "" : config('app.url') . '/images/' . $file_name
            ]);
        }
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    }

    function shareTimeline(Request $request, $id) {
       try{
           $selectTimeline = Timeline::select('user_id', 'post', 'photos')
            ->where('timeline_id', '=', $id)
            ->get();

            foreach ($selectTimeline as $index => $timeline) {
               $data = $request->get('data');
               $updateTimeline = new Timeline();
               $updateTimeline->user_id = $data['userId'];
               $updateTimeline->post = $timeline['post'];
               $updateTimeline->photos = $timeline['photos'];
               $updateTimeline->status = 'active';
               $updateTimeline->posts_user_id = $timeline['user_id'];
               $updateTimeline->save();
               return $this->respondWithSuccess('Post shared');
            }
        }
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }      
    }

    function shareTimelineInFriendProfile(Request $request, $id) {
       try{
            $selectTimeline = Timeline::select('user_id', 'post', 'photos')
            ->where('timeline_id', '=', $id)
            ->get();
            
            foreach ($selectTimeline as $index => $timeline) {
               $data = $request->get('data');
               $updateTimeline = new Timeline();
               $updateTimeline->user_id = $data['friendId'];
               $updateTimeline->post = $timeline['post'];
               $updateTimeline->photos = $timeline['photos'];
               $updateTimeline->status = 'shared';
               $updateTimeline->posts_user_id = $data['userId'];
               $updateTimeline->save();
               return $this->respondWithSuccess('Post shared');
            }
        }
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }      
    }

    function getTimeline($user_id) {
       
       $response=[];
       try{
           $timelines = Timeline::select(['u.user_id as userId', 'u.first_name as firstName', 'u.last_name as lastName','up.user_id as postUserId', 'up.first_name as postUserfirstName', 'up.last_name as postUserlastName', 'timeline.timeline_id as timelineId', 'timeline.post as post', 'timeline.photos as photo', 'timeline.created_at as postingTime', 'timeline.status as status', 'p.pic as profilePic', 'pup.pic as postUserProfilePic'])
            ->join('user as u', 'timeline.user_id', '=', 'u.user_id')
            ->leftjoin('user as up', 'timeline.posts_user_id', '=', 'up.user_id')
            ->leftjoin('user_pic as p','u.user_pic_id', '=', 'p.pic_id')
            ->leftjoin('user_pic as pup','up.user_pic_id', '=', 'pup.pic_id')
            ->where('timeline.user_id', '=', $user_id)
            ->where('timeline.status', '!=', 'deleted')
            ->orderBy('timeline.timeline_id', 'DESC')
            ->get();
                            
            foreach ($timelines as $index => $timeline) {
                array_push($response, [
                    "timelineId" => $timeline['timelineId'],
                    "userId" => $timeline['userId'],
                    "firstName" =>$timeline['firstName'],
                    "lastName" => $timeline['lastName'],
                    "textContent" =>  is_null($timeline['post']) || empty($timeline['post']) ? "" : $timeline['post'],
                    "photos" => is_null($timeline['photo']) || empty($timeline['photo']) ? "" : config('app.url') . '/images/' . $timeline['photo'],
                    "postingTime" => $timeline['postingTime'],
                    "profilePic" => is_null($timeline['profilePic']) || empty($timeline['profilePic']) ? config('app.url') . '/images/default_pic.jpg' : config('app.url') . '/images/' . $timeline['profilePic'],
                    "postUserId" => $timeline['postUserId'],
                    "postUserfirstName" =>$timeline['postUserfirstName'],
                    "postUserlastName" => $timeline['postUserlastName'],
                    "postUserProfilePic" => is_null($timeline['postUserProfilePic']) || empty($timeline['postUserProfilePic']) ? config('app.url') . '/images/default_pic.jpg' : config('app.url') . '/images/' . $timeline['postUserProfilePic'],
                    "status" => $timeline['status']
                ]);
            } 
             
            return $this->respondWithSuccess($response);
        }
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    }

    function getAllTimeline() {
       
       $response=[];
       try{
           $timelines = Timeline::select(['u.user_id as userId', 'u.first_name as firstName', 'u.last_name as lastName','up.user_id as postUserId', 'up.first_name as postUserfirstName', 'up.last_name as postUserlastName',
            'timeline.post as post', 'timeline.photos as photo', 'timeline.timeline_id as timelineId','timeline.created_at as postingTime', 'user_pic.pic as profilePic'])
            ->join('user as u', 'timeline.user_id', '=', 'u.user_id')
            ->leftjoin('user as up', 'timeline.posts_user_id', '=', 'up.user_id')
            ->leftjoin('user_pic','u.user_pic_id', '=', 'user_pic.pic_id')
            ->where('timeline.status', '=', 'active')
            ->orderBy('timeline.timeline_id', 'DESC')
            ->get();

            foreach ($timelines as $index => $timeline) {
                array_push($response, [
                    "timelineId" => $timeline['timelineId'],
                    "userId" => $timeline['userId'],
                    "firstName" =>$timeline['firstName'],
                    "lastName" => $timeline['lastName'],
                    "textContent" =>  is_null($timeline['post']) || empty($timeline['post']) ? "" : $timeline['post'],
                    "photos" => is_null($timeline['photo']) || empty($timeline['photo']) ? "" : config('app.url') . '/images/' . $timeline['photo'],
                    "postingTime" => $timeline['postingTime'],
                    "profilePic" => is_null($timeline['profilePic']) || empty($timeline['profilePic']) ? config('app.url') . '/images/default_pic.jpg' : config('app.url') . '/images/' . $timeline['profilePic'],
                    "postUserId" => $timeline['postUserId'],
                    "postUserfirstName" =>$timeline['postUserfirstName'],
                    "postUserlastName" => $timeline['postUserlastName']
                ]);
            } 
            return $this->respondWithSuccess($response);
        }
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    }

    function deleteTimeline(Request $request, $id){

        try{
          $friend = Timeline::findOrFail($id);
          $friend->status = 'deleted';
          $friend->save();
          return $this->respondWithSuccess('Timeline Deleted');
        } 
        catch (\Exception $exception){
          return $this->respondWithError($exception->getMessage());
        } 
    }

    function timelineComment(Request $request) {
        $data = $request->get('data');
        try{
            $comment = new TimelineComment();
            $comment->timeline_id = $data['timelineId'];
            $comment->user_id = $data['userId'];
            $comment->comment = $data['comment'];
            $comment->status = 'active';
            $comment->save();

            return $this->respondWithSuccess('Comment added');
        }
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    } 

    function editTimelineComment(Request $request, $id) {
        $data = $request->get('data');
        try{
            $comment = TimelineComment::findOrFail($id);
            $comment->id = $data['commentId'];
            $comment->timeline_id = $data['timelineId'];
            $comment->user_id = $data['userId'];
            $comment->comment = $data['comment'];
            $comment->status = 'active';
            $comment->save();

            return $this->respondWithSuccess('Comment updated');
        }
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    } 

    function getTimelineComment($timeline_id) {
       
       $response=[];
       try{
           $timelines = TimelineComment::select(['user.first_name as firstName', 'user.last_name as lastName', 'timeline_comment.id as commentId', 'timeline_comment.timeline_id as timelineId', 'timeline_comment.user_id as userId', 'timeline_comment.comment as comment', 'timeline_comment.created_at as postingTime', 'user_pic.pic as profilePic', 'timeline.photos as photo'])
            ->join('user', 'timeline_comment.user_id', '=', 'user.user_id')
            ->join('timeline', 'timeline_comment.timeline_id', '=', 'timeline.timeline_id')
            ->leftjoin('user_pic','user.user_pic_id', '=', 'user_pic.pic_id')
            ->where('timeline_comment.timeline_id', '=', $timeline_id)
            ->where('timeline_comment.status', '=', 'active')
            ->orderBy('timeline_comment.id', 'DESC')
            ->get();
                            
            foreach ($timelines as $index => $timeline) {
                array_push($response, [
                    "commentId" => $timeline['commentId'],
                    "timelineId" => $timeline['timelineId'],
                    "userId" => $timeline['userId'],
                    "firstName" => $timeline['firstName'],
                    "lastName" => $timeline['lastName'],
                    "photos" => is_null($timeline['photo']) || empty($timeline['photo']) ? "" : config('app.url') . '/images/' . $timeline['photo'],
                    "profilePic" => is_null($timeline['profilePic']) || empty($timeline['profilePic']) ? config('app.url') . '/images/default_pic.jpg' : config('app.url') . '/images/' . $timeline['profilePic'],
                    "comment" => $timeline['comment'],
                    "postingTime" => $timeline['postingTime']
                ]);
            } 
             
            return $this->respondWithSuccess($response);
        }
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    }

    function deleteTimelineComment($id) {

       try{
          $comment = TimelineComment::findOrFail($id);
          $comment->status = 'deleted';
          $comment->save();
          return $this->respondWithSuccess('Comment Deleted');
        } 
        catch (\Exception $exception){
          return $this->respondWithError($exception->getMessage());
        } 
    }

    function timelineLike(Request $request) {

        $data = $request->get('data');
        try{
            $user = TimelineLike::where([['user_id', $data['userId']], ['timeline_id', $data['timelineId']]])->first();
            if (!is_null($user) && $user->status == 'liked'){
                $user->status = 'unliked';
                $user->save();
                return $this->respondWithSuccess('pic unLiked');
            }
            else if(!is_null($user) && $user->status == 'unliked'){
                $user->status = 'liked';
                $user->save();
                return $this->respondWithSuccess('pic Liked');
            }
            else{
                $like = new TimelineLike();
                $like->timeline_id = $data['timelineId'];
                $like->user_id = $data['userId'];
                $like->status = 'liked';

                $like->save();
                return $this->respondWithSuccess('pic Liked');
            }
        }catch(\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    }

    function timelineLikeStatus($userId, $timelineId) {
        try{
            $user = TimelineLike::select('status')
            ->where('user_id', '=', $userId)
            ->where('timeline_id', '=', $timelineId)
            ->get();
            return $this->respondWithSuccess($user);
        }catch(\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    }


    function countTimelineLike($id) {
        
        try{
            $wordlist = TimelineLike::where('timeline_id', '=', $id)
            ->where('status', '=', 'liked')
            ->count();
            return $this->respondWithSuccess($wordlist);
        }
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    }

    function getNameOfTimelinePic($timeline_id) {
       
       $response=[];
       try{
            $comments = TimelineLike::select(['user.first_name as firstName', 'user.last_name as lastName', 'timeline_like.id as id','timeline_like.user_id as userId', 'timeline_like.timeline_id as timelineId'])
            ->join('user', 'timeline_like.user_id', '=', 'user.user_id')
            ->where('timeline_like.timeline_id', '=', $timeline_id)
            ->where('timeline_like.status', '=', 'liked')
            ->get();

            foreach ($comments as $index => $comment) {
                array_push($response, [
                    "id" => $comment['id'],
                    "timelineId" => $comment['timelineId'],
                    "userId" => $comment['userId'],
                    "firstName" => $comment['firstName'],
                    "lastName" => $comment['lastName']
                ]);
            } 
            return $this->respondWithSuccess($response);
        }
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    }
}
