<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController;
use Illuminate\Support\Facades\Input;
use App\Model\Cover_Pic;
use App\Model\CoverPicComment;
use App\Model\CovePicLike;
use App\Model\User;

class CoverPicController extends BaseController
{
    function updateCoverPic(Request $request) {
        $data = $request->get('data');
        
        $validator = \Validator::make($request->all(), [
            'data.coverPic' => 'required',
        ]);

        if($validator->fails()){
            return $this->respondWithValidationFail($validator->errors()->messages());
        }

        $file_name = 'image_' . time() . '.png';
        $file_path = public_path() . '/images/' . $file_name;
        if($data['coverPic'] != "") {
            file_put_contents($file_path, base64_decode($data['coverPic']));
        }

        try{
            $pic = new Cover_Pic;
            $pic->user_id =$data['userId'];
            $pic->cover_pic = $file_name;
            $pic->status = 'active';
            $pic->save();
            $pic->cover_id;

            //updating column user_pic_id of user table 

            $user = User::find($pic->user_id);
            $user->cover_pic_id = $pic->cover_id;
            $user->save();

            return $this->respondWithSuccess('Cover pic updated');
        }
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    }

    function getCoverPic($user_id) {
       
       $response=[];
       try{
           $coverPics = Cover_Pic::select(['user_id', 'cover_id', 'cover_pic'])
                ->where('user_id', '=', $user_id)
                ->where('status', '=', 'active')
                ->orderBy('cover_id', 'DESC')
                ->get();

            foreach ($coverPics as $index => $coverPic) {
                 array_push($response, [
                    "picId" => $coverPic['cover_id'],
                    "userId" => $coverPic['user_id'],
                    "coverPic" => config('app.url') . '/images/' . $coverPic['cover_pic']
                ]);
            } 
            return $this->respondWithSuccess($response);
        }
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    }

    function deleteCoverPic(Request $request, $id){

        try{
          $friend = Cover_Pic::findOrFail($id);
          $friend->status = 'deleted';
          $friend->save();
          return $this->respondWithSuccess('Cover image Deleted');
        } 
        catch (\Exception $exception){
          return $this->respondWithError($exception->getMessage());
        } 
    }


    function coverPicComment(Request $request) {
        $data = $request->get('data');
        try{
            $comment = new CoverPicComment();
            $comment->pic_id = $data['picId'];
            $comment->user_id = $data['userId'];
            $comment->comment = $data['comment'];
            $comment->status = 'active';
            $comment->save();
            return $this->respondWithSuccess('Comment added');
        }
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    } 

    function editCoverPicComment(Request $request, $id) {
        $data = $request->get('data');
        try{
            $comment = CoverPicComment::findOrFail($id);
            $comment->id = $data['commentId'];
            $comment->pic_id = $data['picId'];
            $comment->user_id = $data['userId'];
            $comment->comment = $data['comment'];
            $comment->status = 'active';
            $comment->save();
            return $this->respondWithSuccess('Comment Updated');
        }
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    } 

    function getCoverPicComment($pic_id) {
       
       $response=[];
       try{
           $comments = CoverPicComment::select(['user.first_name as firstName', 'user.last_name as lastName','coverpic_comment.id as commentId', 'coverpic_comment.user_id as userId', 'coverpic_comment.pic_id as picId', 'coverpic_comment.comment as comment', 'coverpic_comment.created_at as postingTime', 'user_pic.pic as profilePic', 'cover_pic.cover_pic as coverPic'])
            ->join('user', 'coverpic_comment.user_id', '=', 'user.user_id')
            ->join('cover_pic' , 'coverpic_comment.pic_id', '=', 'cover_pic.cover_id')
            ->leftjoin('user_pic','user.user_pic_id', '=', 'user_pic.pic_id')
            ->where('coverpic_comment.pic_id', '=', $pic_id)
            ->where('coverpic_comment.status', '=', 'active')
            ->orderBy('coverpic_comment.id', 'DESC')
            ->get();

            foreach ($comments as $index => $comment) {
                array_push($response, [
                    "commentId" => $comment['commentId'],
                    "picId" => $comment['picId'],
                    "userId" => $comment['userId'],
                    "firstName" => $comment['firstName'],
                    "lastName" => $comment['lastName'],
                    "coverPic" => is_null($comment['coverPic']) || empty($comment['coverPic']) ? config('app.url') . '/images/default_cover_pic2.jpg' : config('app.url') . '/images/' .$comment['coverPic'],
                    "profilePic" => is_null($comment['profilePic']) || empty($comment['profilePic']) ? config('app.url') . '/images/default_pic.jpg' : config('app.url') . '/images/' .$comment['profilePic'],
                    "comment" =>$comment['comment'],
                    "postingTime" =>$comment['postingTime'],
                    //"postingTime" =>$comment['created_at']->format('Y-m-d h:m')
                ]);
            } 
             
            return $this->respondWithSuccess($response);
        }
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    }


    function deleteCoverPicComment($id) {
        try{
          $comment = CoverPicComment::findOrFail($id);
          $comment->status = 'deleted';
          $comment->save();
          return $this->respondWithSuccess('Comment Deleted');
        } 
        catch (\Exception $exception){
          return $this->respondWithError($exception->getMessage());
        } 
    }

    function coverPicLike(Request $request) {

        $data = $request->get('data');
        try{
            $user = CovePicLike::where([['user_id', $data['userId']], ['cover_id', $data['coverId']]])->first();
            if (!is_null($user) && $user->status == 'liked'){
                $user->status = 'unliked';
                $user->save();
                return $this->respondWithSuccess('pic unLiked');
            }
            else if(!is_null($user) && $user->status == 'unliked'){
                $user->status = 'liked';
                $user->save();
                return $this->respondWithSuccess('pic Liked');
            }
            else{
                $user = new CovePicLike();
                $user->cover_id = $data['coverId'];
                $user->user_id = $data['userId'];
                $user->status = 'liked';

                $user->save();
                return $this->respondWithSuccess('pic Liked');
            }
        }catch(\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    }

    function coverPicLikeStatus($userId, $coverId) {
        try{
            $user = CovePicLike::select('status')
            ->where('user_id', '=', $userId)
            ->where('cover_id', '=', $coverId)
            ->get();
            return $this->respondWithSuccess($user);
        }catch(\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    }

    function countCoverPicLike($id) {
        
        try{
            $wordlist = CovePicLike::where('cover_id', '=', $id)
            ->where('status', '=', 'liked')
            ->count();
            return $this->respondWithSuccess($wordlist);
        }
         catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    }

    function getNameOfCoverPic($id) {
       
       $response=[];
       try{
            $comments = CovePicLike::select(['user.first_name as firstName', 'user.last_name as lastName', 'cover_pic_like.id as id', 'cover_pic_like.user_id as userId', 'cover_pic_like.cover_id as coverId'])
            ->join('user', 'cover_pic_like.user_id', '=', 'user.user_id')
            ->where('cover_pic_like.cover_id', '=', $id)
            ->where('cover_pic_like.status', '=', 'liked')
            ->get();

            foreach ($comments as $index => $comment) {
                array_push($response, [
                    "id" => $comment['id'],
                    "coverId" => $comment['coverId'],
                    "userId" => $comment['userId'],
                    "firstName" => $comment['firstName'],
                    "lastName" => $comment['lastName']
                ]);
            } 
            return $this->respondWithSuccess($response);
        }
        catch (\Exception $exception){
            return $this->respondWithError($exception->getMessage());
        }
    }
}
