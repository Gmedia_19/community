<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Exception;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

class authJWT
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $header = $request->header('authorization');
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (TokenExpiredException $e) {
            return response()->json([
                    'reasonCode' => 1,
                    'reasonText' => 'token_expired',
                    'data'       => 'Token is expired'
                ], 401);
        } catch (TokenInvalidException $e) {
             return response()->json([
                    'reasonCode' => 1,
                    'reasonText' => 'invalid_token',
                    'data'       => 'Token is invalid'
                ], 401);
        } catch (JWTException $e) {
            //return response($e->getMessage());
            return response()->json([
                    'reasonCode' => 1,
                    'reasonText' => 'token_absent',
                    'data'       => 'Token is not present'
                ], 401);
        }
        return $next($request);
    }
}
