<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Temple extends Model
{
	public function __construct()
    {
        $this->table = 'temple';
        parent::__construct();
    }    
}
