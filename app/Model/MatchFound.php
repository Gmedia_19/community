<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MatchFound extends Model
{
     public function __construct()
    {
        $this->table = 'match_found';
        parent::__construct();
    } 
}
