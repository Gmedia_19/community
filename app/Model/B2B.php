<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class B2B extends Model
{
    protected $table;
    protected $primaryKey = 'b_id';

	public function __construct()
    {
        $this->table = 'b2b';
        parent::__construct();
    }    
}
