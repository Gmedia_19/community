<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TempleEvent extends Model
{
    public function __construct()
    {
        $this->table = 'temple_event';
        parent::__construct();
    }  
}
