<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Cover_Pic extends Model
{
   protected $table;
    protected $primaryKey = 'cover_id';

	public function __construct()
    {
        $this->table = 'cover_pic';
        parent::__construct();
    }    
}
