<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Timeline extends Model
{
    protected $primaryKey = 'timeline_id';
	public function __construct()
    {
        $this->table = 'timeline';
        parent::__construct();
    }    
}
