<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class NewInDB extends Model
{
   protected $table;

	public function __construct()
    {
        $this->table = 'new_in_db';
        parent::__construct();
    }   
}
