<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PicComment extends Model
{
    protected $primaryKey = 'pic_comment_id';
	public function __construct()
    {
        $this->table = 'userpic_comment';
        parent::__construct();
    }    
}
