<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BookHub extends Model
{
   public function __construct()
    {
        $this->table = 'book_hub';
        parent::__construct();
    }  
}
