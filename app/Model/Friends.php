<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Friends extends Model
{
   
	public function __construct()
    {
        $this->table = 'friends';
        parent::__construct();
    }  
}
