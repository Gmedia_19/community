<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DbHub extends Model
{
    protected $table;

	public function __construct()
    {
        $this->table = 'daivajnya_hub';
        parent::__construct();
    }    
}
