<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CareerTalent extends Model
{
  protected $table;

	public function __construct()
    {
        $this->table = 'career_talent';
        parent::__construct();
    }  
}
