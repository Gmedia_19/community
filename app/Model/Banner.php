<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $table;
    
	public function __construct()
    {
        $this->table = 'home_banner';
        parent::__construct();
    }    
}
