<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TimelineComment extends Model
{
   protected $primaryKey = 'id';
	public function __construct()
    {
        $this->table = 'timeline_comment';
        parent::__construct();
    }    
}
