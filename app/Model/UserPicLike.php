<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserPicLike extends Model
{
   protected $table;
    protected $primaryKey = 'pic_like_id';

	public function __construct()
    {
        $this->table = 'userpic_like';
        parent::__construct();
    }  
}
