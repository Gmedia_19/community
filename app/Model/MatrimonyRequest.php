<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MatrimonyRequest extends Model
{
    public function __construct()
    {
        $this->table = 'matrimony_interest';
        parent::__construct();
    } 
}
