<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class JewelleryHub extends Model
{
   protected $table;
    protected $primaryKey = 'j_id';

	public function __construct()
    {
        $this->table = 'jewellery_hub';
        parent::__construct();
    }      
}
