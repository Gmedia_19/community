<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
//use Illuminate\Contracts\JWTSubject;
//use user as Authenticatable;
// class User extends Model
// {
//   protected $primaryKey = 'user_id';
// 	public function __construct()
//   {   
//     $this->table = 'user';
//     parent::__construct();
//   }  
// }
use Illuminate\Http\Request;

use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject

{
    use Notifiable;
   
    protected $primaryKey = 'user_id';

    public function __construct()
    {
        $this->table = 'user';
        parent::__construct();
    }

    protected $fillable = [
        'first_name', 'last_name', 'email_id', 'password', 'is_verified', 'provider', 'provider_id'
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function profilePic()
    {
        return $this->hasOne('App\Model\User_Pic', 'pic_id', 'user_pic_id');
    }

    public function coverPic()
    {
        return $this->hasOne('App\Model\Cover_Pic', 'cover_id', 'cover_pic_id');
    }

    // public function messages()
    // {
    //     return $this->hasMany(Message::class);
    // }
    
}