<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class JewelleryImage extends Model
{
   protected $table;
   protected $fillable = [
        'user_id', 'image', 'text', 'status'
    ];
	public function __construct()
    {
        $this->table = 'jewellery_pic';
        parent::__construct();
    }   
}
