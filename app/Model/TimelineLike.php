<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TimelineLike extends Model
{
   
	public function __construct()
    {
        $this->table = 'timeline_like';
        parent::__construct();
    }  
}
