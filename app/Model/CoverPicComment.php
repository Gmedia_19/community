<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CoverPicComment extends Model
{
    protected $table;

	public function __construct()
    {
        $this->table = 'coverpic_comment';
        parent::__construct();
    }  
}
