<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
   public function __construct()
    {
        $this->table = 'notification';
        parent::__construct();
    }   
}
