<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class User_Pic extends Model
{
    protected $table;
    protected $primaryKey = 'pic_id';

	public function __construct()
    {
        $this->table = 'user_pic';
        parent::__construct();
    }    
}
