<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CovePicLike extends Model
{
   public function __construct()
    {
        $this->table = 'cover_pic_like';
        parent::__construct();
    }  
}
