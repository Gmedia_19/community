<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    //protected $primaryKey = 'message_id';

	public function __construct()
    {
        $this->table = 'live_message';
        parent::__construct();
    } 
}
