<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LastName extends Model
{
   public function __construct()
    {
        $this->table = 'last_name';
        parent::__construct();
    } 
}
