<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class NewsEvents extends Model
{
   public function __construct()
  {   
    $this->table = 'news_events';
    parent::__construct();	
    
  }    
}
