<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MatrimonyBanner extends Model
{
	public function __construct()
    {
        $this->table = 'matrimony_banner';
        parent::__construct();
    } 
}
