
<!doctype html>
<html>
<head>
 @include('admin.includes.admin_head') 
</head>
<body>
<div class="container">
 <header class="row">
  @include('admin.includes.admin_header') 
  </header>
  <div id="main" class="row">
   </div>
   <div class="clearfix">
    </div>

  <!-- responsive tab section -->
  <section>
    <div class="container">
      <div class="row">
        <div class="board">
         
          <div class="board-inner ">
            <ul class="nav nav-tabs" id="myTab">
              <div class="liner"></div>
              <li class="active">
               <a href="#home" data-toggle="tab" title="Timeline">
                <span class="round-tabs one">
                  <i class="fas fa-history"></i> 
                </span> 
              </a></li>

              <li><a href="#profile" data-toggle="tab" title="profile">
               <span class="round-tabs two">
                 <i class="fas fa-user-secret"></i>
               </span> 
             </a>
           </li>
           <li><a href="#friends" data-toggle="tab" title="Friends">
             <span class="round-tabs three">
               <i class="fas fa-users"></i>
             </span> </a>
           </li>

           <li><a href="#photos" data-toggle="tab" title="Photos">
             <span class="round-tabs four">
               <i class="far fa-image"></i>
             </span> 
           </a></li>

           <li><a href="#groups" data-toggle="tab" title="Groups">
             <span class="round-tabs six">
              <i class="far fa-user-circle"></i>
            </span> </a>
          </li>
          <li><a href="#videos" data-toggle="tab" title="videos">
           <span class="round-tabs five">
             <i class="fas fa-video"></i>
           </span> </a>
         </li>
         <li class="search-container"> 
    <form action="/action_page.php">
      <input type="text" placeholder="Search.." name="search" class="search-bt">
      <button type="submit" class="sbbtn"><i class="fa fa-search"></i></button>
    </form>
  </li>
</ul>
</div>
<div class="tab-content">
        <div class="tab-pane fade in active" id="home">

         <div class="col-md-5">

           <div class="col-md-12  photo-section" style="border:solid 2px #fff;">

            <div class="title">

              <i class="far fa-image ph"></i>
               <span>  Photos </span>
            </div>
            <div class="col-md-3" id="timelineImages">
             
<a href="#" class="pop" data-toggle="modal" data-target="#myModal">
<img src="images/img_nature.jpg" style="width: 400px; height: 264px;">
</a>
</div>
 </div>
<div class="clearfix"> </div>
<div class="col-md-12 photo-section">

          <div class="title">
            <i class="fas fa-users ph1"></i>
            <span>  Friends </span>
          </div>
          <div class="col-md-4"  id="friendsImagesList">
            <!-- <div class="photos"> 
              <img  src="images/b2b.png">
            </div> -->
          </div>
       </div>

       <div class="clearfix"> </div>
       <div class="col-md-12 photo-section">

        <div class="title">
          <i class="fas fa-video class="ph"></i>

          <span>  Videos </span>
        </div>
        <div class="col-md-4">
          <div class="photos"> 
            <img class="profile-pic" src="images/b2b.png">
          </div>
        </div>
        <div class="col-md-4">

         <img class="profile-pic" src="images/b2b.png">
       </div>
       <div class="col-md-4">

         <img class="profile-pic" src="images/b2b.png">
       </div>

     </div>
   </div>
   <!-- end of first tab -->
   <div class="col-md-7" style="border:solid 2px red;">

    <div class="clearfix">
    </div>


    <div class="col-md-12">
          @include('User_dashboard.includes.timeline')

          <div class="clearfix"> </div>
          <br/>
          <!-- <div class="col-md-12" id="timeline">   
          </div> -->
       </div>   
    </div> 
    <!-- end of status div -->
    <div class="clearfix"> </div>
    <br/>

  </div>
  <div class="clearfix"> </div> <br/>
</div>
<div class="tab-pane fade" id="profile">
 <div class="col-md-12">
  <div class="about_title">
    <i class="fa fa-user" aria-hidden="true"></i> <span> About</span>
  </div>
  <div class="about_tab">
    <button class="tablinks" onclick="openCity(event, 'personalInfo')" id="defaultOpen">Contact And Basic info</button>
    <button class="tablinks" onclick="openCity(event, 'professionalInfo')">Work and Education</button>
    <button class="tablinks" onclick="openCity(event, 'familyInfo')">Family and Relationships</button>
  </div>

  <div id="personalInfo" class="about_content">

    <!-- edit form column -->
    <div class="col-md-12 personal-info">
      <h3>Personal info</h3>
        <form class="form-horizontal" role="form">
          <div class="form-group">
            <div class="col-md-1"> </div>
              <label class="col-lg-3 control-label">First name:</label>
            <div class="col-lg-6">
              <input class="form-control" type="text" value="Jane">
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-1"> </div>
              <label class="col-lg-3 control-label">Last name:</label>
            <div class="col-lg-6">
                <input class="form-control" type="text" value="Bishop">
            </div>
          </div>

          <div class="form-group"> <!-- Date input -->
            <div class="col-md-1"> </div>
               <label class="control-label col-lg-3" for="date">Date Of birth</label>
            <div class="col-lg-6">
                <input class="form-control" id="date" name="date" placeholder="MM/DD/YYY" type="text"/>
            </div>
          </div>
         <div class="form-group">
           <div class="col-md-1"> </div>
              <label class="control-label col-lg-3" for="date">Gender</label>
           <div class="col-lg-6"> 
               <label class="radio-inline">
                   <input type="radio" name="optradio">Male
               </label>
              <label class="radio-inline">
                  <input type="radio" name="optradio">Female
              </label>
           </div> 
          </div>

<div class="form-group">
 <div class="col-md-1"> </div>
 <label class="col-lg-3 control-label">Mobile No:</label>
 <div class="col-lg-6">
  <input class="form-control" type="text" value="">
</div>
</div>
<div class="form-group">
  <label class="col-md-3 control-label"></label>
  <div class="col-md-8">
    <input type="button" class="btn btn-primary" value="Save Changes">
    <span></span>
    <input type="reset" class="btn btn-default" value="Cancel">
  </div>
</div>
</form>
</div>
</div>

<div id="professionalInfo" class="about_content">

 <!-- edit form column -->
 <div class="col-md-12 personal-info">
   
  <h3>Work</h3>
  <form class="form-horizontal" role="form">
    <div class="form-group">
      <div class="col-md-1"> </div>
      <label class="col-lg-3 control-label">Working In</label>
      <div class="col-lg-6">
        <input class="form-control" type="text" value="Jane">
      </div>
    </div>
    <div class="form-group">
     <div class="col-md-1"> </div>
     <label class="col-lg-3 control-label">Highschool</label>
     <div class="col-lg-6">
      <input class="form-control" type="text" value="Bishop">
    </div>
  </div>
  <div class="form-group">
   <div class="col-md-1"> </div>
   <label class="col-lg-3 control-label">Graduation</label>
   <div class="col-lg-6">
    <input class="form-control" type="text" value="">
  </div>
</div>
<div class="form-group">
 <div class="col-md-1"> </div>
 <label class="col-lg-3 control-label">Current City</label>
 <div class="col-lg-6">
  <input class="form-control" type="text" value="">
</div>
</div>
<div class="form-group">
 <div class="col-md-1"> </div>
 <label class="col-lg-3 control-label">Home Town</label>
 <div class="col-lg-6">
  <input class="form-control" type="text" value="">
</div>
</div>
<div class="form-group">
  <label class="col-md-3 control-label"></label>
  <div class="col-md-8">
    <input type="button" class="btn btn-primary" value="Save Changes">
    <span></span>
    <input type="reset" class="btn btn-default" value="Cancel">
  </div>
</div>
</form>
</div>
</div>

<div id="familyInfo" class="about_content">
  <div class="col-md-12 personal-info">
   
    <h3>Family</h3>
    <form class="form-horizontal" role="form">
      <div class="form-group">
        <div class="col-md-1"> </div>
        <label class="col-lg-3 control-label">Relationship Status</label>
        <div class="col-lg-6">
          <input class="form-control" type="text" value="Jane">
        </div>
      </div>
      <div class="form-group">
       <div class="col-md-1"> </div>
       <label class="col-lg-3 control-label">Family Members</label>
       <div class="col-lg-6">
        <input class="form-control" type="text" value="Bishop">
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label"></label>
      <div class="col-md-8">
        <input type="button" class="btn btn-primary" value="Save Changes">
        <span></span>
        <input type="reset" class="btn btn-default" value="Cancel">
      </div>
    </div>

  </form>
</div>
</div>
</div>

</div>

<div class="tab-pane fade" id="friends">
 <div class="col-md-12"> 
  <div class="friend-title"> 
   <i class="fas fa-users" style="font-size:25px"> </i> <span style="font-size: 18px;
   margin: 0px 2px 2px 14px; font-family:robot">  Friends </span>
 </div>

 <div class="search">
  <input type="text" class="searchTerm" placeholder="What are you looking for?">
  <button type="submit" class="searchButton">
    <i class="fa fa-search"></i>
  </button>
</div>

<ul id="tabs">
  <li><a href="#about">All Friends</a></li>
  <li><a href="#advantages">Recently Added</a></li>
</ul>
<div class="tabContent" id="about">
  <div class="top">
    <h2>Friend's List</h2>
  </div>
  <div class="col-md-12" id="friendsList">
  </div>
</div>

<div class="clearfix"></div>

<div class="tabContent" id="advantages">
 <div class="top">
    <h2>Friend's List</h2>
  </div>
 <div class="col-md-12" id="recentFriendsList">
         
  </div> 
</div>
<!-- friends javascript  tab section -->
</div>
</div>
<div class="tab-pane fade" id="photos">
 
  <div class="col-md-12"> 
    <div class="friend-title"> 
     <i class="fas fa-users" style="font-size:25px"> </i> <span style="font-size: 18px;
     margin: 0px 2px 2px 14px; font-family:robot">  Photos </span>
   </div>
   <div id="tabs-container">
    <ul class="photos-tabs-menu">
      <li class="current"><a href="#photos-tab-1">Your Photos</a></li>
      <li><a href="#photos-tab-2">Album</a></li>
      </ul>
      <div class="photos-tab">
        <div id="photos-tab-1" class="photos-tab-content">
           <div class="col-md-4 col-sm-12 co-xs-12 gal-item" id="photosList">
          </div> 







</div>
        <div id="photos-tab-2" class="photos-tab-content">
          <p>List of photo Albums </p>
          
        </div>
        
        
      </div>
    </div>
  </div>

</div>
<div class="tab-pane fade" id="groups">
 <div class="col-md-12 video-shadow">

  <h3 class="head text-center">Groups  </h3>
</div>
</div>

<div class="tab-pane fade" id="videos">
 <div class="col-md-12 video-shadow">

   <div class="col-md-4">
    <div class="video"><iframe class="embed-responsive-item" src="https://www.youtube.com/embed/AWdA7hdP4ZA?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe></div>
  </div>
  <div class="col-md-4">
    <div class="video"><iframe class="embed-responsive-item" src="https://www.youtube.com/embed/I39RKjerJss?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe></div>
  </div>
  <div class="col-md-4">
    <div class="video"><iframe class="embed-responsive-item" src="https://www.youtube.com/embed/Dkx4LYeFnVY?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe></div>
  </div>
</div>
</div>




<div class="clearfix"></div>
</div>

</div>
</div>
</div>
</section>

</div>

<footer class="row">
  @include('User_dashboard.includes.footer')

</footer>
<script type="text/javascript" src="{{asset('js/jquery-notification.js')}}"></script>
<script type="text/javascript" src={{ asset('/js/user/main.js') }}></script>
<script type="text/javascript" src={{ asset('js/tabs.js') }}></script>
</div>
</body>
</html>

