 @include('admin.includes.admin_head') 
 @include('admin.includes.admin_header')

<style>
.img-thumbnail{
  width:100%;
  height:100px;
  object-fit: cover;
  object-position: center;
  margin:10px;
}

@media(max-width: 480px) {
  .img-thumbnail{
    height:50px;
  }
}
</style>

<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

<div class="nav-side-menu">
  <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>
           <div class="menu-list">
              <ul id="menu-content" class="menu-content collapse out">
                <li>
                  <a href="#">
                  <i class="fa fa-dashboard fa-lg"></i> Dashboard
                  </a>
                </li>
                <li  data-toggle="collapse" data-target="#products" class="collapsed active">
                  <a href="#"><i class="fa fa-gift fa-lg"></i> DB Hub <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="products">
                    <li class="active"><a href="#">Post Hub Details</a></li>
                    <li><a href="#">Display All Hub Details</a></li>
                    <li><a href="#">Display Hub By City</a></li>
                    <li><a href="#">Post Hub Event</a></li>
                    <li><a href="#">Display Hub Events</a></li>
                    
                </ul>
                <li data-toggle="collapse" data-target="#service" class="collapsed">
                  <a href="#"><i class="fa fa-globe fa-lg"></i> News and Events <span class="arrow"></span></a>
                </li>  
                <ul class="sub-menu collapse" id="service">
                  <li>Add News and Events</li>
                  <li>Display all News</li>
                  <li>Display Category News 3</li>
                </ul>
               <li data-toggle="collapse" data-target="#new" class="collapsed">
                  <a href="#"><i class="fa fa-car fa-lg"></i> Jewellery Hub <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="new">
                  <li>Post Details</li>
                  <li>Display Jewellery hub by type</li>
                  <li>Display Stock</li>
                </ul>

                <li data-toggle="collapse" data-target="#mat" class="collapsed">
                  <a href="#"><i class="fa fa-car fa-lg"></i>  Matrimony <span class="arrow">
                    
                  </span></a>
                </li>

                 <ul class="sub-menu collapse" id="mat">
                  <li>Post Banner</li>
                <!--   <li>Display Jewellery hub by type</li>
                  <li>Display Stock</li> -->
                </ul>
                
               <li data-toggle="collapse" data-target="#talent" class="collapsed">
                  <a href="#"><i class="fa fa-car fa-lg"></i>  Career and Talent <span class="arrow">
                    
                  </span></a>
                </li>

                 <ul class="sub-menu collapse" id="talent">
                  <li>Post Banner</li>
                <!--   <li>Display Jewellery hub by type</li>
                  <li>Display Stock</li> -->
                </ul>
                 <li data-toggle="collapse" data-target="#b2b" class="collapsed">
                  <a href="#"><i class="fa fa-car fa-lg"></i> B2B <span class="arrow">
                    
                  </span></a>
                </li>

                 <ul class="sub-menu collapse" id="b2b">
                  <li>Add product/property</li>
                <!--   <li>Display Jewellery hub by type</li>
                  <li>Display Stock</li> -->
                </ul>
                 <li data-toggle="collapse" data-target="#temple" class="collapsed">
                  <a href="#"><i class="fa fa-car fa-lg"></i> Temple <span class="arrow">
                    
                  </span></a>
                </li>

                 <ul class="sub-menu collapse" id="temple">
                  <li>Post Temple Details</li>
                <!--   <li>Display Jewellery hub by type</li>
                  <li>Display Stock</li> -->
                </ul>

               
                 <li>
                  <a href="#">
                  <i class="fa fa-users fa-lg"></i> Users
                  </a>
                </li>
            </ul>
     </div>
</div>









 <div class="container-fluid"> 
  <div class="col-md-3">
  </div>

  <div class="col-md-9">
  
      <br>
<div class="bs-form col-md-8">
  <div class="title">
    <h1 class="text-center"> News and Events</h1>
  </div>
    <form class="form-horizontal" id="msgfrm">

 <div class="form-group">
 <label class="control-label col-xs-3" for="firstName">Category</label>
<div class="col-xs-6">
                <select class="form-control" id="category">
                    <option>Select Category</option>
                    <option>Entertainment</option>
                     <option>Sports</option>
                    <option>Marriage</option>
                      <option>New Offers</option>
                        <option>Demise</option>
                       
                    <!-- <option>Select Category</option> -->
                </select>
            </div>
          </div>


       

         

        <div class="form-group">
            <label class="control-label col-xs-3" for="Headline">Headline:</label>
            <div class="col-xs-6">
                <input type="text" class="form-control" id="headline"  placeholder="Headline" required>
            </div>
        </div>
   
        <div class="form-group">
            <label class="control-label col-xs-3" for="Content1">Content1</label>
            <div class="col-xs-6">
                <textarea rows="3" class="form-control" id="content1" placeholder="Content1" required></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-3" for="Content2">Content2</label>
            <div class="col-xs-6">
                <textarea rows="3" class="form-control" id="content2"  placeholder="Content2" required></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-3" for="Headline">Upload  Image:</label>
            <div class="col-xs-6">
                
                <input type='file' onchange="readURL(this);" id="pic" />
                                       
            </div>
        </div>
       
        
           
      
        
        
        <br>
        <div class="form-group">
            <div class="col-xs-offset-3 col-xs-6">
                <input type="submit" class="btn btn-primary" value="Submit">
                <input type="reset" class="btn btn-default" value="Reset">
            </div>
        </div>
    </form>
</div> 
<div  id="alert" style="display:none;">
    <strong class="alert alert-success text-center" >Successfully Added News!</strong> 
  </div>
<div class="clearfix">
</div>

 



<div class="bs-form col-md-8">
  <div class="title">
    <h1 class="text-center"> Jewellery Hub
   </h1>
  </div>
<form class="form-horizontal" id="jewelleryfrm">
  <input type="hidden" name="userid1" value="1" id="userid">
 <div class="form-group">
 <label class="control-label col-xs-3" for="firstName">type</label>
<div class="col-xs-6">
                <select class="form-control" id="type">
                    <option>Select Category</option>
                    <option>Shops</option>
                     <option>Vendors</option>
                    <option>Workers</option>
                      
                  </select>
            </div>
          </div>
       <div class="form-group">
            <label class="control-label col-xs-3" for="Name">Name:</label>
            <div class="col-xs-6">
              <input type="text" class="form-control" id="name"  placeholder="Name" required>
            </div>
        </div>
   
        <div class="form-group">
            <label class="control-label col-xs-3" for="Shop Name">Shop Name</label>
            <div class="col-xs-6">
               <input type="text" class="form-control" id="shop-name"  placeholder="Shop Name" required>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-3" for="HomeNumber">Home Number</label>
            <div class="col-xs-6">
               <input type="text" class="form-control" id="homenumber"  placeholder="Headline" required>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-xs-3" for="Address">Address</label>
            <div class="col-xs-6">
                <textarea rows="3" class="form-control" id="address"  placeholder="Content2" required></textarea>
            </div>
        </div>

         <div class="form-group">
            <label class="control-label col-xs-3" for="FatherName">Father Name</label>
            <div class="col-xs-6">
               <input type="text" class="form-control" id="fathername"  placeholder="Headline" required>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-3" for="Contact Number">Contact Number</label>
            <div class="col-xs-6">
               <input type="text" class="form-control" id="contact-no"  placeholder="Headline" required>
            </div>
        </div>
         <div class="form-group">
            <label class="control-label col-xs-3" for="Quantity"> Quantity</label>
            <div class="col-xs-6">
               <input type="text" class="form-control" id="quantity"  placeholder="Headline" required>
            </div>
        </div>
         <div class="form-group">
            <label class="control-label col-xs-3" for="Quality">Quality </label>
            <div class="col-xs-6">
               <input type="text" class="form-control" id="quality"  placeholder="Headline" required>
            </div>
        </div>

         <div class="form-group">
            <label class="control-label col-xs-3" for="Content">Content </label>
            <div class="col-xs-6">
               <textarea rows="3" class="form-control" id="jewellerycontent"  placeholder="Content2" required></textarea>
            </div>
        </div>

<div class="form-group">
            <label class="control-label col-xs-3" for="jewelleryType">jewelleryType </label>
            <div class="col-xs-6">
               <input type="text" class="form-control" id="jewellerytype"  placeholder="jewelleryType" required>
            </div>
        </div>
         <div class="form-group">
            <label class="control-label col-xs-3" for="StockDetails">Stock Details </label>
            <div class="col-xs-6">
               <textarea rows="3" class="form-control" id="stock-details"  placeholder="Stock Details" required></textarea>
            </div>
        </div>

        <div class="form-group">
      <label for="photo" class="col-sm-2 control-label">Upload</label>
      <div class="col-sm-10">
        <input type="file" class="form-control" name="photo" id="photo" accept=".png, .jpg, .jpeg" onchange="readFile(this);" multiple>
      </div>
    </div>
     <div id="status"></div>
  <div id="photos" class="row"></div> 
        <br>
        <div class="form-group">
            <div class="col-xs-offset-3 col-xs-6">
                <input type="submit" class="btn btn-primary" value="Submit">
                <input type="reset" class="btn btn-default" value="Reset">
            </div>
        </div>
    </form>
</div> 
<div  id="alert1" style="display:none;">
    <strong class="alert alert-success text-center" >Successfully Added Jewellery Hub!</strong> 
  </div>
</div>
</div> <br> <br>










<script>
  function readFile(input) {
    $("#status").html('Processing...');
    counter = input.files.length;
    for(x = 0; x<counter; x++){
      if (input.files && input.files[x]) {

        var reader = new FileReader();

        reader.onload = function (e) {

          var a =reader.result;
         var base64result = reader.result.split(',')[1];
              console.log( base64result);
sessionStorage.setItem('jewelleryimage',base64result);

          $("#photos").append('<div class="col-md-3 col-sm-3 col-xs-3"><img src="'+e.target.result+'" class="img-thumbnail"></div>');
        };

        reader.readAsDataURL(input.files[x]);
      }
    }
    if(counter == x){$("#status").html('');}
  }



   
</script>









</script>






                  
                
   
<script type="text/javascript">
  alert("entering");
sessionStorage.setItem('base64',"");

function readURL(element) {
         var file = element.files[0];
         var reader = new FileReader();
         reader.onloadend = function() {
               
         var a =reader.result;
         var base64result = reader.result.split(',')[1];

         // window.a1=base64result;
         console.log( base64result);
         //alert(base64result);
      
         //alert(base64);
         sessionStorage.setItem('newsimage',base64result);
      }
        reader.readAsDataURL(file);
        // alert("leaving");
   }







</script>




<script type="text/javascript" src="{{asset('admin/js/addnews.js')}}"></script>


<!-- <script type="text/javascript" src="{{asset('career/js/career.js')}}"></script> -->

