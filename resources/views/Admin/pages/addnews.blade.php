
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
<!-- <meta name="csrf-token" content="{{ csrf_token() }}"> -->
</head>
<body>


    <div class="container py-5">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-center mb-5">Add News</h2>
                <div class="row">
                    <div class="col-md-6 mx-auto">
                        <div class="card border-secondary">
                            <div class="card-header">
                                <h3 class="mb-0 my-2">Adding News</h3>
                            </div>
                            <div class="card-body">
                                <form class="form" role="form" autocomplete="off" id="add">
                                   
                                  <div class="form-group">
                                        <label for="exampleSelect1">Select News category</label>
                                        <select class="form-control" name="category" id="cat">
                                          <option>Entertainment</option>
                                          <option>Sports</option>
                                            <option>Marriage</option>
                                              <option>New Offers</option>
                                                <option>Death</option>
                                         
                                      </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="inputEmail3">Headline</label>
                                    <input type="text" class="form-control" name="heading" id="hd" placeholder="Headline" required="">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputFile">Upload Image</label>
                                    <input type="file" class="form-control-file" name="photos" aria-describedby="fileHelp" id="ph"></div>
                                <div class="form-group">
                                    <label for="inputVerify3">Content1</label>
                                    <input type="text" class="form-control" name="content1" placeholder="Content1" required="" id="cnt1">
                                </div>
                                <div class="form-group">
                                    <label for="inputVerify3">Content2</label>
                                    <input type="text" class="form-control" name="content2" placeholder="Content2" required="" id="cnt2">
                                </div>
                                <div class="form-group">
                                    <button onclick="addNews(event)" type="submit" class="btn btn-success btn-lg">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--/row-->

        </div>
        <!--/col-->
    </div>
    <!--/row-->
</div>
<!--/container-->

<script type="text/javascript">
    
function addNews(e){
 
    e.preventDefault();
    var form=$(this).closest('form');
    console.log("1");
    console.log("2");  

 var formData= {
                category :$('#cat :selected').text(),
                heading  : $('#hd').val(),
                photos   : $('#ph').val(),
                content1 : $('#cnt1').val(),
                content2 : $('#cnt2').val()

}
console.log(formData);
       $.ajax({
        type:"POST",
        url:"/api/1.0/addNews",
        data : { data : formData },

         //dataType: 'json',
        success: function(data){
            console.log(data);
        },
        error: function(data){

        }
    });
   }

</script> 























<!--   <script>
   
    $(document).ready(function() {

        // Ajax for our form
        $('#add').on('submit', function(event){
            event.preventDefault();

            var formData = {
                 category   : $('input[name=category]').val(),
                heading   : $('input[name=heading]').val(),
                content1 : $('input[name=content1]').val(),
            }

            $.ajax({
                type     : "POST",
               
                url      : "/api/1.0/addNews",
                data     : formData,
                cache    : false,

                success  : function(data) {
                    console.log(data);
                }
            })

            // console.log(formData);

            return false;

        alert($(this).attr('action'));

            alert('form is submited');
        });
    });
    </script>
 -->






</body>