<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <title>login</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  </head>

  <body>
    <center><h2><font color="red">Login</font></h2></center>
    <div class="container">
      <form action="/action_page.php" id="login">
        <div class="form-group">
          <label for="email">Email:</label>
          <input type="email" class="form-control" placeholder="Enter email"  id="emailid">
        </div>

        <div class="form-group">
          <label for="pwd">Password:</label>
          <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pwd">
        </div>

        <div class="checkbox">
          <label><input type="checkbox" name="remember">Remember me</label>
        </div>

        <button type="submit" class="btn btn-default" onclick="addlogin(event)">Submit</button>
      </form>
    </div>

    <script type="text/javascript">
    
    function addlogin(e){
      alert( "entered");
      e.preventDefault();
      var form=$('#login').closest('form');
      var formData= {
        emailId : $('#emailid').val(),
        password : $('#pwd').val(),
      }
      console.log(formData);

      $.ajax({

        type:"POST",
        url:"/api/1.0/login",
        data : { data : formData },
       
        success: function(data){
          var token=data.result.token;
          var userId=data.result.userData.userId;
          var firstName=data.result.userData.firstName;
           
          sessionStorage.setItem('userId', userId);
          sessionStorage.setItem('firstName', firstName);
          console.log(userId);
          sessionStorage.setItem('token', data.result.token);
           // sessionStorage.setItem('userId', data.result.userData.userId);
          console.log("name    " +token);
          alert("User profile Submitted successfully");
          //return false;
          window.location = 'chat';
        },

        error: function(data){

          alert("user is not added, please fill all the fileds with valid data");
          $('#login').trigger("reset");
        }
      });
    }
    </script>
  </body>
</html>


