
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title></title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">


<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">




    <style>
        
  .modal-header, .modal-body, .modal-footer{
    padding: 25px;
  }
  .modal-footer{
    text-align: center;
    margin-left: 384px;
color:#fff;
background: rgba(119, 109, 98,0.2);
  }
  #signup-modal-content, #forgot-password-modal-content{
    display: none;
  }
      
  /** validation */
      
  input.parsley-error   {    
      border-color:#843534;
      box-shadow: none;
  }
  input.parsley-error:focus{    
      border-color:#843534;
      box-shadow:inset 0 1px 1px rgba(0,0,0,.075),0 0 6px #ce8483
  }
  .parsley-errors-list.filled {
      opacity: 1;
      color: #a94442;
      display: none;
  }

.modal-content

{
background-image:url("bg.png");
width:850px;

}


.main-header {
    position: relative;
    left: 0px;
    
    z-index: 999;
    width: 100%;
    transition: all 500ms ease;
    -moz-transition: all 500ms ease;
    -webkit-transition: all 500ms ease;
    -ms-transition: all 500ms ease;
    -o-transition: all 500ms ease;
    
}

.main-header .header-top {
    position: relative;
    background: #2c3038;
    color: #555555;
    z-index: 5;
    background: #DAA520;
    height: 28px;
}

.auto-container {
    position: static;
    max-width: 1200px;
    padding: 0px 15px;
    margin: 0 auto;
        margin-bottom: 0px;
    margin-bottom: 0px;
    margin-bottom: 2px;
}

.main-header .header-top .top-left {
    position: relative;
    float: left;
    padding: 6px 0px;
    color: #ffffff;
    font-size: 12px;
}

.main-header .header-top ul li {
    position: relative;
    float: left;
    font-size: 14px;
   
    margin-right: 20px;
    color: #fff;
    letter-spacing: 1px;
    font-family: roboto;
    font-weight: 600;
    list-style-type: none;

}


.main-header .header-top .top-right {
    position: relative;
    padding: 4px 0px;
    float: right;
}


.main-header .header-top .top-right ul li {
    margin-right: 0px;
    margin-left: 25px;
   margin-top: 0px;
}


#flipkart-navbar {
  border-bottom: solid 1px #f0ece3;
    color: #FFFFFF;
}

.row1{
    padding-top: 10px;
}

.row2 {
    padding-bottom: 20px;
}

.flipkart-navbar-input {
    padding: 11px 16px;
    border-radius: 2px 0 0 2px;
    border: 0 none;
    outline: 0 none;
    font-size: 15px;
    color: black;
    background: #f3f7f8;
}

.flipkart-navbar-button {
   background-color: #c4941d;
    border: 1px solid #ffe11b;
    border-radius: 0 2px 2px 0;
    color: #565656;
    padding: 10px 0;
    height: 43px;
    cursor: pointer;
}

.cart-button {
    background-color: #2469d9;
    box-shadow: 0 2px 4px 0 rgba(0, 0, 0, .23), inset 1px 1px 0 0 hsla(0, 0%, 100%, .2);
    padding: 10px 0;
    text-align: center;
    height: 41px;
    border-radius: 2px;
    font-weight: 500;
    width: 120px;
    display: inline-block;
    color: #FFFFFF;
    text-decoration: none;
    color: inherit;
    border: none;
    outline: none;
     background-color: #daa520;
}

.cart-button:hover{
    text-decoration: none;
    color: #fff;
    cursor: pointer;

}

.cart-svg {
    display: inline-block;
    width: 16px;
    height: 16px;
    vertical-align: middle;
    margin-right: 8px;
}

.item-number {
    border-radius: 3px;
    background-color: rgba(0, 0, 0, .1);
    height: 20px;
    padding: 3px 6px;
    font-weight: 500;
    display: inline-block;
    color: #fff;
    line-height: 12px;
    margin-left: 10px;
}

.upper-links {
    display: inline-block;
    padding: 0 11px;
    line-height: 23px;
    font-family: 'Roboto', sans-serif;
    letter-spacing: 0;
    color: inherit;
    border: none;
    outline: none;
    font-size: 12px;
}

.dropdown {
    position: relative;
    display: inline-block;
    margin-bottom: 0px;
}

.dropdown:hover {
    background-color: #fff;
}

.dropdown:hover .links {
    color: #000;
}

.dropdown:hover .dropdown-menu {
    display: block;
}

.dropdown .dropdown-menu {
    position: absolute;
    top: 100%;
    display: none;
    background-color: #fff;
    color: #333;
    left: 0px;
    border: 0;
    border-radius: 0;
    box-shadow: 0 4px 8px -3px #555454;
    margin: 0;
    padding: 0px;
}

.links {
    color: #fff;
    text-decoration: none;
}

.links:hover {
    color: #fff;
    text-decoration: none;
}

.profile-links {
    font-size: 12px;
    font-family: 'Roboto', sans-serif;
    border-bottom: 1px solid #e9e9e9;
    box-sizing: border-box;
    display: block;
    padding: 0 11px;
    line-height: 23px;
}

.profile-li{
    padding-top: 2px;
}

.largenav {
    display: none;
}

.smallnav{
    display: block;
}

.smallsearch{
    margin-left: 15px;
    margin-top: 15px;
}

.menu{
    cursor: pointer;
}

@media screen and (min-width: 768px) {
    .largenav {
        display: block;
    }
    .smallnav{
        display: none;
    }
    .smallsearch{
        margin: 0px;
    }
}

/*Sidenav*/
.sidenav {
    height: 100%;
    width: 0;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 0;
    background-color: #fff;
    overflow-x: hidden;
    transition: 0.5s;
    box-shadow: 0 4px 8px -3px #555454;
    padding-top: 0px;
}

.sidenav a {
    padding: 8px 8px 8px 32px;
    text-decoration: none;
    font-size: 25px;
    color: #818181;
    display: block;
    transition: 0.3s
}

.sidenav .closebtn {
    position: absolute;
    top: 0;
    right: 25px;
    font-size: 36px;
    margin-left: 50px;
    color: #fff;        
}

@media screen and (max-height: 450px) {
  .sidenav a {font-size: 18px;}
}

.sidenav-heading{
    font-size: 36px;
    color: #fff;
}






/*
Fade content bs-carousel with hero headers
Code snippet by maridlcrmn (Follow me on Twitter @maridlcrmn) for Bootsnipp.com
Image credits: unsplash.com
*/

/********************************/
/*       Fade Bs-carousel       */
/********************************/
.fade-carousel {
    position: relative;
    height: 350px;
}
.fade-carousel .carousel-inner .item {
    height: 350px;
}
.fade-carousel .carousel-indicators > li {
    margin: 0 2px;
    background-color: #f39c12;
    border-color: #f39c12;
    opacity: .7;
}
.fade-carousel .carousel-indicators > li.active {
  width: 10px;
  height: 10px;
  opacity: 1;
}

/********************************/
/*          Hero Headers        */
/********************************/
.hero {
    position: absolute;
    top: 50%;
    left: 50%;
    z-index: 3;
    color: #fff;
    text-align: center;
    text-transform: uppercase;
    text-shadow: 1px 1px 0 rgba(0,0,0,.75);
      -webkit-transform: translate3d(-50%,-50%,0);
         -moz-transform: translate3d(-50%,-50%,0);
          -ms-transform: translate3d(-50%,-50%,0);
           -o-transform: translate3d(-50%,-50%,0);
              transform: translate3d(-50%,-50%,0);
}
.hero h1 {
    font-size: 6em;    
    font-weight: bold;
    margin: 0;
    padding: 0;
}

.fade-carousel .carousel-inner .item .hero {
    opacity: 0;
    -webkit-transition: 2s all ease-in-out .1s;
       -moz-transition: 2s all ease-in-out .1s; 
        -ms-transition: 2s all ease-in-out .1s; 
         -o-transition: 2s all ease-in-out .1s; 
            transition: 2s all ease-in-out .1s; 
}
.fade-carousel .carousel-inner .item.active .hero {
    opacity: 1;
    -webkit-transition: 2s all ease-in-out .1s;
       -moz-transition: 2s all ease-in-out .1s; 
        -ms-transition: 2s all ease-in-out .1s; 
         -o-transition: 2s all ease-in-out .1s; 
            transition: 2s all ease-in-out .1s;    
}

/********************************/
/*            Overlay           */
/********************************/
.overlay {
    position: absolute;
    width: 100%;
    height: 350px;
    z-index: 2;
    background-color: #080d15;
    opacity: .7;
}

/********************************/
/*          Custom Buttons      */
/********************************/
.btn.btn-lg {padding: 10px 40px;}
.btn.btn-hero,
.btn.btn-hero:hover,
.btn.btn-hero:focus {
    color: #f5f5f5;
    background-color: #1abc9c;
    border-color: #1abc9c;
    outline: none;
    margin: 20px auto;
}

/********************************/
/*       Slides backgrounds     */
/********************************/
.fade-carousel .slides .slide-1, 
.fade-carousel .slides .slide-2,
.fade-carousel .slides .slide-3 {
  height: 100vh;
  background-size: cover;
  background-position: center center;
  background-repeat: no-repeat;
}
.fade-carousel .slides .slide-1 {
  background-image: url(https://ununsplash.imgix.net/photo-1416339134316-0e91dc9ded92?q=75&fm=jpg&s=883a422e10fc4149893984019f63c818); 
  height:350px;
}
.fade-carousel .slides .slide-2 {
  background-image: url(https://ununsplash.imgix.net/photo-1416339684178-3a239570f315?q=75&fm=jpg&s=c39d9a3bf66d6566b9608a9f1f3765af);
  height:350px
}
.fade-carousel .slides .slide-3 {
  background-image: url(https://ununsplash.imgix.net/photo-1416339276121-ba1dfa199912?q=75&fm=jpg&s=9bf9f2ef5be5cb5eee5255e7765cb327);
  height:350px
}

/********************************/
/*          Media Queries       */
/********************************/
@media screen and (min-width: 980px){
    .hero { width: 980px; }    
}
@media screen and (max-width: 640px){
    .hero h1 { font-size: 4em; }    
}









.offer
{
  text-align: center;
}












    </style>

  </head>

  <body>

    <!-- Fixed navbar -->
<div cass="clearfix"> </div>


<header class="main-header">
    <div class="header-top">
          <div class="auto-container clearfix">
              <!-- Top Left -->
              <div class="top-left">
                            <ul class="clearfix">
                      <li><strong> <i class="fa fa-phone" aria-hidden="true"></i></strong>  Call Us : 04546254540</li>
                        <li><strong><i class="fa fa-envelope" aria-hidden="true"></i></strong>  Email : connect@ </li>
                         <li><strong> <i class="fa fa-question-circle" aria-hidden="true"></i>
</strong> <a style="color:#fff;" href="mailto:connect@kothaiandalchits.com"> Have any Questions ? </a></li>
                    </ul>
                          </div>
                <!-- Top Right -->
                <div class="top-right">
                <div class="gk">
          <ul class="clearfix">
                      <li><a href="index.aspx"> <i class="fa fa-home" aria-hidden="true"></i>
 Home</a></li> 
                                  


 <li><a href="javascript:void(0)" data-toggle="modal" data-target="#login-signup-modal"> <i class="fa fa-sign-in" aria-hidden="true"></i> Login/Signup</a></li>
                                                
                                               
                                </ul> 
                                            </div>
                </div>
            </div>
        </div><!-- Header Top End -->

        </header>





<div id="flipkart-navbar">
    <div class="container">
        <div class="row row1">
            
            <!-- <ul class="largenav pull-right">
                <li class="upper-links"><a class="links" href="http://clashhacks.in/">Link 1</a></li>
                <li class="upper-links"><a class="links" href="https://campusbox.org/">Link 2</a></li>
                <li class="upper-links"><a class="links" href="http://clashhacks.in/">Link 3</a></li>
                <li class="upper-links"><a class="links" href="http://clashhacks.in/">Link 4</a></li>
                <li class="upper-links"><a class="links" href="http://clashhacks.in/">Link 5</a></li>
                <li class="upper-links"><a class="links" href="http://clashhacks.in/">Link 6</a></li>
                
                <li class="upper-links">
                    <a class="links" href="http://clashhacks.in/">
                        <svg class="" width="16px" height="12px" style="overflow: visible;">
                            <path d="M8.037 17.546c1.487 0 2.417-.93 2.417-2.417H5.62c0 1.486.93 2.415 2.417 2.415m5.315-6.463v-2.97h-.005c-.044-3.266-1.67-5.46-4.337-5.98v-.81C9.01.622 8.436.05 7.735.05 7.033.05 6.46.624 6.46 1.325v.808c-2.667.52-4.294 2.716-4.338 5.98h-.005v2.972l-1.843 1.42v1.376h14.92v-1.375l-1.842-1.42z" fill="#fff"></path>
                        </svg>
                    </a>
                </li>



                <li class="upper-links dropdown"><a class="links" href="http://clashhacks.in/">Dropdown</a>
                    <ul class="dropdown-menu">
                        <li class="profile-li"><a class="profile-links" href="http://yazilife.com/">Link</a></li>
                        <li class="profile-li"><a class="profile-links" href="http://hacksociety.tech/">Link</a></li>
                        <li class="profile-li"><a class="profile-links" href="http://clashhacks.in/">Link</a></li>
                        <li class="profile-li"><a class="profile-links" href="http://clashhacks.in/">Link</a></li>
                        <li class="profile-li"><a class="profile-links" href="http://clashhacks.in/">Link</a></li>
                        <li class="profile-li"><a class="profile-links" href="http://clashhacks.in/">Link</a></li>
                        <li class="profile-li"><a class="profile-links" href="http://clashhacks.in/">Link</a></li>
                    </ul>
                </li>
            </ul>
          -->
        </div>
        <div class="row row2">
            <div class="col-sm-2">
                <h2 style="margin:0px;"><span class="smallnav menu" onclick="openNav()">☰ Brand</span></h2>
                <h1 style="margin:0px; color: goldenrod;"><span class="largenav">Logo</span></h1>
            </div>
            <div class="flipkart-navbar-search smallsearch col-sm-8 col-xs-11">
                <div class="row">
                    <input class="flipkart-navbar-input col-xs-11" type="" placeholder="Search for Products, Brands and more" name="">
                    <button class="flipkart-navbar-button col-xs-1">
                        <svg width="15px" height="15px">
                            <path d="M11.618 9.897l4.224 4.212c.092.09.1.23.02.312l-1.464 1.46c-.08.08-.222.072-.314-.02L9.868 11.66M6.486 10.9c-2.42 0-4.38-1.955-4.38-4.367 0-2.413 1.96-4.37 4.38-4.37s4.38 1.957 4.38 4.37c0 2.412-1.96 4.368-4.38 4.368m0-10.834C2.904.066 0 2.96 0 6.533 0 10.105 2.904 13 6.486 13s6.487-2.895 6.487-6.467c0-3.572-2.905-6.467-6.487-6.467 "></path>
                        </svg>
                    </button>
                </div>
            </div>
            <div class="cart largenav col-sm-2">
                <a class="cart-button">
                    <svg class="cart-svg " width="16 " height="16 " viewBox="0 0 16 16 ">
                        <path d="M15.32 2.405H4.887C3 2.405 2.46.805 2.46.805L2.257.21C2.208.085 2.083 0 1.946 0H.336C.1 0-.064.24.024.46l.644 1.945L3.11 9.767c.047.137.175.23.32.23h8.418l-.493 1.958H3.768l.002.003c-.017 0-.033-.003-.05-.003-1.06 0-1.92.86-1.92 1.92s.86 1.92 1.92 1.92c.99 0 1.805-.75 1.91-1.712l5.55.076c.12.922.91 1.636 1.867 1.636 1.04 0 1.885-.844 1.885-1.885 0-.866-.584-1.593-1.38-1.814l2.423-8.832c.12-.433-.206-.86-.655-.86 " fill="#fff "></path>
                    </svg> Cart
                    <span class="item-number ">0</span>
                </a>
            </div>
        </div>
    </div>
</div>
<div id="mySidenav" class="sidenav">
    <div class="container" style="background-color: #2874f0; padding-top: 10px;">
        <span class="sidenav-heading">Home</span>
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">×</a>
    </div>
    <a href="http://clashhacks.in/">Link</a>
    <a href="http://clashhacks.in/">Link</a>
    <a href="http://clashhacks.in/">Link</a>
    <a href="http://clashhacks.in/">Link</a>
</div>


<script type="text/javascript">
  
function openNav() {
    document.getElementById("mySidenav").style.width = "70%";
    // document.getElementById("flipkart-navbar").style.width = "50%";
    document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.body.style.backgroundColor = "rgba(0,0,0,0)";
}

</script>





    
    <!-- Bootstrap Modal -->
    
    
    <!--Login, Signup, Forgot Password Modal -->


   

    

    <div id="login-signup-modal" class="modal fade" tabindex="-1" role="dialog">
      
       
     
    
      <div class="modal-dialog" role="document">
    
      <!-- login modal content -->
        <div class="modal-content" id="login-modal-content">
        
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" style="color: #ffff; text-align:center; margin-left: 429px;"><span class="glyphicon glyphicon-lock" ></span> Login Now!</h4>
      </div>
        
<div class="col-md-6"> hii </div>
        <div class="col-md-6"> 
        <div class="modal-body">
          <form method="post" id="Login-Form" role="form">
          
            <div class="form-group">
                <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></div>
                <input name="email" id="email" type="email" class="form-control input-lg" placeholder="Enter Email" required data-parsley-type="email" >
                </div>                      
            </div>
            
            
            <div class="form-group">
                <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
                <input name="password" id="login-password" type="password" class="form-control input-lg" placeholder="Enter Password" required data-parsley-length="[6, 10]" data-parsley-trigger="keyup">
                </div>                      
            </div>
            
            <div class="checkbox">
              <label style="color: #fff;"><input type="checkbox" value="" checked>Remember me</label>
            </div>
              <button type="submit" class="btn btn-success btn-block btn-lg" style="background:rgba(119, 109, 98,0.2); border-color:rgba(119, 109, 98,0.2);">LOGIN</button>
          </form>
        </div>
</div>



        
        <div class="modal-footer">
          <p >
          <a id="FPModal" href="javascript:void(0)" style="color: #fff;">Forgot Password?</a> | 
          <a id="signupModal" href="javascript:void(0)" style="color: #fff;">Register Here!</a>
          </p>
        </div>
        
       </div>
        <!-- login modal content -->
        
        <!-- signup modal content -->
        



        <div class="modal-content" id="signup-modal-content">
        
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" style="text-align: center;
margin-left: 401px;
color: #fff"><span class="glyphicon glyphicon-lock"></span> Signup Now!</h4>
      </div>
               
                <div class="col-md-6"> hii </div> 

                 <div class="col-md-6">  
       <div class="modal-body">
          <form method="post" id="Signin-Form" role="form">
          
            <div class="form-group">
                <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></div>
                <input name="email" id="email" type="email" class="form-control input-lg" placeholder="Enter Email" required data-parsley-type="email">
                </div>                     
            </div>
            
            <div class="form-group">
                <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
                <input name="password" id="passwd" type="password" class="form-control input-lg" placeholder="Enter Password" required data-parsley-length="[6, 10]" data-parsley-trigger="keyup">
                </div>                      
            </div>
            
            <div class="form-group">
                <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
                <input name="password" id="confirm-passwd" type="password" class="form-control input-lg" placeholder="Retype Password" required data-parsley-equalto="#passwd" data-parsley-trigger="keyup">
                </div>                      
            </div>
            
            
              <button type="submit" class="btn btn-success btn-block btn-lg" style="background: rgba(119, 109, 98,0.2);
border-color: rgba(119, 109, 98,0.2);">CREATE ACCOUNT!</button>
          </form>
        </div>
        </div> 

        <div class="modal-footer">
          <p>Already a Member ? <a id="loginModal" href="javascript:void(0)" style="color:#fff;">Login Here!</a></p>
        </div>
        
      </div>

        <!-- signup modal content -->
        
        <!-- forgot password content -->
         <div class="modal-content" id="forgot-password-modal-content">
        
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" style="text-align: center;
margin-left: 401px;
color: #fff;"><span class="glyphicon glyphicon-lock"></span> Recover Password!</h4>
      </div>
         <div class="col-md-6"> hii </div> 

          <div class="col-md-6">   
        <div class="modal-body">
          <form method="post" id="Forgot-Password-Form" role="form">
          
            <div class="form-group">
                <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></div>
                <input name="email" id="email" type="email" class="form-control input-lg" placeholder="Enter Email" required data-parsley-type="email">
                </div>                     
            </div>
                        
            <button type="submit" class="btn btn-success btn-block btn-lg" style="background: rgba(119, 109, 98,0.2);
border-color: rgba(119, 109, 98,0.2);">
              <span class="glyphicon glyphicon-send"></span> SUBMIT
            </button>
          </form>
        </div>
        </div>
        
        <div class="modal-footer">
          <p>Remember Password ? <a id="loginModal1" href="javascript:void(0)" style="color:#fff;">Login Here!</a></p>
        </div>
        
      </div>        
      

    
    
      </div>
     
      
    </div>


<div class="clearfix">


</div>


<!------ Include the above in your HEAD tag ---------->

<div class="container-fluid" style="margin-left: 0px !important;
padding-left: 0px !important;" > 
  <div class="col-md-8" style=" margin-left: 0px !important;
padding-left: 0px !important;
margin-right: 0px !important;
padding-right: 0px !important;"> 
<div class="carousel fade-carousel slide" data-ride="carousel" data-interval="4000" id="bs-carousel">
  <!-- Overlay -->
  <div class="overlay"></div>

  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#bs-carousel" data-slide-to="0" class="active"></li>
    <li data-target="#bs-carousel" data-slide-to="1"></li>
    <li data-target="#bs-carousel" data-slide-to="2"></li>
  </ol>
  
  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item slides active">
      <div class="slide-1"></div>
      <div class="hero">
        <hgroup>
            <h1>We are creative</h1>        
            <h3>Get start your next awesome project</h3>
        </hgroup>
        <button class="btn btn-hero btn-lg" role="button">See all features</button>
      </div>
    </div>
    <div class="item slides">
      <div class="slide-2"></div>
      <div class="hero">        
        <hgroup>
            <h1>We are smart</h1>        
            <h3>Get start your next awesome project</h3>
        </hgroup>       
        <button class="btn btn-hero btn-lg" role="button">See all features</button>
      </div>
    </div>
    <div class="item slides">
      <div class="slide-3"></div>
      <div class="hero">        
        <hgroup>
            <h1>We are amazing</h1>        
            <h3>Get start your next awesome project</h3>
        </hgroup>
        <button class="btn btn-hero btn-lg" role="button">See all features</button>
      </div>
    </div>
  </div> 
</div>


</div>

<div class="col-md-4" style="
padding-left: 0px !important;

padding-right: 0px !important;">
  <div class="offer" id="slider">  
    <h3> Best offers </h3></div>



       <div class="container">
    <!-- slider -->
    <div class="row">
        <div class="col-md-9" id="slider">
            <!-- Top part of the slider -->
            <div class="row">
                <div class="col-md-2" id="carousel-bounding-box">
                    <div id="myCarousel" class="carousel slide">
                        <!-- Carousel items -->
                        <div class="carousel-inner">
                            <div class="active item" data-slide-number="0">
                                <img class="img-rounded img-responsive" src="http://lorempixel.com/120/100">
                            </div>
                            <div class="item" data-slide-number="1">
                                <img class="img-rounded img-responsive" src="http://lorempixel.com/120/100/technics/1">
                            </div>
                            <div class="item" data-slide-number="2">
                                <img class="img-rounded img-responsive" src="http://lorempixel.com/120/100/business/1">
                            </div>
                            <div class="item" data-slide-number="3">
                                <img class="img-rounded img-responsive" src="http://lorempixel.com/120/100/city">
                            </div>
                            <div class="item" data-slide-number="4">
                                <img class="img-rounded img-responsive" src="http://lorempixel.com/120/100/city/1">
                            </div>
                            <div class="item" data-slide-number="5">
                                <img class="img-rounded img-responsive" src="http://lorempixel.com/120/100">
                            </div>
                        </div><!--/carousel-inner-->
                    </div><!--/carousel-->
                    <ul class="carousel-controls-mini list-inline text-center">
                      <li><a href="#myCarousel" data-slide="prev">‹</a></li>
                      <li><a href="#myCarousel" data-slide="next">›</a></li>
                    </ul><!--/carousel-controls-->
              </div><!--/col-->
              <div class="col-md-4" id="carousel-text"></div>
              <div style="display:none;" id="slide-content">
                    <div id="slide-content-0">
                         <h5>Slide One</h5>

                        <p>This is mini slider / carousel.</p> <small>October 13 2013 - <a href="#">Read more</a></small>

                    </div>
                    <div id="slide-content-1">
                         <h5>Slide Two</h5>

                        <p>Mini carousel with Bootstrap</p> <small>October 15 2013 - <a href="#">Read more</a></small>

                    </div>
                    <div id="slide-content-2">
                         <h5>Slider Three</h5>

                        <p>Facebook-style paged image slider</p> <small>October 19 2013 - <a href="#">Read more</a></small>

                    </div>
                    <div id="slide-content-3">
                         <h5>Slider Four</h5>

                        <p>Lorem Ipsum Dolor</p> <small>October 22 2013 - <a href="#">Read more</a></small>

                    </div>
                    <div id="slide-content-4">
                         <h5>Slider Five</h5>

                        <p>Lorem Ipsum Dolor</p> <small>October 25 2013 - <a href="#">Read more</a></small>

                    </div>
                    <div id="slide-content-5">
                         <h5>Slider Six</h5>

                        <p>Lorem Ipsum Dolor</p>
                        <p class="sub-text">October 24 2012 - <a href="#">Read more</a>
                        </p>
                    </div>
              </div><!--/slide-content-->
            </div><!--/row-->
        </div><!--/col-->
    </div><!--/row slider-->
</div>
   



<script type="text/javascript">
  
$('#myCarousel').carousel({
  interval: 5000
});

$('#carousel-text').html($('#slide-content-0').html());

// When the carousel slides, auto update the text
$('#myCarousel').on('slid.bs.carousel', function (e) {
  var id = $('.item.active').data('slide-number');
  $('#carousel-text').html($('#slide-content-'+id).html());
});

</script>




</div>

</div>






    

</body>










        <!--Login, Signup, Forgot Password Modal -->
              
        
  <!-- Bootstrap Modal -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.4.4/parsley.min.js"></script>
    
    <script>
    $(document).ready(function(){
        
    $('#Login-Form').parsley();
    $('#Signin-Form').parsley();
    $('#Forgot-Password-Form').parsley();
        
    $('#signupModal').click(function(){             
      $('#login-modal-content').fadeOut('fast', function(){
         $('#signup-modal-content').fadeIn('fast');
      });
    });
                
    $('#loginModal').click(function(){                
      $('#signup-modal-content').fadeOut('fast', function(){
         $('#login-modal-content').fadeIn('fast');
      });
    });
          
    $('#FPModal').click(function(){             
      $('#login-modal-content').fadeOut('fast', function(){
         $('#forgot-password-modal-content').fadeIn('fast');
      });
    });
          
    $('#loginModal1').click(function(){               
      $('#forgot-password-modal-content').fadeOut('fast', function(){
         $('#login-modal-content').fadeIn('fast');
      });
    });
    
  });
    </script>




    
  </body>
</html>
