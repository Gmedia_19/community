
<!doctype html>
<html>
<head>
    @include('includes.head')
</head>
<body>
<div class="container-fluid"  >

    <header class="row">
        @include('includes.header')
    </header>

    </div>
<div class="container-fluid">
    <div id="main" class="row">

            @yield('content')

</div>
</div>

    <footer class="row">
        @include('includes.footer')
    </footer>

</div>
</body>
</html>

