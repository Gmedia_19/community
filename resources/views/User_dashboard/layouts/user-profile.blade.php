<!doctype html>
<html>
<head>
 @include('User_dashboard.includes.head') 
</head>
<body>
  <div class="container">
    <header class="row">
      @include('User_dashboard.includes.header') 
     </header>
   


  <!-- responsive tab section -->


</div> 

<footer class="row">
  @include('User_dashboard.includes.footer')

</footer>
<script type="text/javascript" src="{{asset('js/jquery-notification.js')}}"></script>
<script type="text/javascript" src={{ asset('/js/user/main.js') }}></script>
<script type="text/javascript" src={{ asset('js/tabs.js') }}></script>
</div>
</body>
</html>

