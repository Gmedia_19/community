 <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
<link href="{{asset('css/style.css')}}" rel="stylesheet">
<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> 

 <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/solid.js" integrity="sha384-+Ga2s7YBbhOD6nie0DzrZpJes+b2K1xkpKxTFFcx59QmVPaSA8c7pycsNaFwUK6l" crossorigin="anonymous"></script> 
<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/fontawesome.js" integrity="sha384-7ox8Q2yzO/uWircfojVuCQOZl+ZZBg2D2J5nkpLqzH1HY0C1dHlTKIbpRz/LG23c" crossorigin="anonymous"></script> 

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"> 





<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js "></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js "></script>
<![endif]-->
<!-- YOU NEED THIS REFERENCE TO BE ABLE TO GET FONTAWESOME 4 ICONS TO THIS SNIPP -->


<style>


.navbar-default .navbar-nav>li>a {
    color: #fff;
}

@media (min-width: 768px)
.navbar-right {
    
}

.navbar-collapse form[role="search"] {
    padding: 3px;
    margin: 0px;
    z-index: 0;
}

@media (min-width: 768px)
.navbar-right {
     float: right!important; 
}

.svg-inline--fa.fa-w-16 {
    width: 2em;
}



.navbar-default .navbar-nav>.active>a, .navbar-default .navbar-nav>.active>a:hover, .navbar-default .navbar-nav>.active>a:focus {
    color: #fff;
   
}

.nav-tabs>li>a {
    margin-right: 2px;
    line-height: 1.42857143;
    border: 1px solid transparent;
    
}



/* sidemenu */

.nav-side-menu {
  overflow: auto;
  font-family: verdana;
  font-size: 12px;
  font-weight: 200;
  background-color: #2e353d;
  position: fixed;
  top: 0px;
  width:197px;
  height: 80%;
  color: #e1ffff;
}
.nav-side-menu .brand {
  background-color: #23282e;
  line-height: 50px;
  display: block;
  text-align: center;
  font-size: 14px;
}
.nav-side-menu .toggle-btn {
  display: none;
}
.nav-side-menu ul,
.nav-side-menu li {
  list-style: none;
  padding: 0px;
  margin: 0px;
  line-height: 35px;
  cursor: pointer;
  /*    
    .collapsed{
       .arrow:before{
                 font-family: FontAwesome;
                 content: "\f053";
                 display: inline-block;
                 padding-left:10px;
                 padding-right: 10px;
                 vertical-align: middle;
                 float:right;
            }
     }
*/
}
.nav-side-menu ul :not(collapsed) .arrow:before,
.nav-side-menu li :not(collapsed) .arrow:before {
  font-family: FontAwesome;
  content: "\f078";
  display: inline-block;
  padding-left: 10px;
  padding-right: 10px;
  vertical-align: middle;
  float: right;
}
.nav-side-menu ul .active,
.nav-side-menu li .active {
  border-left: 3px solid #d19b3d;
  background-color: #4f5b69;
}
.nav-side-menu ul .sub-menu li.active,
.nav-side-menu li .sub-menu li.active {
  color: #d19b3d;
}
.nav-side-menu ul .sub-menu li.active a,
.nav-side-menu li .sub-menu li.active a {
  color: #d19b3d;
}
.nav-side-menu ul .sub-menu li,
.nav-side-menu li .sub-menu li {
  background-color: #181c20;
  border: none;
  line-height: 28px;
  border-bottom: 1px solid #23282e;
  margin-left: 0px;
}
.nav-side-menu ul .sub-menu li:hover,
.nav-side-menu li .sub-menu li:hover {
  background-color: #020203;
}
.nav-side-menu ul .sub-menu li:before,
.nav-side-menu li .sub-menu li:before {
  font-family: FontAwesome;
  content: "\f105";
  display: inline-block;
  padding-left: 10px;
  padding-right: 10px;
  vertical-align: middle;
}
.nav-side-menu li {
  padding-left: 0px;
  border-left: 3px solid #2e353d;
  border-bottom: 1px solid #23282e;
}
.nav-side-menu li a {
  text-decoration: none;
  color: #e1ffff;
}
.nav-side-menu li a i {
  padding-left: 10px;
  width: 20px;
  padding-right: 20px;
}
.nav-side-menu li:hover {
  border-left: 3px solid #d19b3d;
  background-color: #4f5b69;
  -webkit-transition: all 1s ease;
  -moz-transition: all 1s ease;
  -o-transition: all 1s ease;
  -ms-transition: all 1s ease;
  transition: all 1s ease;
}
@media (max-width: 767px) {
  .nav-side-menu {
    position: relative;
    width: 100%;
    margin-bottom: 10px;
  }
  .nav-side-menu .toggle-btn {
    display: block;
    cursor: pointer;
    position: absolute;
    right: 10px;
    top: 10px;
    z-index: 10 !important;
    padding: 3px;
    background-color: #ffffff;
    color: #000;
    width: 40px;
    text-align: center;
  }
  .brand {
    text-align: left !important;
    font-size: 22px;
    padding-left: 20px;
    line-height: 50px !important;
  }
}
@media (min-width: 767px) {
  .nav-side-menu .menu-list .menu-content {
    display: block;
  }
}

.comment_text
{
  resize: none;
       width: 95.75%;
    padding: 6px 2px 0px 7px;
    margin-top: 7px !important;
    height: 40px;
}







a:hover{
    text-decoration:none;    
}

.db-timeline-img img {
    width: 100%;
    border-radius: 4px 4px 0 0;
    -webkit-border-radius: 4px 4px 0 0;
}

.profile-thumb img {
    width: 140px;
    height: 140px;
    border-radius: 50%;
    -webkit-border-radius: 50%;
    margin-top: -90px;
    border: 3px solid #fff;
}

.profile-info .panel-footer ul li a {
    color: #7a7a7a;
}

.profile-thumb {
    float: left;
    position: relative;
}

.p-text-area, .p-text-area:focus {
    border: none;
    font-weight: 300;
    box-shadow: none;
    color: #c3c3c3;
    font-size: 16px;
}


.db-user-mail {
    margin: 10px 0 0 20px;
    display: inline-block;
}


.fb-name  {
    bottom: 5px;
    left: 175px;
    position: absolute;
}

.fb-name h2 a {
    color: #FFFFFF;
    text-rendering: optimizelegibility;
    text-shadow: 0 0 3px rgba(0, 0, 0, 0.8);
    font-size: 25px;
}

.db-user-thumb {
    float: left;
    width: 70px;
    margin-right:15px;
}

.db-user-thumb img {
    width: 70px;
    height: 70px;
    border-radius: 50%;
    -webkit-border-radius: 50%;
}


.db-user-details h3 {
    margin: 15px 0 0;
    font-size: 18px;
    font-weight: 300;
}

.db-user-details p {
    color: #c3c3c3;
}


.db-user-status {
    padding: 10px 0;
    line-height: 20px;
}

.db-time-action {
    padding: 15px 0;
}


.db-border {
    border-top:1px solid #ebeef5;
}

.db-time-action span, .fb-time-action a {
    margin-right: 5px;
}

.db-time-action a {
    color: #2972a1;
}

.db-time-action a:hover {
    text-decoration: underline;
}

.db-time-action span {
    color: #5a5a5a;
}

.db-status-container, .fb-comments li {
    margin: 0 -15px 0 -15px;
    padding: 0 15px;
}

.db-gray-bg {
    background: #f6f6f6;
}

.db-comments li {
    border-top:1px solid #ebeef5;
    padding: 15px;
}

.db-comments .cmt-thumb  {
    width: 50px;
    float: left;
    margin-right: 15px;
}

.db-comments .cmt-thumb img {
    width: 50px;
}

.db-comments .cmt-details {
    padding-top: 5px;
}


.db-comments .cmt-details a  {
    font-size: 14px;
    font-weight: bold;
}

.db-comments .cmt-details a.like-link {
    font-size: 12px;
    font-weight: normal;
}

.cmt-form {
    display: inline-block;
    width: 70%;
}

.cmt-form textarea{
    height: 50px;
    line-height: 35px;
}

.db-timeliner h2 {
    background: #828283;
    color: #fff;
    margin-top: 0;
    padding: 10px 15px;
    font-size: 16px;
    border-radius: 4px;
    -webkit-border-radius: 4px;
    font-weight: 300;
}

.db-timeliner ul {
    margin-left:15px;
    margin-bottom: 20px;
    list-style-type:none;
}

.db-comments{
    list-style-type:none;
}

.db-timeliner ul li {
    margin-bottom: 3px;
}

.db-timeliner ul li a{
    color: #999797;
    border-left: 4px solid #d3d7dd;
    padding-left:10px;
    padding-top: 3px;
    padding-bottom: 3px;
    display: block;
}

.db-timeliner ul li a:hover{
    color: #999797;
    border-left: 4px solid #b1b1b1;
    padding-left:10px;
}

.db-timeliner ul li.active a{
    color: #7a7a7a;
    border-left: 4px solid #7a7a7a;
    padding-left:10px;
}

.recent-highlight {
    background: #FF6C60 !important;
}
           

.img-post
{
      padding: 15px;
}



.gallery
{
    display: inline-block;
    margin-top: 20px;
}

    






/* .img-modal {
  display: block;
}
*/

.img-modal .modal-dialog {
    /* An arbitrary minimum height. Feel free to modify this one as well */
    min-height: 350px;
    height: 80%;
}

.img-modal .modal-content, .img-modal .modal-body, .img-modal .row, .img-modal .modal-image {
    height: 100%;
}

.modal-content {
    border-radius: 0;
}

.modal-body {
    padding-top: 0;
    padding-bottom: 0;
}

.modal-image {
    background: #000;
    padding :0;
}

.modal-image img {
    margin: 0 auto;
    max-height: 100%;
    max-width: 100%;
    
    position: relative;
    top: 50%;
    -webkit-transform: translateY(-50%);
    -ms-transform: translateY(-50%);
    transform: translateY(-50%);
}

.img-modal .img-modal-btn {
    display: block;
    position: absolute;
    top: 0;
    bottom: 0;
    background: black;
    opacity: 0;
    font-size: 1.5em;
    width: 45px;
    color: #fff;
    transition: opacity .2s ease-in;
}

.img-modal .modal-image:hover .img-modal-btn {
    opacity: 0.4;  
}

.img-modal .modal-image:hover .img-modal-btn:hover {
   opacity: 0.75;   
}

.img-modal .img-modal-btn.right {
    right: 0;
}

.img-modal .img-modal-btn i {
    position: absolute;
    top: 50%;
    left: 0;
    right: 0;
    text-align: center;
    margin-top: -.75em;
}

.img-modal .modal-meta {
    position: relative;   
    height: 100%;
}

.img-modal .modal-meta-top {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 45px;
    padding: 5px 10px;
    overflow-y: auto;
}

.img-modal .modal-meta-top .img-poster img {
    height: 70px;
    width: 70px;
    float: left;
    margin-right: 15px;
}

.img-modal .modal-meta-top .img-poster strong {
    display: block; 
    padding-top: 15px;
}

.img-modal .modal-meta-top .img-poster span {
    display: block;   
    color: #aaa;
    font-size:.9em;
}

.img-modal .modal-meta-bottom {
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0;
    padding: 5px;
    border-top: solid 1px #ccc;
}

.img-modal .img-comment-list {
    list-style: none;  
    padding: 0;
}
.img-modal .img-comment-list li {
    margin:0;
    margin-top:10px;
}

.img-modal .img-comment-list li > div {
    display:table-cell;
}

.img-modal .img-comment-list img {
    border-radius:50%;
    width: 42px;
    height: 42px;
    margin-right: 10px;
    margin-top: 20px;
}

.img-modal .img-comment-list p {
    margin: 0;
}

.img-modal .img-comment-list span {
    font-size: .8em; 
    color: #aaa;
}

@media only screen and (max-width : 992px) {
    .modal-content {
        border-radius: 0;
        max-height: 100%;
        overflow-y: auto;
    }
    
    .img-modal .modal-image {
        height: calc(100% - 100px);
    }
    
    .img-modal .modal-meta {
        height: auto;
    }
    
    .img-modal .modal-meta-top {
        position: static;
        padding-top: 15px;
    }
    
    .img-modal .modal-meta-bottom {
        position: static;
        margin: 0 -15px;
    }
}




 


i.inputfile {
  width: 0.1px;
  height: 0.1px;
  opacity: 0;
  overflow: hidden;
  position: absolute;
  z-index: -1;
}

.inputfile + label {
    font-size: 1.25em;
    font-weight: 700;
    color: white;
       background-color: rgba(0, 0, 255, 0.3);
    display: inline-block;

    position: absolute;
    top: 139px;
    

    left: 140px;
    border-radius: 5px;
}











.inputfile:focus + label,
.inputfile + label:hover {
    background-color: black;



}

.inputfile + label {
  cursor: pointer; /* "hand" cursor */
}




.img-thumbnail {
    display: inline-block;
    width: 100% \9;
    max-width: 100%;
    height: auto;
    padding: 4px;
    line-height: 1.42857143;
    background-color: #fff;
    border: 1px solid #ddd;
    border-radius: 50%;
    -webkit-transition: all .2s ease-in-out;
    -o-transition: all .2s ease-in-out;
    transition: all .2s ease-in-out;
}







</style>


