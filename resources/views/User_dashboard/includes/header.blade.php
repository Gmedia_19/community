
<!-- navnar -->

<div class="container top_image">
  <div class="col-md-9">
     <nav class="navbar navbar-default navbar-fixed-top an" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Logo</a>
          <li class="active visible-xs">  <a href="#">Home <i class="fas fa-home" style="font-size:20px;"></i></a></li>
          </div>
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li class="active">  <a href="#">Home <i class="fas fa-home" style="font-size:20px;"></i></a></li>
            <li><a href="#"> Find Friends <i class="fas fa-users" style="font-size: 25px;"></i></a></li>
            <li><a href="#">  <i class="fas fa-user" style="font-size: 25px;"></i>  </a></li>
            <li><a href="#"><i class="fab fa-facebook-messenger" style="font-size: 25px;"> </i>  </a> </li>
            <li><a href="#">Notifications <i class="fa fa-user"></i> </a></li>
            <li id="noti_Container">
            <div id="noti_Counter"></div>  
             <div id="noti_Button"></div>    
             <div id="notifications">
                <h3>Notifications</h3>
                <div style="height:300px;"></div>
                <div class="seeAll"><a href="#">See All</a></div>
              </div>
            </li>
          </ul>
          <ul class="nav navbar-nav ">
           <li><a href="#"><i class="fa fa-th"></i>Apps</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                <i class="fa fa-user"></i><span id="profileUserName">John Doe</span><span class="caret"></span>
              </a>
              <ul class="dropdown-menu" role="menu">
                <li><a class="SendEmail"><i class="fa fa-commenting-o" aria-hidden="true"></i>Feedback</a></li>
                <li><a href="#"><i class="fa fa-user"></i>Profile</a></li>
                <li><a href="#"><i class="fa fa-sign-out"></i>Logout</a></li>
              </ul>
            </li>
          </ul>
           </div>
           </div>
         </nav>
     </div>
      <div class="col-md-3">

       </div>
</div>


<div class="clearfix">
</div>
<!-- end of navbar -->

<!-- start of cover photo section-->
<div class="container-fluid">
  <div class="row" style="border:solid 2px red;">
   <div class="col-md-9">
    <div class="profile clearfix">                            
        <div class="image1">
        <img src="https://lorempixel.com/700/300/nature/2/" class="img-cover">
        </div>                            
        <div class="user clearfix">
        <div class="avatar">
        <img src="https://bootdey.com/img/Content/user-453533-fdadfd.png" class="img-thumbnail img-profile">
        <input type="file" name="file" id="file" class="inputfile"  style="visibility:hidden;"/>
        <label for="file"><i class="fas fa-ellipsis-h"></i></label>
            </div>  
           <div class="actions">
                <div class="btn-group">
                    <button class="btn btn-default btn-sm tip btn-responsive" title="" data-original-title="Add to friends"> <i class="far fa-edit"></i> Edit Profile</button>
                </div>
            </div>                                                                                                
        </div>  
        <div class="col-md-1">
        </div>
         <div class="col-md-10 col-xs-12">
             <div class="info">
           <h2>Martin maoth</h2>   
            <p><i class="far fa-building"></i><span class="title"></span>  Web UI Developer at Innoveds Tech Solutions (P) LTD</p>                                    
            <p><i class="fas fa-university"></i> <span class="title"></span> A.M.C College, 18th Km. Bannerghatta Road, Bangalore-83</p>
            <p> <i class="fas fa-home"></i><span class="title"></span> Bengaluru, Karnataka, India </p>                                
        </div>
        </div>
         <div class="col-md-1">
        </div>
                              
                                     
    </div>


</div>
</div>
</div>


