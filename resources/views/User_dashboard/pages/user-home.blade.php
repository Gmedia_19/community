
<!doctype html>
<html>
<head>
    @include('User_dashboard.includes.head')

</head>
<body>
    <div class="container-fluid">
        <header class="row">
            @include('User_dashboard.includes.header')
        </header>

        <div id="main" class="row" style="margin-top:50px;">

            <div class="col-md-1"> </div>

            <div class="col-md-3 col-sm-12 col-xs-12">
<div class="nav-side-menu">
                    <div class="brand">Brand Logo</div>
                    <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>

                    <div class="menu-list">

                        <ul id="menu-content" class="menu-content collapse out">
<li>
                              <a href="user-profile">
                                  <i class="fa fa-user fa-lg"></i> Profile
                              </a>
                          </li>

                          <li>
                              <a  href="" class="active">
                                  <i class="fa fa-users fa-lg"></i> News Feed
                              </a>
                          </li>


                          <li>
                              <a href="user-profile#friends">
                                  <i class="fa fa-users fa-lg"></i> Friends List
                              </a>
                          </li>
                          <li>
                              <a href="#">
                                  <i class="fa fa-users fa-lg"></i> Groups
                              </a>
                          </li>

                          <li>
                              <a href="#">
                                  <i class="fa fa-users fa-lg"></i> Videos
                              </a>
                          </li>
                      </ul>
                  </div>
              </div>

          </div>

          <div class="col-md-5 col-sm-12 col-xs-12">


            @include('User_dashboard.includes.timeline')
</div>
<div class="col-md-3 col-sm-12 col-xs-12">
</div>
</div>
</div>
<footer class="row">
    @include('User_dashboard.includes.footer')

</footer>

</div>

<script type="text/javascript" src="{{asset('js/jquery-notification.js')}}"></script>
<script type="text/javascript" src={{ asset('/js/user/main.js') }}></script>
</body>
</html>

