 <meta charset="utf-8">
  <title> Template</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">


  <!-- Bootstrap core CSS -->

   <link href="{{asset('css/style.css')}}" rel="stylesheet">
<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">




<script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.4.4/parsley.min.js"></script>







    <style>
      










        

        .navbar-inverse {
     background-color: #222;  
    /* border-color: #080808; */
   
}

.nav-text 
{


font-family: robot;
text-transform: uppercase;
font-size: 15px;
color:black;
}

.navbar-inverse {
    background-color: #fff;
    border-color:#fff;
    margin-top: 13px;
}
  .modal-header, .modal-body, .modal-footer{
    padding: 25px;
  }
  .modal-footer{
    text-align: center;
    margin-left: 384px;
color:#fff;
background: rgba(119, 109, 98,0.2);
  }
  #signup-modal-content, #forgot-password-modal-content{
    display: none;
  }
      
  
      
  input.parsley-error   {    
      border-color:#843534;
      box-shadow: none;
  }
  input.parsley-error:focus{    
      border-color:#843534;
      box-shadow:inset 0 1px 1px rgba(0,0,0,.075),0 0 6px #ce8483
  }
  .parsley-errors-list.filled {
      opacity: 1;
      color: #a94442;
      display: none;
  }

.modal-content

{
background-image:url("images/bg.jpg");
width:850px;

}


.main-header {
    position: relative;
    left: 0px;
    
    z-index: 999;
    width: 100%;
    transition: all 500ms ease;
    -moz-transition: all 500ms ease;
    -webkit-transition: all 500ms ease;
    -ms-transition: all 500ms ease;
    -o-transition: all 500ms ease;
    
}

.main-header .header-top {
    position: relative;
    background: rgba(44, 48, 53, 0.30);
    color: #555555;
    z-index: 5;
   
    height: 28px;
}

.auto-container {
    position: static;
    max-width: 1200px;
    padding: 0px 15px;
    margin: 0 auto;
        margin-bottom: 0px;
    margin-bottom: 0px;
    margin-bottom: 2px;
}

.main-header .header-top .top-left {
    position: relative;
    float: left;
    padding: 6px 0px;
    color: #ffffff;
    font-size: 12px;
}

.main-header .header-top ul li {
    position: relative;
    float: left;
    font-size: 14px;
   
    margin-right: 20px;
    color: #fff;
    letter-spacing: 1px;
    font-family: robot;
    
    list-style-type: none;

}


.main-header .header-top .top-right {
    position: relative;
    padding: 4px 0px;
    float: right;
}


.main-header .header-top .top-right ul li {
    margin-right: 0px;
    margin-left: 25px;
   margin-top: 0px;
}








/*
Fade content bs-carousel with hero headers
Code snippet by maridlcrmn (Follow me on Twitter @maridlcrmn) for Bootsnipp.com
Image credits: unsplash.com
*/

/********************************/
/*       Fade Bs-carousel       */
/********************************/
.fade-carousel {
    position: relative;
    height: 350px;
}
.fade-carousel .carousel-inner .item {
    height: 350px;
}
.fade-carousel .carousel-indicators > li {
    margin: 0 2px;
    background-color: #f39c12;
    border-color: #f39c12;
    opacity: .7;
}
.fade-carousel .carousel-indicators > li.active {
  width: 10px;
  height: 10px;
  opacity: 1;
}

/********************************/
/*          Hero Headers        */
/********************************/
.hero {
    position: absolute;
    top: 50%;
    left: 50%;
    z-index: 3;
    color: #fff;
    text-align: center;
    text-transform: uppercase;
    text-shadow: 1px 1px 0 rgba(0,0,0,.75);
      -webkit-transform: translate3d(-50%,-50%,0);
         -moz-transform: translate3d(-50%,-50%,0);
          -ms-transform: translate3d(-50%,-50%,0);
           -o-transform: translate3d(-50%,-50%,0);
              transform: translate3d(-50%,-50%,0);
}
.hero h1 {
    font-size: 6em;    
    font-weight: bold;
    margin: 0;
    padding: 0;
}

.fade-carousel .carousel-inner .item .hero {
    opacity: 0;
    -webkit-transition: 2s all ease-in-out .1s;
       -moz-transition: 2s all ease-in-out .1s; 
        -ms-transition: 2s all ease-in-out .1s; 
         -o-transition: 2s all ease-in-out .1s; 
            transition: 2s all ease-in-out .1s; 
}
.fade-carousel .carousel-inner .item.active .hero {
    opacity: 1;
    -webkit-transition: 2s all ease-in-out .1s;
       -moz-transition: 2s all ease-in-out .1s; 
        -ms-transition: 2s all ease-in-out .1s; 
         -o-transition: 2s all ease-in-out .1s; 
            transition: 2s all ease-in-out .1s;    
}

/********************************/
/*            Overlay           */
/********************************/
.overlay {
    position: absolute;
    width: 100%;
    height: 350px;
    z-index: 2;
    /* background-color: #080d15; */
    opacity: .7;
}

/********************************/
/*          Custom Buttons      */
/********************************/
.btn.btn-lg {padding: 10px 40px;}
.btn.btn-hero,
.btn.btn-hero:hover,
.btn.btn-hero:focus {
    color: #f5f5f5;
    background-color: #1abc9c;
    border-color: #1abc9c;
    outline: none;
    margin: 20px auto;
}

/********************************/
/*       Slides backgrounds     */
/********************************/
.fade-carousel .slides .slide-1, 
.fade-carousel .slides .slide-2,
.fade-carousel .slides .slide-3 {
  height: 100vh;
  background-size: cover;
  background-position: center center;
  background-repeat: no-repeat;
}
.fade-carousel .slides .slide-12 {
  background-image: url(image/); 
  height:350px;
}
.fade-carousel .slides .slide-22 {
  background-image: url(image/);
  height:350px
}
.fade-carousel .slides .slide-32 {
  background-image: url(image/);
  height:350px
}


























 


























    </style>
  

