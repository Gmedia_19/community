<!doctype html>
<html>
<head>
 @include('home.includes.home_head') 
</head>
<body>
  <div class="container1">
<div id="main" class="row1">
  @include('home.includes.home_content')

</div>

<footer class="row">
  @include('User_dashboard.includes.footer')
</footer>

<script type="text/javascript" src="{{asset('home/js/vendor/jquery-1.12.4.min.js')}}"></script> 
<script type="text/javascript" src="{{asset('home/js/vendor/scrolloverflow.min.js')}}"></script>
<script type="text/javascript" src="{{asset('home/js/vendor/all.js')}}"></script>
<script type="text/javascript" src="{{asset('home/js/particlejs/particles.min.js')}}"></script>
<script type="text/javascript" src="{{asset('home/js/jquery.downCount.js')}}"></script>
<script type="text/javascript" src="{{asset('home/js/form_script.js')}}"></script>
<script type="text/javascript" src="{{asset('home/js/main.js')}}"></script>
<script type="text/javascript" src="{{asset('home/js/vendor/modernizr-2.7.1.min.js')}}"></script>






</div>
</body>
</html>

