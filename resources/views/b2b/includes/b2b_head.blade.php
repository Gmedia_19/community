 <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1">


    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


 <link href="{{asset('b2b/css/style.css')}}" rel="stylesheet">

<!-- Calendar -->

 <link href="{{asset('b2b/css/jquery-ui.css')}}" rel="stylesheet">






<style>
/*
Fade content bs-carousel with hero headers
Code snippet by maridlcrmn (Follow me on Twitter @maridlcrmn) for Bootsnipp.com
Image credits: unsplash.com
*/

/********************************/
/*       Fade Bs-carousel       */
/********************************/
.fade-carousel {
    position: relative;
     height:400px;
}
.fade-carousel .carousel-inner .item {
    height: 100vh;
}
.fade-carousel .carousel-indicators > li {
    margin: 0 2px;
    background-color: #f39c12;
    border-color: #f39c12;
    opacity: .7;
}
.fade-carousel .carousel-indicators > li.active {
  width: 10px;
  height: 10px;
  opacity: 1;
}

/********************************/
/*          Hero Headers        */
/********************************/
.hero {
    position: absolute;
    top: 50%;
    left: 50%;
    z-index: 3;
    color: #fff;
    text-align: center;
    text-transform: uppercase;
    text-shadow: 1px 1px 0 rgba(0,0,0,.75);
      -webkit-transform: translate3d(-50%,-50%,0);
         -moz-transform: translate3d(-50%,-50%,0);
          -ms-transform: translate3d(-50%,-50%,0);
           -o-transform: translate3d(-50%,-50%,0);
              transform: translate3d(-50%,-50%,0);
}
.hero h1 {
    font-size: 6em;    
    font-weight: bold;
    margin: 0;
    padding: 0;
}

.fade-carousel .carousel-inner .item .hero {
    opacity: 0;
    -webkit-transition: 2s all ease-in-out .1s;
       -moz-transition: 2s all ease-in-out .1s; 
        -ms-transition: 2s all ease-in-out .1s; 
         -o-transition: 2s all ease-in-out .1s; 
            transition: 2s all ease-in-out .1s; 
}
.fade-carousel .carousel-inner .item.active .hero {
    opacity: 1;
    -webkit-transition: 2s all ease-in-out .1s;
       -moz-transition: 2s all ease-in-out .1s; 
        -ms-transition: 2s all ease-in-out .1s; 
         -o-transition: 2s all ease-in-out .1s; 
            transition: 2s all ease-in-out .1s;    
}

/********************************/
/*            Overlay           */
/********************************/
.overlay {
    position: absolute;
    width: 100%;
   height:400px;
    z-index: 2;
   /*  background-color: #080d15; */
    opacity: .7;
}

/********************************/
/*          Custom Buttons      */
/********************************/
.btn.btn-lg {padding: 10px 40px;}
.btn.btn-hero,
.btn.btn-hero:hover,
.btn.btn-hero:focus {
    color: #f5f5f5;
    background-color: #1abc9c;
    border-color: #1abc9c;
    outline: none;
    margin: 20px auto;
}

/********************************/
/*       Slides backgrounds     */
/********************************/
.fade-carousel .slides .slide-1, 
.fade-carousel .slides .slide-2,
.fade-carousel .slides .slide-3 {
  height: 100vh;
  background-size: cover;
  background-position: center center;
  background-repeat: no-repeat;
}
.fade-carousel .slides .slide-1 {
  background-image: url(images/slide1.jpg); 
}
.fade-carousel .slides .slide-2 {
  background-image: url(images/slide2.jpg);
}
.fade-carousel .slides .slide-3 {
  background-image: url(images/slide4.jpg);
}

/********************************/
/*          Media Queries       */
/********************************/
@media screen and (min-width: 980px){
    .hero { width: 980px; }    
}
@media screen and (max-width: 640px){
    .hero h1 { font-size: 4em; }    
}

.carousel-inner {
    position: relative;
    width: 100%;
height:400px;
    overflow: hidden;
    margin-top: -21px;
}


.matches-main-agileinfo {
    margin-bottom: 1.5em;
    padding-bottom: 1em;
    /* border-bottom: 1px dotted #ad3838; */
    box-shadow: 0px 0px 4px 4px #bdbcbc;
}
.profile1 {
     background: url(images/college.jpg) no-repeat 0px 0px;
    height: 270px;
    /* padding-left: 55px; */
    width: 100%;
    margin-left: -15px;
}

.person-info-agileits-mat {
   
  /* margin-top: 1.5em; */
    height: 270px;
    margin-left: -41px;

}

.person-info-agileits-mat ul li {
    list-style: none;
    display: block;
    margin-bottom: 10px;
    color: #8a8a8a;
    font-size: 16px;
    letter-spacing: 0.5px;
}

.person-info-agileits-mat ul li span {
    width: 141px;
    display: inline-block;
    color: #4a4a4a;
    font-weight: 600;
}

.person-info-agileits-mat ul li a {
    display: inline-block;
    color: #000;
    padding: 10px 20px;
    font-size: 15px;
    background: #44c7f4;
    margin: 10px 10px 0 0px;
}

.about-person h6, .person-info-agileits-mat h3 {
    font-size: 20px;
    margin: 1em 0 .5em;
    color: #000;
}

.about-person
{
font-family: robot;
    font-size: 18px;
}

.button12 a {
    z-index: 2;
    display: block;
    position: absolute;
    width: 100%;
    height: 100%;
}
.button12:hover a {
    color: #fff;
}
.about-person .sim-button {
    background: #ffad00;
       margin-top: 0em!important;
}
.sim-button {
    line-height: 50px;
    height: 50px;
    text-align: center;
    margin-right: auto;
    margin-left: auto;
    margin-top: 31px;
    width: 16.9%;
    display: inline-block;
    cursor: pointer;
    background: rgba(0, 0, 0, 0.24);
}

.button12 {
    color: rgba(255,255,255,1);
    -webkit-transition: all 0.5s;
    -moz-transition: all 0.5s;
    -o-transition: all 0.5s;
    transition: all 0.5s;
    position: relative;
    border: 1px solid rgba(255,255,255,0.5);
    overflow: hidden;
}
.title_mt
{
  font-family:robot;
  font-size: 16px;
}


.navbar-inverse {
        box-shadow: 0 -53px 3px #ffad00 inset;
    border-color: #ffffff;
    background-color: #fff;
    
     
}

.navbar-inverse .navbar-nav>li>a {
    color: black;
    font-family: robot;
    font-size: 19px;
}






.navbar-inverse .navbar-nav>.active>a, .navbar-inverse .navbar-nav>.active>a:focus, .navbar-inverse .navbar-nav>.active>a:hover {
    color: #fff;
    background-color: #ffb61c;
}



.navbar-inverse .navbar-nav>li>a:focus, .navbar-inverse .navbar-nav>li>a:hover {
    color: #b38f38;
    background-color: transparent;
}

.top-bar
{


box-shadow: 0px 2px 2px 0px rgba(50, 50, 50, 0.10);
}

@media (min-width: 768px)
{
.navbar-form .input-group>.form-control {
    width: 233%;
}
}


</style>
</head>
