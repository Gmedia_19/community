
<!doctype html>
<html>
<head>
   @include('temples.includes.temp_head') 


</head>
<body>
    <div class="container-fluid">
        <header class="row">
      
        @include('temples.includes.temp_header1') 

    </header>
</div>
<div class="container-fluid">
     
        <div id="main" class="row" style=" margin-top: -22px;">

<div id="header-banner">
       <div class="banner-overlay">
          <div class="container">
            <div class="row">
              <section class="col-sm-6">
                <h1 class="text-upper"> Shree Vetal Maharudra Saunsthan</h1>
              </section>
              
              
              <section class="col-sm-6">
                <ol class="breadcrumb1">
                  <li class="home"><a href="index.aspx">Home</a></li>
                  <li><a href="#">Temple</a></li>
                  <li class="active"><a href="tour-pack7.aspx">Shree Vetal Maharudra Saunsthan</a></li>
                </ol>
              </section>
            </div>
          </div>
        </div>
      </div> 
</div>


</div> <div class="clearfix"></div>
     <div class="container">  
     <div class="col-md-9"> 





    <div class="tab_container">
      <input id="tab1" type="radio" name="tabs" checked>
      <label for="tab1"><i class="fa fa-home" aria-hidden="true"></i><span>Temple</span></label>

      <input id="tab2" type="radio" name="tabs">
      <label for="tab2"><i class="fa fa-history" aria-hidden="true"></i><span>History</span></label>

      <input id="tab3" type="radio" name="tabs">
      <label for="tab3"><i class="fa fa-newspaper-o" aria-hidden="true"></i><span>Events</span></label>

      <input id="tab4" type="radio" name="tabs">
      <label for="tab4"><i class="fa fa-phone" aria-hidden="true"></i><span>Contact</span></label>

     

      <section id="content1" class="tab-content">
        
<div class="dad">
  <div class="son-1">
  </div>
    <p class="son-text"><span class="son-span">Shree Vetal Maharudra Saunsthan
</span> <br/>
<span class="son-span">Mulgao, Goa 403503, 
</span>


</p>
</div>


           
            
      </section>

      <section id="content2" class="tab-content">
        <h3>Headline 2</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>

             <h3>Headline 2</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
          
      </section>

      <section id="content3" class="tab-content">
       
<div class="row">
  <!-- First row -->
 
    <div class="col-xs-12 col-md-12">
      <!-- Card -->
      <article class="card animated fadeInLeft">
        <img class="card-img-top img-responsive" src="images/event.jpg" alt="Deer in nature" />
       
      </article><!-- .end Card -->
    </div>
    <div class="col-md-12"> 
     <div class="card-block">
         <div class="col-md-4"> 
          <h1 class="section-header text-center" style="    
    padding: 2px;
    font-family: robot;">Shree Vetal <span class="content-header1 ">  Maharudra </span></h1>
</div>
 <div class="col-md-4"> 
 
       <h1 class="section-header text-center" style="    margin-left: 98px;
    padding: 2px;
    font-family: robot;"> <time class="entry-time" datetime="2016-10-03T10:56:34+00:00"><a href="#">
 <i class="fa fa-clock-o" aria-hidden="true"></i> <span class="content-header1 ">  03/10/2016 at 10:56 AM </span> </a></time> </h1>

    </div>
    <div class="clearfix"> </div>
   
          <h6 class="text-muted">George Orwell</h6>
          <p class="card-text">Tired of their servitude to man, a group of farm animals revolt and establish their own society, only to be betrayed into worse servitude by their leaders, the pigs. This satire addresses the communist philosophy the Soviet Union.</p>
 <details>
  <summary>View more</summary>


   <p class="card-text">Tired of their servitude to man, a group of farm animals revolt and establish their own society, only to be betrayed into worse servitude by their leaders, the pigs. This satire addresses the communist philosophy the Soviet Union.</p>

       </details>
        </div>
    </div>
  
  <!-- Second row -->
 
</div>






        
      </section>

      <section id="content4" class="tab-content">
       

<div class="jumbotron">
                  <div class="row">
                      
                      <div class="col-md-8 col-xs-12 col-sm-6 col-lg-8">
                          <div class="container" style="border-bottom: 1px solid #9c6d0b;">
                            <h2>John Doe</h2>
                          </div>
                            <hr>
                          <ul class="container details">
                            <li><p> <i class="fa fa-phone" aria-hidden="true"></i> &nbsp; +91 90000 00000</p></li>
                            <li><p><span class="glyphicon glyphicon-envelope one" style="width:50px;"></span>somerandom@email.com</p></li>
                            <li><p>
                              <i class="fa fa-map-marker" aria-hidden="true"></i> &nbsp; &nbsp;                            Hyderabad</p></li>
                            <li><p>
                              <i class="fa fa-envelope" aria-hidden="true"></i>
                              <a href="#"> &nbsp; www.example.com</p></a>
                          </ul>
                      </div>
                  </div>










            
            
      </section>

      
    </div>


</div>


 <div class="col-md-3">

 <div class="text-center up-evnts">

  <h4>upcoming events <hr/>  </h4>
</div>

<div class="shadow">
<div class="text-center up-evnts">

  <h3>Event Name </h3>
</div> 
<div class="container2">
  <img src="images/event.jpg " alt="upcoming-events" class="image1 img-responsive">
  <div class="overlay">
<time class="entry-time" datetime="2016-10-03T10:56:34+00:00"><a href="#">
 <i class="fa fa-clock-o" aria-hidden="true"></i> <span class="content-header1" style="font-size: 18px;">  03/10/2016 at 10:56 AM </span> </a></time>

  </div>
  </div>
  </div>
  <div class="desc">
 <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
 <p style="text-align:center;"> <button type="button" style="box-shadow: 0 -12px 129px #ffad00 inset;">View Details</button> </p>
  </div>

</div> 


</div>  
         
<div class="clearfix"> </div>
<br/>








<div class="container">
    <div class="row">
    <div class='list-group gallery'>
            <div class='col-sm-4 col-xs-6 col-md-3'>
                <a class="thumbnail fancybox" rel="ligthbox" href="http://pipsum.com/300x320.jpg">
                    <img class="img-responsive" alt="" src="http://pipsum.com/320x320" />
                    <div class='text-right'>
                        <small class='text-muted'>Image Title</small>
                    </div> <!-- text-right / end -->
                </a>
            </div> <!-- col-6 / end -->
            <div class='col-sm-4 col-xs-6 col-md-3'>
                <a class="thumbnail fancybox" rel="ligthbox" href="http://pipsum.com/300x320.jpg">
                    <img class="img-responsive" alt="" src="http://pipsum.com/320x320" />
                    <div class='text-right'>
                        <small class='text-muted'>Image Title</small>
                    </div> <!-- text-right / end -->
                </a>
            </div> <!-- col-6 / end -->
            <div class='col-sm-4 col-xs-6 col-md-3'>
                <a class="thumbnail fancybox" rel="ligthbox" href="http://pipsum.com/300x320.jpg">
                    <img class="img-responsive" alt="" src="http://pipsum.com/320x320" />
                    <div class='text-right'>
                        <small class='text-muted'>Image Title</small>
                    </div> <!-- text-right / end -->
                </a>
            </div> <!-- col-6 / end -->
            <div class='col-sm-4 col-xs-6 col-md-3'>
                <a class="thumbnail fancybox" rel="ligthbox" href="http://pipsum.com/300x320.jpg">
                    <img class="img-responsive" alt="" src="http://pipsum.com/320x320" />
                    <div class='text-right'>
                        <small class='text-muted'>Image Title</small>
                    </div> <!-- text-right / end -->
                </a>
            </div> <!-- col-6 / end -->
            <div class='col-sm-4 col-xs-6 col-md-3'>
                <a class="thumbnail fancybox" rel="ligthbox" href="http://pipsum.com/300x320.jpg">
                    <img class="img-responsive" alt="" src="http://pipsum.com/320x320" />
                    <div class='text-right'>
                        <small class='text-muted'>Image Title</small>
                    </div> <!-- text-right / end -->
                </a>
            </div> <!-- col-6 / end -->
            <div class='col-sm-4 col-xs-6 col-md-3'>
                <a class="thumbnail fancybox" rel="ligthbox" href="http://pipsum.com/300x320.jpg">
                    <img class="img-responsive" alt="" src="http://pipsum.com/320x320" />
                    <div class='text-right'>
                        <small class='text-muted'>Image Title</small>
                    </div> <!-- text-right / end -->
                </a>
            </div> <!-- col-6 / end -->
        </div> <!-- list-group / end -->
  </div> <!-- row / end -->
</div> <!-- container / end -->













<footer class="row">
    @include('User_dashboard.includes.footer')

</footer>



<script type="text/javascript" src="{{asset('js/jquery-notification.js')}}"></script>
<script type="text/javascript" src={{ asset('/js/user/main.js') }}></script>
</body>
</html>

