
<!doctype html>
<html>
<head>
 @include('temples.includes.temp_head') 
</head>
<body>
  <div class="container-fluid">

    <header class="row">
      
      	@include('temples.includes.temp_header1') 

    </header>


<div id="main" class="row">
	
		  @include('temples.includes.temp_content') 
    

</div>





    <div id="main" class="row">

      @yield('content')

   
      <div class="clearfix"> </div>
   

</div>

<footer class="row">
  @include('User_dashboard.includes.footer')

</footer>


</div>
</body>
</html>

