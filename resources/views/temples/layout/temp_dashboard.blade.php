
<!doctype html>
<html>
<head>
 @include('temples.includes.temp_head') 
</head>
<body>
  <div class="container">

  


<div id="main" class="row" style="margin-top: 52px;">
	
		<div class="col-md-6"> 

     @include('temples.includes.admin_sidebar') 
     </div>

   
    

</div>





    <div id="main" class="row">

      @yield('content')

   
      <div class="clearfix"> </div>
   

</div>

<footer class="row">
  @include('User_dashboard.includes.footer')

</footer>
<script type="text/javascript" src="{{asset('temples/js/admin_sidebar.js')}}"></script>
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/build/jquery.datetimepicker.full.min.js"></script>
<script src="http://cdn.craig.is/js/rainbow-custom.min.js"></script>

</div>
</body>
</html>

