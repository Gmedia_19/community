 <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

<link href="{{asset('temples/css/temp_style.css')}}" rel="stylesheet">
<link href="{{asset('temples/css/bootstrap-datetimepicker.css')}}" rel="stylesheet">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/jquery.datetimepicker.min.css" />
<link href="{{asset('temples/css/bootstrap.min.css')}}" rel="stylesheet">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{asset('js/bootstrap-datetimepicker.js') }}"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.css"/>



<style>

.navbar-inverse {
      box-shadow: 0 -12px 22px #ffad00 inset;
    border-color: #ffffff;
    
     
}

.navbar-inverse .navbar-nav>li>a {
    color: black;
    font-family: robot;
    font-size: 19px;
}






.navbar-inverse .navbar-nav>.active>a, .navbar-inverse .navbar-nav>.active>a:focus, .navbar-inverse .navbar-nav>.active>a:hover {
    color: #fff;
    background-color: #ffb61c;
}



.navbar-inverse .navbar-nav>li>a:focus, .navbar-inverse .navbar-nav>li>a:hover {
    color: #b38f38;
    background-color: transparent;
}


ul.right li a {font-size: 1.3rem;}



#wrapper {
  width: 960px;
  margin: 0 auto;

}

#wrapper {
  perspective: 2500;
  -webkit-perspective: 2500;
  width: 800px;
  
  margin: 30px auto 0 auto;
  perspective-origin: 50% 150px;
  -webkit-perspective-origin: 50% 150px;
  transition: perspective, 1s;
  -o-transition: -o-perspective, 1s;
  -moz-transition: -moz-perspective, 1s;
  -webkit-transition: -webkit-perspective, 1s;
}

#image:hover {
  animation-play-state:paused;
  -o-animation-play-state:paused;
  -moz-animation-play-state:paused;
  -webkit-animation-play-state:paused;
}


@-webkit-keyframes spin {
  from {
    transform: rotateY(0);
    -o-transform: rotateY(0);
    -ms-transform: rotateY(0);
    -moz-transform: rotateY(0);
    -webkit-transform: rotateY(0);
  }
  to {
    transform: rotateY(-360deg);
    -o-transform: rotateY(-360deg);
    -ms-transform: rotateY(-360deg);
    -moz-transform: rotateY(-360deg);
    -webkit-transform: rotateY(-360deg);
  }
}

#image {
  margin: 0 auto;
  height: 300px;
  width: 400px;
  transform-style: preserve-3d;
  -webkit-transform-style: preserve-3d;
  animation: spin 24s infinite linear;
  -moz-animation: spin 24s infinite linear;
  -o-animation: spin 24s infinite linear;
  -webkit-animation: spin 24s infinite linear;
}

.image {
  position: absolute;
  height: 100%;
  width: 100%;
  text-align: center;
  font-size: 20em;
  color: #fff;
}

#image > .i1 {
  background-image:url("images/bgimg3.jpg");
  transform: translateZ(485px);
  -moz-transform: translateZ(485px);
  -o-transform: translateZ(485px);
  -ms-transform: translateZ(485px);
  -webkit-transform: translateZ(485px);
}
#image > .i2 {
    background-image:url("images/bgimg2.jpg");
  transform: rotateY(45deg) translateZ(485px);
  -moz-transform: rotateY(45deg) translateZ(485px);
  -o-transform: rotateY(45deg) translateZ(485px);
  -ms-transform: rotateY(45deg) translateZ(485px);
  -webkit-transform: rotateY(45deg) translateZ(485px);
}
#image > .i3 {
  background-image:url("images/bgimg2.jpg");
  transform: rotateY(90deg) translateZ(485px);
  -moz-transform: rotateY(90deg) translateZ(485px);
  -o-transform: rotateY(90deg) translateZ(485px);
  -ms-transform: rotateY(90deg) translateZ(485px);
  -webkit-transform: rotateY(90deg) translateZ(485px);
}
#image > .i4 {
  background-image:url("images/bgimg3.jpg");
  transform: rotateY(135deg) translateZ(485px);
  -moz-transform: rotateY(135deg) translateZ(485px);
  -o-transform: rotateY(135deg) translateZ(485px);
  -ms-transform: rotateY(135deg) translateZ(485px);
  -webkit-transform: rotateY(135deg) translateZ(485px);
}
#image > .i5 {
    background-image:url("images/bgimg2.jpg");
  transform: rotateY(180deg) translateZ(485px);
  -moz-transform: rotateY(180deg) translateZ(485px);
  -o-transform: rotateY(180deg) translateZ(485px);
  -ms-transform: rotateY(180deg) translateZ(485px);
  -webkit-transform: rotateY(180deg) translateZ(485px);
}
#image > .i6 {
  background-image:url("images/bgimg3.jpg");
  transform: rotateY(225deg) translateZ(485px);
  -moz-transform: rotateY(225deg) translateZ(485px);
  -o-transform: rotateY(225deg) translateZ(485px);
  -ms-transform: rotateY(225deg) translateZ(485px);
  -webkit-transform: rotateY(225deg) translateZ(485px);
}
#image > .i7 {
  background-image:url("images/bgimg2.jpg");
  transform: rotateY(270deg) translateZ(485px);
  -moz-transform: rotateY(270deg) translateZ(485px);
  -o-transform: rotateY(270deg) translateZ(485px);
  -ms-transform: rotateY(270deg) translateZ(485px);
  -webkit-transform: rotateY(270deg) translateZ(485px);
}
#image > .i8 {
   background-image:url("images/bgimg2.jpg");
  transform: rotateY(315deg) translateZ(485px);
  -moz-transform: rotateY(315deg) translateZ(485px);
  -o-transform: rotateY(315deg) translateZ(485px);
  -ms-transform: rotateY(315deg) translateZ(485px);
  -webkit-transform: rotateY(315deg) translateZ(485px);
}






.dad {
    height: 400px;
    width: 100%;
    overflow: hidden;
   position: relative;
    padding: 0;
        z-index:0;
}

.dad > .son-1 {
    position: absolute;
    height: 100%;
    width: 100%;
    -moz-transition: all 5s;
    -webkit-transition: all 5s;
    transition: all 5s;
    -moz-transform: scale(1,1);
    -webkit-transform: scale(1,1);
    transform: scale(1,1);
    background-image: url("images/Shree Vetal Maharudra Saunsthan.jpg");
    -moz-background-size: cover;
    -webkit-background-size: cover;
    background-size: cover;
    z-index: -1;
}

.dad:hover > .son-1 {
    -moz-transform: scale(2,2);
    -webkit-transform: scale(2,2);
    transform: scale(2,2);    
}

.son-text {
    color: #fcfcfc;
  font-size: 2em;
  font-family: 'Josefin Slab', serif;
  text-align: center;
  width: 100%;
  height: 400px;
    -moz-transition: all 1s;
    -webkit-transition: all 1s;
    transition: all 1s;
 padding-top: 34vh;
}

.son-text:hover {
  background-color: #22313F;
  opacity: 0.7;
}

.son-span {
  background: #fcfcfc;
  color: #22313F;
  padding: 15px 45px;
  font-family: 'Josefin Slab', serif;
  font-style: italic;
}



#header-banner {
    margin-bottom: 40px;
    background: url(images/header-banner.jpg) center 0 no-repeat;
    border-bottom: 1px solid #282460;
}
.banner-overlay {
       padding: 44px 0;
    background: rgba(22, 41, 69, 0.8);
}

#header-banner h1 {
    color: #fff;
    /* margin: 0; */
    font-size: 24px;
    font-weight: 400;
    text-shadow: 2px 2px 8px #17130C;
    font-family: roboto;
    letter-spacing: 1px;
}


.breadcrumb1 li.home {
    padding-left: 16px;
    background: url(images/bc-home.png) 0 5px no-repeat;
}

.breadcrumb1 > li {
    display: inline-block;
}

.breadcrumb1 {
    float: right;
    margin: 0;
    padding: 21px 0 0;
    list-style: none;
}

.breadcrumb1 li {
    padding-left: 30px;
    background: url(images/bc-arrow.png) 10px center no-repeat;
}
.breadcrumb1 li a {

  color:#fff;
}


.wrapper {
  margin: 100px auto;
  width: 80%;
  font-family: sans-serif;
  color: #98927C;
  font-size: 14px;
  line-height: 24px;
  max-width: 600px;
  min-width: 340px;
  overflow: hidden;
}
.tabs li {
  list-style: none;
  float: left;
  width: 20%;
}
.tabs a {
  display: block;
  text-align: center;
  text-decoration: none;
  position: relative;
  text-transform: uppercase;
  color: #fff;
  height: 70px;
  line-height: 90px;
  background: linear-gradient(165deg, transparent 29%, #98927C 30%);
}
.tabs a:hover,
.tabs a.active {
  background: linear-gradient(165deg, transparent 29%, #F2EEE2 30%);
  color: #98927C;
}
.tabs a:before {
  content: '';
  position: absolute;
  z-index: 11;
  left: 100%;
  top: -100%;
  height: 70px;
  line-height: 90px;
  width: 0;
  border-bottom: 70px solid rgba(0, 0, 0, 0.1);
  border-right: 7px solid transparent;
}
.tabs a.active:before {
  content: '';
  position: absolute;
  z-index: 11;
  left: 100%;
  top: -100%;
  height: 70px;
  line-height: 90px;
  width: 0;
  border-bottom: 70px solid rgba(0, 0, 0, 0.2);
  border-right: 20px solid transparent;
}
.tabgroup {
  box-shadow: 2px 2px 2px 2px rgba(0, 0, 0, 0.1);
}
.tabgroup div {
  padding: 30px;
  background: #F2EEE2;
  box-shadow: 0 3px 10px rgba(0, 0, 0, 0.3);
}








/*Fun begins*/
.tab_container {
  width: 90%;
  margin: 0 auto;

  position: relative;
}

input, section {
  clear: both;
  padding-top: 10px;
  display: none;
}

label {
  font-weight: 700;
  font-size: 18px;
  display: block;
  float: left;
  width: 25%;
  padding:18px;
  color: black;
  cursor: pointer;
  text-decoration: none;
  text-align: center;
         box-shadow: 0 -12px 129px #ffad00 inset;
}

#tab1:checked ~ #content1,
#tab2:checked ~ #content2,
#tab3:checked ~ #content3,
#tab4:checked ~ #content4,
#tab5:checked ~ #content5 {
  display: block;
  padding: 20px;
  background: #fff;
  font-family: robot;
  border-bottom: solid 1px #ffd16f;
    border-left: solid 1px #ffcf6a;
    border-right: solid 1px #ffca5a;
    color:black;
}

.tab_container .tab-content p,
.tab_container .tab-content h3 {
  -webkit-animation: fadeInScale 0.7s ease-in-out;
  -moz-animation: fadeInScale 0.7s ease-in-out;
  animation: fadeInScale 0.7s ease-in-out;
}
.tab_container .tab-content h3  {
  text-align: center;
}

.tab_container [id^="tab"]:checked + label {
  background: #fff;
  box-shadow: inset 0 3px #462d00;
      border-left: solid 1px #ffc54a;
          border-right: solid 1px #ffc54a;
}

.tab_container [id^="tab"]:checked + label .fa {
      color: #ffb61c;
}

label .fa {
  font-size: 1.3em;
  margin: 0 0.4em 0 0;
}

/*Media query*/
@media only screen and (max-width: 930px) {
  label span {
    font-size: 14px;
  }
  label .fa {
    font-size: 14px;
  }
}

@media only screen and (max-width: 768px) {
  label span {
    display: none;
  }

  label .fa {
    font-size: 16px;
  }

  .tab_container {
    width: 98%;
  }
}

/*Content Animation*/
@keyframes fadeInScale {
  0% {
    transform: scale(0.9);
    opacity: 0;
  }
  
  100% {
    transform: scale(1);
    opacity: 1;
  }
}




}




.container2 {
  position: relative;
  width: 50%;
  max-width: 300px;
  margin-top: -10px;
}

.image1 {
  display: block;
  width: 300px;
  height: 200px;
}

.overlay {
  position: absolute; 
    bottom: 279px;
  background: rgb(0, 0, 0);
  background: rgba(0, 0, 0, 0.5); /* Black see-through */
  color: #f1f1f1; 
  width: 89%;
  transition: .5s ease;

  color: white;
  font-size: 20px;
  padding: 10px;
  text-align: center;
  opacity: 1;

}

.container2:hover .overlay {
  opacity: 1;

}



.desc
{
 padding:5px;
 font-family: robot;
 font-size: 17px;
 text-align:justify;
     border-bottom: 1px solid #ddd !important;
    border-left: 1px solid #ddd !important;
    border-right: 1px solid #ddd !important;
    margin-bottom: 1.5em !important;
}



.shadow
{
      box-shadow: 0px 0px 20px 10px rgba(169, 163, 163, 0.3);

}

.up-evnts
{
  padding-top: 1px;
    font-family: robot;
}



.up-evnts h4 {
 overflow: hidden;
 text-align: center;
}
.up-evnts h4:before,
.up-evnts h4:after {
 background-color: #333;
 content: "";
 display: inline-block;
 height: 1px;
 position: relative;
 vertical-align: middle;
 width: 50%;
}
.up-evnts h4:before {
 right: 0.5em;
 margin-left: -50%;
}
.up-evnts h4:after {
 left: 7.5em;
    /* margin-right: -47%; */
    top: -59px;
}

  .details li {
      list-style: none;
    }

    
    .jumbotron {
    padding-top: 30px;
    padding-bottom: 30px;
    margin-bottom: 30px;
    color: inherit;
    /* background-color: #eee; */
    box-shadow: 0px 0px 0px 1px rgba(169, 163, 163, 0.3);
}

















</style>


