

<div id="navbar-wrapper">
        <header>
            <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">Logo</a>
                    </div>
                    <div id="navbar-collapse" class="collapse navbar-collapse">
                      
                        <ul class="nav navbar-nav navbar-right">
                           
                            <li class="dropdown">
                                <a id="user-profile" href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="http://lorempixel.com/100/100/people/9/" class="img-responsive img-thumbnail img-circle"> Username</a>
                                <ul class="dropdown-menu dropdown-block" role="menu">
                                   
                                    <li><a href="#">Admin</a></li>
                                    <li><a href="#">Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
    </div>

<div class="clearfix"></div>





<div class="container">
    
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
      <li class="active">
          <a href="#home" role="tab" data-toggle="tab">
              <icon class="fa fa-home"></icon> Add Temples
          </a>
      </li>
      <li><a href="#profile" role="tab" data-toggle="tab">
          <i class="fa fa-user"></i> Add Events
          </a>
      </li>
      <li>
          <a href="#messages" role="tab" data-toggle="tab">
              <i class="fa fa-envelope"></i> Messages
          </a>
      </li>
      <li>
          <a href="#settings" role="tab" data-toggle="tab">
              <i class="fa fa-cog"></i> Settings
          </a>
      </li>
    </ul>
    
    <!-- Tab panes -->
    <div class="tab-content">
      <div class="tab-pane fade active in" id="home">
         



           <div class="col-md-8 temp"> 
          <form>
            <div class="col-md-6 form-line">
              <div class="form-group">
              <label for="exampleInputEmail">Temple Name</label>
              <select class="form-control">
                <option>Shree Mahamaya Kalika Saunsthan</option>
                <option>Shree Vetal Maharudra Saunsthan</option>
                <option>Shree Shantadurga Verlekarin Saunsthan</option>
                <option>Shree Shantadurga Sangodkarin Saunsthan</option>
                <option>Shree Gajantalkshmi Devasthan</option>
                <option>Shree Someshwar Devasthan</option>
                <option>Shree Mahalakshmi Devalay</option>
                <option>Shree Ram Mandir</option>
                <option>Shree Marutiray temple</option>
                <option>Shree Sansthan Maruti Gad</option>
                <option>Shree Datt Mandir</option>
                <option>Shree Ravalnath Sansthan</option>
                <option>Shree Shantadurga Chmundeshwasri Kudatari Mahamaya</option>
                <option>Shree Datt Mandir</option>
                <option>Shree Vimaleshwar Sansthan</option>
                <option>Shree Mahalasa Narayani Sansthan</option>
                <option>Shree Nagesh Mahamaya Sansthan</option>
                <option>Shree Radha Krishna Temple</option>
                <option>Shree Ravalghadi Temple</option>
                <option>Shree Bhagavati Haldonkarin</option>
                <option>Jnaneshwari Peeth</option>
                <option>Shri Shivnath Ravalnath temple</option>
                <option>Shree Mudgeri Mahalasa Sansthan</option>
                <option>Shree Ganpati Devasthan</option>
                <option>Shree Santeri</option>
                <option>Shree Lakshmi Narayan temple</option>
                <option>Shree Eshwar temple</option>
                <option>Shree Shanta Durga temple</option>
                <option>Shree Gayatri Temple</option>
                <option>Shree Lakshmi Narayan Temple</option>
                <option>Shree Lakshmi Venkatesha Temple</option>
                <option>Shree Gopalakrishna Swamy Temple</option>
              </select>
            </div> 
             <div class="form-group">
              <label for="telephone">God's Name</label>
              <input type="text" class="form-control" id="telephone" placeholder=" God's Name.">
            </div>
              <div class="form-group">
                <label>God's Pic</label>
                <a id='picUpload' href='#'></a>
                <input id='fileUpload' type="file"/>

              </div><div class="form-group">
                <label for ="description"> History</label>
                <textarea  class="form-control" id="description" placeholder="History"></textarea>
              </div></div>
            <div class="col-md-6">

             <div class="form-group">
              <label for="exampleInputEmail">Location</label>
              <select class="form-control">
                <option>Kasarpal Goa</option>
                <option>Mulgao Goa</option>
                <option>Marcela Goa</option>
                <option>Marcela Goa</option>
                <option>Sangolda Goa</option>
                <option>Zaumbaulim,Goa</option>
                <option>Benaulim Goa</option>
                <option>Margao Goa</option>
                <option>Mala Panjim, Goa</option>
                <option>Curchorem Goa</option>
                <option>Kucheli Bardes</option>
                <option>Shiroda Goa</option>
                <option>Curtorim Goa</option>
                <option>Ghudo Avede Goa</option>
                <option>Chinchinim Goa</option>
                <option>Rivona Goa</option>
                <option>Verna Goa</option>
                <option>Nagoa Goa</option>
                <option>Sanquelim Goa</option>
                <option>Advalpal</option>
                <option>Khandola Goa</option>
                <option>Karki</option>
                <option>Baad Karwar</option>
                <option>Mudgeri Karwar</option>
                <option>Bankikodla</option>
                <option>Karwar</option>
                <option>Kumta</option>
                <option>Ankola</option>
                <option>Murudeshwar</option>
                <option>Mangalore</option>
                <option>Bhatkal</option>
                <option>Cochin, Kerala</option>
</select>
            </div> 
            <div class="form-group">
              <label for="exampleInputEmail">Admin</label>
              <input type="text" class="form-control" id="exampleInputEmail" placeholder="Admin ">
            </div>  

            <div class="form-group">
              <label for="telephone">Mobile No.</label>
              <input type="tel" class="form-control" id="telephone" placeholder=" Enter 10-digit mobile no.">
            </div>
               <div class="form-group">
              <label for="telephone">Alternative Mobile No.</label>
              <input type="tel" class="form-control" id="telephone" placeholder=" Enter 10-digit mobile no.">
            </div>

 <div><button type="button" class="btn btn-default submit"><i class="fa fa-paper-plane" aria-hidden="true"></i> Submit</button>
            </div>

          </div> 
</form>

      </div>
   
   

 



      </div>
      <div class="tab-pane fade" id="profile">
          <div class="col-md-4 temp"> 
          <form>
            <div class="col-md-12">
              <div class="form-group">
              <label for="exampleInputEmail">Temple Name</label>
              <select class="form-control">
                <option>Shree Mahamaya Kalika Saunsthan</option>
                <option>Shree Vetal Maharudra Saunsthan</option>
                <option>Shree Shantadurga Verlekarin Saunsthan</option>
                <option>Shree Shantadurga Sangodkarin Saunsthan</option>
                <option>Shree Gajantalkshmi Devasthan</option>
                <option>Shree Someshwar Devasthan</option>
                <option>Shree Mahalakshmi Devalay</option>
                <option>Shree Ram Mandir</option>
                <option>Shree Marutiray temple</option>
                <option>Shree Sansthan Maruti Gad</option>
                <option>Shree Datt Mandir</option>
                <option>Shree Ravalnath Sansthan</option>
                <option>Shree Shantadurga Chmundeshwasri Kudatari Mahamaya</option>
                <option>Shree Datt Mandir</option>
                <option>Shree Vimaleshwar Sansthan</option>
                <option>Shree Mahalasa Narayani Sansthan</option>
                <option>Shree Nagesh Mahamaya Sansthan</option>
                <option>Shree Radha Krishna Temple</option>
                <option>Shree Ravalghadi Temple</option>
                <option>Shree Bhagavati Haldonkarin</option>
                <option>Jnaneshwari Peeth</option>
                <option>Shri Shivnath Ravalnath temple</option>
                <option>Shree Mudgeri Mahalasa Sansthan</option>
                <option>Shree Ganpati Devasthan</option>
                <option>Shree Santeri</option>
                <option>Shree Lakshmi Narayan temple</option>
                <option>Shree Eshwar temple</option>
                <option>Shree Shanta Durga temple</option>
                <option>Shree Gayatri Temple</option>
                <option>Shree Lakshmi Narayan Temple</option>
                <option>Shree Lakshmi Venkatesha Temple</option>
                <option>Shree Gopalakrishna Swamy Temple</option>
              </select>
            </div> 
             <div class="form-group">
              <label for="telephone">Event Name</label>
              <input type="text" class="form-control" id="telephone" placeholder=" Event Name">
            </div>
              <div class="form-group">
                <label>Event's Pic</label>
                <a id='picUpload' href='#'></a>
                <input id='fileUpload' type="file"/>

              </div>

  <div class="form-group">
              <label for="exampleInputEmail">Date And Time</label>
             
              <!-- Here by using Id selector the datetime picker will load on this input element -->
<input id="datetimepicker" class="form-control" type="text">
            </div>  


              <div class="form-group">
                <label for ="description"> Description</label>
                <textarea  class="form-control" id="description" placeholder="History"></textarea>
              </div>
<div><button type="button" class="btn btn-default submit1"><i class="fa fa-paper-plane" aria-hidden="true"></i> Submit</button>
            </div>

            </div>
            <div class="col-md-6"></div> 
</form>

      </div>
      </div>
      <div class="tab-pane fade" id="messages">


          <h2>Messages Content Goes Here</h2>
          







<script>
      $(document).ready(function() {
          $.datetimepicker.setLocale('pt-BR');
        $('#datetimepicker').datetimepicker();
      });
</script>










      </div>
      <div class="tab-pane fade" id="settings">
          <h2>Settings Content Goes Here</h2>
          
      </div>
    </div>
    
</div>








