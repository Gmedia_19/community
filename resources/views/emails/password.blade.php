<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="utf-8">
<link href="{{asset('home/login/css/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('home/login/css/style.css')}}" rel="stylesheet">
 <script type="text/javascript" src="{{asset('home/login/js/jquery.min.js')}}">
 </script>
  <script type="text/javascript" src="{{asset('home/login/js/bootstrap.min.js')}}">
  </script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">



<style>
.panel-body {
    padding: 15px;
    font-family: robot;
}
.btn-warning {
    color: #fff;
    background-color: #ffad00;
    border-color: #eea236;
}
</style>




</head>
	<body>
		<!-- <form ="roleform" method="post" action="/postResetPassword" >
            <h3>Reset Password</h3>
            {{ csrf_field() }}
        <input type="hidden" name= "userId" id="userId" value="{{ $userId }}"> 
        Password :  <input type="password"  id="password" name="password" placeholder="Password"/>
        Retype Password :  <input type="password"  id="confirmPassword" name="confirmPassword" placeholder="Confirm Password"/>

       
            <div>
                <button type="submit" class="btn btn-default submit">Reset Password</button>
            </div>
        </form> -->






 <br>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
              <div class="panel-body">
                <div class="text-center">
                  <h3><i class="fa fa-lock fa-4x"></i></h3>
                  <h2 class="text-center">Forgot Password?</h2>
                  {{ csrf_field() }}
                  <p>You can reset your password here.</p>
                  <div class="panel-body">
    
                    <form role="form" autocomplete="off" class="form" method="post" action="/postResetPassword">
                          <input type="hidden" name="userId" id="userId" value="{{ $userId }}">
                      <div class="form-group">
                        <div class="input-group">
                          <span class="input-group-addon">
                          <i class="fas fa-key"></i>
                

                        </span>

                          <input type="password"  id="password" name="password" placeholder="Password" class="form-contrl" />
                          </div>
                      </div>
                       <div class="form-group">
                        <div class="input-group">
                          <span class="input-group-addon">
                          <i class="fas fa-key"></i>

                        </span>
                          <input type="password" id="confirmPassword" name="confirmPassword" placeholder="Confirm Password" class="form-control" type="password">
                        </div>
                      </div>


                      <div class="form-group">
                        <input name="recover-submit" class="btn btn-lg btn-warning btn-block" value="Reset Password" type="submit" >
                      </div>
                      
                       
                    </form>
    
                  </div>
                </div>
              </div>
            </div>
          </div>
    </div>
</div>











	</body>
</html>