<!DOCTYPE html>
<html lang="en-US">
	<head>
	<meta charset="utf-8">
	</head>
	<body>
		<h3>Reset your Password</h3>

		Hi <strong>{{ $name }}</strong>,

		<p>There was recently a request to change the password for your account.

		If you requested this password change, please click on the following link to reset your password: </p>

		<h3><a href="{{$link}}"> Click here </a></h3>

		<p>If clicking the link does not work, please copy and paste the URL into your browser instead.


		If you did not make this request, you can ignore this message and your password will remain the same.</p>

		<h4>Thanks</h4>
		<p>Daivajnya brahmin</p>
	</body>
</html>