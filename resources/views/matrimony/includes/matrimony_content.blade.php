<!-- banner section -->
<div class="col-md-6" style="padding-left:0px !important;"> 
  <div class="reg col-md-5 hidden-xs" >
    <p style="font-size: 20px; font-family: robot" class="section-header">To Find Your Soulmate <span class="content-header"> &nbsp; Register Here    </span> </p> 
  </div>
  <div class="triangle-right col-md-1">
  </div>
  <div class="carousel fade-carousel slide" data-ride="carousel" data-interval="4000" id="bs-carousel" style="    margin-top:16px;">
    <!-- Overlay -->
    <div class="overlay"></div>
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#bs-carousel" data-slide-to="0" class="active"></li>
      <li data-target="#bs-carousel" data-slide-to="1"></li>
      <li data-target="#bs-carousel" data-slide-to="2"></li>
    </ol>
    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item slides active">
        <div class="slide-1"></div>
        <div class="hero">
        <!--<hgroup>
            <h1>We are creative</h1>        
            <h3>Get start your next awesome project</h3>
        </hgroup> 
        <button class="btn btn-hero btn-lg" role="button">See all features</button>-->
      </div>
    </div>
    <div class="item slides">
      <div class="slide-2"></div>
      <!--<div class="hero">        
           
        <button class="btn btn-hero btn-lg" role="button">See all features</button>
      </div> -->
    </div>
    <div class="item slides">
      <div class="slide-3"></div>
      <!-- <div class="hero">        
      
        <button class="btn btn-hero btn-lg" role="button">See all features</button>
      </div> -->
    </div>
  </div> 
</div>
</div>

<div class="col-md-6 form-box"" style="margin-top:5px;">
  <form role="form" class="registration-form"  id="addmatrimony">
    <fieldset>
      <div class="form-top">
        <div class="form-top-left">
          <h3><span><i class="fa fa-calendar-check-o" aria-hidden="true"></i></span> Account Information</h3>

        </div>
      </div>
      <div class="form-bottom">
        <div class="row"> 
         <div class="form-group col-md-12 col-sm-12">
           <input type="hidden" class="form-control" id="user" autocomplete='given-name'>
         </div>
         <div class="form-group col-md-6 col-sm-6">
          <input type="text" class="form-control" placeholder="Firstname" id="fname" name="firstName"  autocomplete='given-name'>
        </div>
        <div class="form-group col-md-6 col-sm-6">
          <input type="text" class="form-control" placeholder="Lastname" id="lname" name="lastName" autocomplete='given-name'>
        </div>
      </div>
      <div class="row">
        <div class="form-group col-md-6 col-sm-6">
          <select class="form-control" id="profile">
            <option>Profile For</option>
            <option>self</option>
            <option>daughter</option>
            <option>son </option>
            <option> relative/friend</option>
            <option> sister</option>
            <option> brother</option>
            <option> client marriage bureau</option>
          </select>
        </div>
    <div class="form-group col-md-6 col-sm-6">
         <select class="form-control" id="gender">
          <option>Gender</option>
          <option>Male</option>
          <option>Female</option>
        </select> 
      </div>
    </div>
    <div class="row">
       <div class="form-group col-md-6 col-sm-6">
         <select class="form-control" id="mstatus">
          <option>Martial Status</option>
          <option>Unmarried</option>
          <option>Remarriage</option>
        </select> 
      </div>
     
    <div class="form-group col-md-6 col-sm-6">
      <input type="text" class="form-control" placeholder="Contact No" id="phno" autocomplete='given-name'>
    </div>
    <div class="form-group col-md-6 col-sm-6">
      <input type="text" class="form-control" placeholder="Alternative Contact No" id="altphno" autocomplete='given-name'>
    </div>

 <div class="form-group col-md-6 col-sm-6">
        <input type="text" class="form-control" placeholder="Age" id="age" autocomplete='given-name'>
      </div>
  <!--   <div class="form-group col-md-6 col-sm-6">
      <input type="email" class="form-control" placeholder="Enter email" id="email" autocomplete='given-name'>
    </div> -->
  </div>

  <button type="button" class="btn btn-next">Next</button>
</div>
</fieldset>
<fieldset>
  <div class="form-top">
    <div class="form-top-left">
      <h3><span><i class="fa fa-calendar-check-o" aria-hidden="true"></i></span>Account Information</h3>

    </div>
  </div>
  <div class="form-bottom">
    <div class="row">
     
      <div class="form-group col-md-6 col-sm-6">
        <select class="form-control" id="height">
          <option>Select Height</option>
          <option value="volvo">4ft 5in - 134cm</option>
          <option value="saab">4ft 6in - 137cm</option>
          <option value="saab">4ft 7in - 139cm</option>
          <option value="opel">4ft 8in - 142cm</option>
          <option value="audi">4ft 9in - 144cm</option>
          <option value="audi">4ft 10in - 147cm</option>
          <option value="audi">4ft 11in - 149cm</option>
          <option value="audi">5ft - 152cm</option>
          <option value="audi">5ft 1inch - 154cm</option>
          <option value="audi">5ft 2in - 157cm</option>
          <option value="audi">5ft 3in - 160cm</option>
          <option value="audi">5ft 4in - 162cm</option>
          <option value="audi">5ft 5in - 165cm</option>
          <option value="audi">5ft 6in - 167cm</option>
          <option value="audi">5ft 7in - 170cm</option>
          <option value="audi">5ft 8in - 172cm</option>
          <option value="audi">5ft 9in - 175cm</option>
          <option value="audi">5ft 10in - 177cm</option>
          <option value="audi">5ft 11in - 180cm</option>
          <option value="audi">6ft - 182cm</option>
          <option value="audi">6ft 1in - 185cm</option>
          <option value="audi">6ft 2in - 187cm</option>
          <option value="audi">6ft 3n - 190cm</option>
          <option value="audi">6ft 4in - 193cm</option>
          <option value="audi">6ft 5in - 195cm</option>
          <option value="audi">6ft 6in - 198cm</option>
          <option value="audi">6ft 7in - 200cm</option>
          <option value="audi">6ft 8in-203cm</option>
          <option value="audi">6ft 9in-205cm</option>
          <option value="audi">6ft 10in-208cm</option>
          <option value="audi">6ft 11in-210cm</option>
          <option value="audi">7ft-213cm</option>

        </select>
      </div>
       <div class="form-group col-md-6 col-sm-6">
        <select class="form-control" id="color">
          <option>Chosee your color</option>
          <option>Fair </option>
          <option>Medium Fair</option>
          <option>Black </option>
        </select>
      </div>
    </div>

    <div class="row">
     
      <div class="form-group col-md-6 col-sm-6">
        <input type="date" placeholder="Enter Blood Group" class="form-control" id="btdate" autocomplete='given-name'>

      </div>
      <div class="form-group col-md-6 col-sm-6">
       <input type="text" class="form-control" placeholder="Enter Blood Group" id="bloodgp" autocomplete='given-name'>
     </div>
    </div>

    <div class="row">
      
     <div class="form-group col-md-6 col-sm-6">
       <input type="text" class="form-control" placeholder="Enter Birth Place" id="btplace" autocomplete='given-name'>
     </div>
      <div class="form-group col-md-6 col-sm-6">
      <input type="text" class="form-control" placeholder="Enter Birth Time" id="bttime" autocomplete='given-name'>
    </div>

   </div>
   <div class="row">

   
    <div class="form-group col-md-6 col-sm-6">
     <input type="text" class="form-control" placeholder="Enter Ganna" id="ganna" autocomplete='given-name'>
   </div>
   <div class="form-group col-md-6 col-sm-6">
   <input type="text" class="form-control" placeholder="Enter Gothra" id="gothra" autocomplete='given-name'>
 </div>

 </div>
 <div class="row">
  
 <div class="form-group col-md-6 col-sm-6">
  <input type="text" class="form-control" placeholder="Enter Nakshathra" id="nakshathra" autocomplete='given-name'>
</div>
</div>
<button type="button" class="btn btn-previous">Previous</button>
<button type="button" class="btn btn-next">Next</button>
</div> 
</fieldset>
<fieldset>
  <div class="form-top">
    <div class="form-top-left">
      <h3><span><i class="fa fa-calendar-check-o" aria-hidden="true"></i></span> Account Details</h3>
    </div>
  </div>
  <div class="form-bottom">
   <div class="row">

    <div class="form-group col-md-6 col-sm-6">
     <input type="text" class="form-control" placeholder="Enter Rashi" id="rashi" autocomplete='given-name'>
   </div>


   <div class="form-group col-md-6 col-sm-6">
     <select class="form-control" id="qualification">
      <option>Highest Qualification</option>
      <option>Doctorate</option>
      <option>Masters</option>
      <option value="">Honours Degree</option>
      <option value="">Bachelors</option>
      <option value="">Undergraduate</option>
      <option value="">Associates degree</option>
      <option value="">Diploma</option>
      <option value="">High school</option>
      <option value="">Less than high school</option>
      <option value="">Trade school</option>
    </select>
  </div>
</div>
<div class="row">

  <div class="form-group col-md-6 col-sm-6">
   <select class="form-control" id="education">
    <option>Education Field</option>
    <option value="">Advertising/Marketing</option>
    <option value="">Administrative services</option>
    <option value="">Architecture</option>
    <option value="">Armed Forces</option>
    <option value="">Arts</option>
    <option value="">Commerce</option>
    <option value="">Computers/It</option>
    <option value="">Education</option>
    <option value="">Engineering/Technology</option>
    <option value="">Fashion</option>
    <option value="">Fine Arts</option>
    <option value="">Home Science</option>
    <option value="">Law</option>
    <option value="">Management</option>
    <option value="">Medicine</option>
    <option value="">Nursing/Health sciences</option>
    <option value="">Office administration</option>
    <option value="">Science</option>
    <option value="">Shipping</option>
    <option value="">Travel and Tourism</option>
    <option value="">Other</option>
  </select>
</div>
<div class="form-group col-md-6 col-sm-6">
 <input type="text" class="form-control" placeholder="Working in" id="working" autocomplete='given-name'>
</div>
</div>
<div class="row">

 <div class="form-group col-md-6 col-sm-6">
   <input type="text" class="form-control" placeholder="Working As" id="workingas" autocomplete='given-name'>
 </div>

 <div class="form-group col-md-6 col-sm-6">
   <input type="text" class="form-control" placeholder="Enter Ambition" id="ambition" autocomplete='given-name'>
 </div>

</div>


<div class="row">

  <div class="form-group col-md-6 col-sm-6">
    <textarea class="form-control rounded-0" id="hobbies" rows="3" placeholder="Enter Hobbies" autocomplete='given-name'></textarea>

  </div>


  <div class="form-group col-md-6 col-sm-6">
    <textarea class="form-control rounded-0" id="expectation" rows="3" placeholder="Expectations" autocomplete='given-name'></textarea>

  </div>

</div>
<div class="row">

 <div class="form-group col-md-6 col-sm-6">
  <input type="email" class="form-control" placeholder="Income" id="income" autocomplete='given-name'>
</div>
</div>
<button type="button" class="btn btn-previous">Previous</button>
<button type="button" class="btn btn-next">Next</button>
</div>
</fieldset>

<fieldset> <div class="form-top">
  <div class="form-top-left">
    <h3><span><i class="fa fa-calendar-check-o" aria-hidden="true"></i></span> Family Details</h3>

  </div>
</div>
<div class="form-bottom">
  <div class="row">
    <div class="form-group col-md-6 col-sm-6">

     <input type="text" class="form-control" placeholder="Present City" id="presentcity" autocomplete='given-name'>

   </div>
   <div class="form-group col-md-6 col-sm-6">
    <input type="text" class="form-control" placeholder="Permanent City" id="permanentcity" autocomplete='given-name'>
  </div>
</div>
<div class="row">
  <div class="form-group col-md-6 col-sm-6">
   <input type="text" class="form-control" placeholder="Country" id="country" autocomplete='given-name'>
 </div>
 <div class="form-group col-md-6 col-sm-6">

   <input type="text" class="form-control" placeholder="Father Name" id="father" autocomplete='given-name'>

 </div>

</div>

<div class="row">


 <div class="form-group col-md-6 col-sm-6">

   <input type="text" class="form-control" placeholder="Father Profession" id="fprofession" autocomplete='given-name'>
 </div>

 <div class="form-group col-md-6 col-sm-6">
   <input type="text" class="form-control" placeholder="Mother Name" id="mother" autocomplete='given-name'>
 </div>

</div>

<div class="row">

 <div class="form-group col-md-6 col-sm-6">
   <input type="text" class="form-control" placeholder="Mother Profession" id="mprofession" autocomplete='given-name'>
 </div>

 <div class="form-group col-md-6 col-sm-6">
  <select class="form-control" id="siblings" autocomplete='given-name'>
    <option>Siblings</option>
    <option value="">0</option>
    <option value="">1</option>
    <option value="">2 </option>
    <option value="">3</option>
    <option value="">4</option>
    <option value="">5</option>
    <option value="">6</option>
    <option value="">7</option>
    <option value="">8</option>
    <option value="">9</option>
    <option value="">10</option>

  </select>
</div>
</div>
<div class="row">
  <div class=" col-md-6 col-sm-6">
    <label>Upload user photo</label>
    <input type='file' onchange="readURL(this);" id="userpic" />
  <!--   <img id="blah" src="http://placehold.it/180" alt="your image" style="max-width:100px;
    height:100px;
    margin-top:20px;" /> -->

  </div>
  <div class=" col-md-6 col-sm-6">
      <label>Upload kundli photo</label>
    <input type='file' onchange="readURL(this);" id="kundlipic" />
   <!--  <img id="blah" src="http://placehold.it/180" alt="your image" style="max-width:100px;
    height:100px;
    margin-top:20px;" /> -->

  </div>
</div>
<div id="alrt"> </div>
<button type="button" class="btn btn-previous">Previous</button>
<button type="submit" class="btn" onclick="addmt(event)">Submit</button> 

</div>
</fieldset>
</form>
</div>
<div class="clearfix"> </div>

<!--  <div class=" col-md-6 col-sm-6">
                                      <input type='file' onchange="readURL(this);" id="pht" />
                                        <img id="blah" src="http://placehold.it/180" alt="your image" style="max-width:100px;
  height:100px;
  margin-top:20px;" />
  
</div> -->
<script type="text/javascript">



  sessionStorage.setItem('base64',"");

  function readURL(element) {
   var file = element.files[0];
   var reader = new FileReader();
   reader.onloadend = function() {

     var a =reader.result;
     var base64result = reader.result.split(',')[1];

         // window.a1=base64result;
         console.log( base64result);
         //alert(base64result);

         //alert(base64);
         sessionStorage.setItem('base64',base64result);
       }
       reader.readAsDataURL(file);
        // alert("leaving");
      }






    </script>




    <script type="text/javascript">
      $(document).ready(function(){



        //var name = sessionStorage.getItem('userId');
    //var nam1=name;
       //$("#user").html(nam1);
      // $("#user").val(name);
//alert(name);
});

// $(function(){
// $('a[title]').tooltip();
// });

</script>








































