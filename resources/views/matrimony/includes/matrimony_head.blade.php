 <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <link href="{{asset('matrimony/css/bootstrap.min.css')}}" rel="stylesheet">
 <link href="{{asset('matrimony/css/style.css')}}" rel="stylesheet">
    <!-- Calendar -->
 <!-- <link href="{{asset('matrimony/css/jquery-ui.css')}}" rel="stylesheet"> -->
 <link href="{{asset('matrimony/css/font-awesome.min.css')}}" rel="stylesheet">
 <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
 <script type="text/javascript" src="{{asset('matrimony/js/jquery.min.js')}}"> 
 </script> 
 <script type="text/javascript" src="{{asset('matrimony/js/bootstrap.min.js')}}">
 </script> 
  <script type="text/javascript" src="{{asset('matrimony/js/main.js')}}">
 </script>
 <script type="text/javascript" src="{{asset('matrimony/js/matrimony.js')}}"></script>


<!-- //Calendar -->
<!--  -->
<style>
/*
Fade content bs-carousel with hero headers
Code snippet by maridlcrmn (Follow me on Twitter @maridlcrmn) for Bootsnipp.com
Image credits: unsplash.com
*/

/********************************/
/*       Fade Bs-carousel       */
/********************************/
.fade-carousel {
    position: relative;
     height:400px;
}
.fade-carousel .carousel-inner .item {
    height: 100vh;
}
.fade-carousel .carousel-indicators > li {
    margin: 0 2px;
    background-color: #f39c12;
    border-color: #f39c12;
    opacity: .7;
}
.fade-carousel .carousel-indicators > li.active {
  width: 10px;
  height: 10px;
  opacity: 1;
}

/********************************/
/*          Hero Headers        */
/********************************/
.hero {
    position: absolute;
    top: 50%;
    left: 50%;
    z-index: 3;
    color: #fff;
    text-align: center;
    text-transform: uppercase;
    text-shadow: 1px 1px 0 rgba(0,0,0,.75);
      -webkit-transform: translate3d(-50%,-50%,0);
         -moz-transform: translate3d(-50%,-50%,0);
          -ms-transform: translate3d(-50%,-50%,0);
           -o-transform: translate3d(-50%,-50%,0);
              transform: translate3d(-50%,-50%,0);
}
.hero h1 {
    font-size: 6em;    
    font-weight: bold;
    margin: 0;
    padding: 0;
}

.fade-carousel .carousel-inner .item .hero {
    opacity: 0;
    -webkit-transition: 2s all ease-in-out .1s;
       -moz-transition: 2s all ease-in-out .1s; 
        -ms-transition: 2s all ease-in-out .1s; 
         -o-transition: 2s all ease-in-out .1s; 
            transition: 2s all ease-in-out .1s; 
}
.fade-carousel .carousel-inner .item.active .hero {
    opacity: 1;
    -webkit-transition: 2s all ease-in-out .1s;
       -moz-transition: 2s all ease-in-out .1s; 
        -ms-transition: 2s all ease-in-out .1s; 
         -o-transition: 2s all ease-in-out .1s; 
            transition: 2s all ease-in-out .1s;    
}

/********************************/
/*            Overlay           */
/********************************/
.overlay 
{
  position: absolute;
    width: 100%;
   height:400px;
    z-index: 2;
   /*  background-color: #080d15; */
    opacity: .7;
}
}

/********************************/
/*          Custom Buttons      */
/********************************/
.btn.btn-lg {padding: 10px 40px;}
.btn.btn-hero,
.btn.btn-hero:hover,
.btn.btn-hero:focus {
    color: #f5f5f5;
    background-color: #1abc9c;
    border-color: #1abc9c;
    outline: none;
    margin: 20px auto;
}

/********************************/
/*       Slides backgrounds     */
/********************************/
.fade-carousel .slides .slide-1, 
.fade-carousel .slides .slide-2,
.fade-carousel .slides .slide-3 {
 height:58vh;
  background-size: cover;
  background-position: center center;
  background-repeat: no-repeat;
}
.fade-carousel .slides .slide-1 {
  background-image: url('{{asset('matrimony/images/banner2.jpg') }}'); 
}
.fade-carousel .slides .slide-2 {
  background-image: url('{{asset('Jewellery Hub/images/banner1.jpg') }}');
}
.fade-carousel .slides .slide-3 {
  background-image: url('{{asset('Jewellery Hub/images/banner1.jpg') }}');
}

/********************************/
/*          Media Queries       */
/********************************/
@media screen and (min-width: 980px){
    .hero { width: 980px; }    
}
@media screen and (max-width: 640px){
    .hero h1 { font-size: 4em; }    
}

.carousel-inner {
    position: relative;
    width: 100%;
height:400px;
    overflow: hidden;
  

}

.btn
{
  border-radius:1px;
}



.btn-info {
    color: #fff;
    background-color: #ffad00;
    border-color: #f4f3f2;
}


.btn-info:hover {
    color: #fff;
    background-color: #e7bd01;
    border-color: #d3ac45;
}


.btn-info.focus, .btn-info:focus {
    color: #fff;
    background-color: #e1aa01;
    border-color: #ffad00;
}


.list-group-item {
    position: relative;
    display: block;
       padding: 4px 15px;
    margin-bottom: -1px;
    background-color: #fff;
    border: 0px solid #ddd;
}


#preview img{ height:100px; }
</style>
</head>

