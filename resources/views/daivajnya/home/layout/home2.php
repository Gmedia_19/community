<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> 
<html lang="en"> <!--<![endif]-->  
<head>
    <title>Daivajnyabrahmin</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">    
    <link rel="shortcut icon" href="favicon.ico">  
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,500,300,600,900,800,700,200,100' rel='stylesheet' type='text/css'>
    <!-- Global CSS -->
    
    <link rel="stylesheet" href="daivajnya/assets/plugins/bootstrap/css/bootstrap.min.css">   
    <!-- Plugins CSS -->    
    <link rel="stylesheet" href="daivajnya/assets/plugins/jasny-bootstrap/css/jasny-bootstrap.min.css">
    <link rel="stylesheet" href="daivajnya/assets/plugins/font-awesome/css/font-awesome.css">  
    <link rel="stylesheet" href="daivajnya/assets/plugins/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
    <link rel="stylesheet" href="daivajnya/assets/plugins/pe-icon-7-stroke/css/helper.css">   
    <link rel="stylesheet" href="daivajnya/assets/plugins/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="daivajnya/assets/plugins/owl-carousel/owl.theme.css">       
    <link rel="stylesheet" href="daivajnya/assets/plugins/fullPage.js/jquery.fullPage.css">
    <!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous"> -->
    <!-- Theme CSS -->
    <link id="theme-style" rel="stylesheet" href="daivajnya/assets/css/styles.css">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

<style>
.owl-item
{
        height: 482px;
}

.filter
{
     -webkit-filter: grayscale(100%); /* Safari 6.0 - 9.0 */
    filter: grayscale(100%);
    height:50px;
}
</style>
</head> 

<body class="homepage">       
    
    <nav id="myNavmenu" class="navmenu navmenu-default navmenu-fixed-right text-center" role="navigation">
        <ul class="list-unstyled menu-links">
            <li class="nav-item active"><a href="home2">Home</a></li>
            <li class="nav-item"><a href="about">About</a></li>
           <!--  <li class="nav-item"><a href="press.html">Press</a></li> -->
            <li class="nav-item"><a href="pricing">Pricing</a></li>
            <li class="nav-item"><a href="faqs">FAQs</a></li>
           <!--  <li class="nav-item"><a href="jobs.html">Jobs</a></li>
            <li class="nav-item"><a href="blog.html">Blog</a></li> -->
            <li class="nav-item"><a href="contact">Contact</a></li>
           
            <!-- <li class="nav-item dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="#">Options<i class="fa fa-angle-down"></i></a>
                <ul class="dropdown-menu">
                    <li><a href="index-2.html">iPhone 6 Plus (Gold)</a></li>
                    <li><a href="index-iphone-2.html">iPhone 6 Plus (Space Grey)</a></li>
                    <li><a href="index-android.html">Android</a></li>                   
                </ul>                            
            </li> -->
        </ul>
        <ul class="social-icons list-inline">
            <li><a href="https://twitter.com"><i class="fa fa-twitter"></i></a></li>                        
            <li><a href="https://www.facebook.com"><i class="fa fa-facebook"></i></a></li>
            <li><a href="https://www.facebook.com"><i class="fa fa-instagram"></i></a></li>
            <li><a href="https://www.linkedin.com"><i class="fa fa-linkedin"></i></a></li>                    
        </ul> 
        
        <ul class="list-unstyled download-buttons">
            <li class="app-store"><a href="https://itunes.apple.com/in/app/daivajnyabrahmin/id1431653216?mt=8"><img class="img-responsive" src="daivajnya/assets/images/buttons/btn-app-store.png" alt="Download from App Store" /></a></li>
            <li class="google-play"><a href="https://play.google.com/store/apps/details?id=com.gmediasolutions.app.daivajnyabrahmin"><img class="img-responsive" src="daivajnya/assets/images/buttons/btn-google-play.png" alt="Download from Google Play" /></a></li>
           <!--  <li class="windows-store"><a href="#"><img class="img-responsive" src="assets/images/buttons/btn-windows-store.png" alt="Download from Windows Phone Store" /></a></li> -->
        </ul> 
        
    </nav>
    
    <div class="canvas-wrapper">    
        <!-- ******HEADER****** --> 
        <header id="header" class="header navbar-fixed-top">  
            <div class="container">
           <div class="col-md-12">
<div class="logo" style="margin-top: 8px;">
              <img src="images/logo.png" class="img-responsive" style="height: 90px;">

          </div>

          <div class="logo1" style="display:inline-block;"> <img src="images/logo2.png" class="img-responsive" >
          </div>

           </div>


                <div class="image col-md-5">
                <div class="logo">
              <!--       <a href="index-2.html"> tttt</a> -->
                <!--   <img src="images/logo.png" class="img-responsive">
                   <img src="images/logo1.png" class="img-responsive"> -->


                <!--  <img src="images/logo1.png" class="img-responsive"> -->


                </div><!--//logo-->

            </div>


            <!--  <div class="image col-md-4">
                <div class="logo">
          
                  <img src="images/logo1.png" class="img-responsive">
                </div>

            </div> -->
                <div class="social-container text-center">
                    <!-- <ul class="social-icons list-inline">
                        <li><a href="https://twitter.com/3rdwave_themes"><i class="fa fa-twitter"></i></a></li>                        
                        <li><a href="https://www.facebook.com/3rdwavethemes"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://www.facebook.com/3rdwavethemes"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>                    
                    </ul>  -->


                </div>            
                
                <div class="navbar-wrapper">
                  <span class="navbar-label">MENU</span>
                  <button type="button" class="navbar-toggle" data-toggle="offcanvas" data-target="#myNavmenu" data-recalc="false" data-canvas="body">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                </div><!--//navbar -->
            </div><!--//container-->
        </header><!--//header-->   
      
        <div id="fullpage" class="sections-wrapper">
            
            <!-- ******Promo Section****** -->
            <section id="promo" class="promo section gradient-1">
                <div class="container">
                    <div class="row">    
                      <br>    <br>                                   
                        <div class="intro-area col-md-4 col-sm-12 col-xs-12">
                            <h2 class="title">Meet Daivajnya Brahmin App</h2>
                            <p class="intro">Daivajnya Brahmin is a community app, it have the features of my profile, matrimony, B2B, Temples, Daivajnya Samaj, Jewellery hub, News and events, Connect to Daivajnya Brahmin all over the world by this app.</p>
                            <div class="video-container">
                                <img src="images/logo.png" alt="video" />
                                <a class="play-trigger" href="#modal-video" data-toggle="modal" data-target="#modal-video" data-backdrop="static" data-keyboard="false"><i class="fa fa-play"></i></a>
                            </div><!--//video-container-->             
                        </div><!--//intro-area-->
                        <div class="clearfix visible-sm-block visible-xs-block"></div>
                        <div class="download-area col-md-4 col-sm-6 col-xs-12 col-md-push-4 col-sm-push-6 col-xs-push-0">
                            <ul class="list-unstyled download-buttons">
                                <li class="app-store"><a href="https://itunes.apple.com/in/app/daivajnyabrahmin/id1431653216?mt=8"><img class="img-responsive" src="daivajnya/assets/images/buttons/btn-app-store.png" alt="Download from App Store" /></a></li>
                                <li class="google-play"><a href="https://play.google.com/store/apps/details?id=com.gmediasolutions.app.daivajnyabrahmin"><img class="img-responsive" src="daivajnya/assets/images/buttons/btn-google-play.png" alt="Download from Google Play" /></a></li>
                             
                            </ul>
                            <ul class="list-unstyled summary">
                              

                                    <li> <!-- <i class="fa fa-check"></i>  --> <img src="daivajnya/assets/images/icons/9.png"><a href="#benefit1">&nbsp;New</a></li>

                             <li>  <img src="daivajnya/assets/images/icons/1.png" class="ic"><a href="#benefit2">&nbsp;&nbsp; My Profile</a></li>
                                <li>


                                <li>
                                <img src="daivajnya/assets/images/icons/2.png" class="ic"> <a href="#benefit3"> &nbsp;&nbsp;Db Hub</a></li>
                                <li>
                        <img src="daivajnya/assets/images/icons/3.png" class="ic">  <a href="#benefit4"> &nbsp;&nbsp;News and events</a></li>
                <li> <img src="daivajnya/assets/images/icons/4.png" class="ic">  <a href="#benefit5">&nbsp;&nbsp;Jewellery Hub</a></li>
        <li> <img src="daivajnya/assets/images/icons/5.png" class="ic">  <a href="#benefit6">&nbsp;&nbsp;Matrimony</a></li>
        <li> <img src="daivajnya/assets/images/icons/6.png" class="ic">  <a href="#benefit7">&nbsp;&nbsp;B2B</a></li>
    <li> <img src="daivajnya/assets/images/icons/7.png" class="ic">  <a href="#benefit8">&nbsp;&nbsp;Careers</a></li>
        <li> <img src="daivajnya/assets/images/icons/8.png" class="ic">  <a href="#benefit9">&nbsp;&nbsp;Temples</a></li>
                            


                            </ul><!--//list-unstyled-->
                        </div><!--//download-area--> 
                        <div class="clearfix visible-xs-block"></div>
                        <div class="device device-iphone text-center col-md-4 col-sm-6 col-xs-12 col-md-pull-4 col-sm-pull-6 col-xs-pull-0">     
                            <div class="device-holder iphone-holder iphone-gold-holder offset-top">                 
                                <div class="device-holder-inner">
                                    <div id="home-slideshow" class="owl-carousel owl-theme"> 
                                      <div class="item"><img class="img-responsive" src="daivajnya/assets/images/devices/screens/main.jpg" alt=""></div>
                                      <div class="item"><img class="img-responsive" src="daivajnya/assets/images/devices/screens/login.jpg" alt=""></div>
                                      <div class="item"><img class="img-responsive" src="daivajnya/assets/images/devices/screens/sign-up.jpg" alt=""></div>
                                      <div class="item"><img class="img-responsive" src="daivajnya/assets/images/devices/screens/my-profile.jpg" alt=""></div>  
                            <div class="item"><img class="img-responsive" src="daivajnya/assets/images/devices/screens/jhub.jpg" alt=""></div> 
                               <div class="item"><img class="img-responsive" src="daivajnya/assets/images/devices/screens/groom.jpg" alt=""></div> 
                                  <div class="item"><img class="img-responsive" src="daivajnya/assets/images/devices/screens/b2b.jpg" alt=""></div> 
                                     <div class="item"><img class="img-responsive" src="daivajnya/assets/images/devices/screens/temple.jpg" alt=""></div> 
 <div class="item"><img class="img-responsive" src="daivajnya/assets/images/devices/screens/events.jpg" alt=""></div> 



                                    </div><!--//slideshow-->
                                </div><!--//device-holder-inner-->  
                            </div><!--//device-holder-->                  
                        </div><!--//phone-holder--> 
                        <div class="clearfix"></div>
                        <div class="arrow-holder text-center">
                            <p class="lead-text"><a href="#benefit1">Find out more</a></p>
                            <a href="#benefit1" class="animate pe-7s-bottom-arrow pe-3x"></a>
                        </div><!--//arrow-holder-->
                    </div><!--//row-->
                </div><!--//container-->            
            </section><!--//promo-->
            
            <!-- ******Benefit-1 Section****** -->

 <section id="benefit-1" class="benefit-2 section gradient-3 has-pattern">
                <div class="section-inner">
                    <div class="container text-center">
                        <div class="row">
                            <div class="content col-md-7 col-sm-6 col-xs-12 col-md-push-4 col-sm-push-6 col-xs-push-0">
                                <div class="content-inner">
                                    <!-- <span class="pe-icon pe-7s-camera pe-4x"></span> -->
                                   <img src="daivajnya/assets/images/icons/db_new.png" >
                                    <h2 class="title text-center">Find latest offers</h2>
                                    <p class="intro text-center"> You can view the News Offers and news updates about app </p>
                                </div><!--//content-inner-->
                                <div class="testimonial text-center">                                          
                                   <!--  <div class="quote-container">
                                        <i class="fa fa-quote-left"></i>
                                        <blockquote class="quote">
                                            This is a great app and it's very easy to use bibendum cursus urna, quis rhoncus arcu. Curabitur vel sollicitudin leo.                                
                                        </blockquote>
                                        <div class="source">
                                            <img class="profile" src="daivajnya/assets/images/profiles/profile-2.png" alt="" />
                                            <p class="meta">
                                                <span class="source-name"><a href="#">@GeraldChavez</a></span>
                                               
                                            </p>
                                        </div>
                                    </div>   -->                
                                </div><!--//testimonial-->
                            </div><!--//content-->
                            <div class="device device-iphone col-md-4 col-sm-6 col-xs-12 col-md-pull-7 col-sm-pull-6 col-xs-pull-0">     
                                <div class="device-holder iphone-holder iphone-gold-holder offset-top">                 
                                    <div class="device-holder-inner">
                                        <img class="img-responsive" src="daivajnya/assets/images/devices/screens/main.jpg" alt=""  style=" height:482px;width: 270px;">
                                    </div><!--//device-holder-inner-->  
                                </div><!--//device-holder-->                  
                            </div><!--//phone-holder-->                         
                        </div><!--//row-->
                    </div><!--//container-->
                </div><!--//section-inner-->
            </section><!--//benefit-2-->

            <section id="benefit-2" class="benefit-1 section gradient-2 has-pattern">
                <div class="section-inner">
                    <div class="container text-center">
                        <div class="row">
                            <div class="content col-md-7 col-sm-6 col-xs-12">
                                <div class="content-inner">
                                <!--     <span class="pe-icon pe-7s-coffee pe-4x"></span> -->
                                 <img src="daivajnya/assets/images/icons/profile.png" >
                                    <h2 class="title text-center">My Profile</h2>
                                    <p class="intro text-center">My Profile helps us to find and connect with daivajnyabrahmin community peoples and you can post the your favorite  pictures.The like functionality allows users to give positive feedback about preferred content</p>
                                </div><!--//content-inner-->
                                <div class="testimonial text-center">                                          
                                    <!-- <div class="quote-container">
                                        <i class="fa fa-quote-left"></i>
                                        <blockquote class="quote">
                                            Back up the above benefit/feature with a relevant tweet/testimonial. It's great that Atom app gives me a chance to pulvinar orci non. Nulla suscipit dolor mauris. Morbi interdum ut ante vel lacinia.                              
                                        </blockquote>
                                        <div class="source">
                                            <img class="profile" src="mg-responsive" src="daivajnya/assets/images/icons/my_profile.png" alt=""/>
                                            <p class="meta">
                                                <span class="source-name"><a href="#">Heather Alexander</a></span>
                                               
                                            </p>
                                        </div>
                                    </div>       -->             
                                </div><!--//testimonial-->
                            </div><!--//content-->
                            <div class="device device-iphone col-md-4 col-sm-6 col-xs-12">     
                                <div class="device-holder iphone-holder iphone-gold-holder offset-top">                 
                                    <div class="device-holder-inner">
                                        <img class="img-responsive" src="daivajnya/assets/images/devices/screens/my-profile.jpg" alt=""  style=" height: 482px;    width: 270px;">
                                    </div><!--//device-holder-inner-->  
                                </div><!--//device-holder-->                  
                            </div><!--//phone-holder-->                         
                        </div><!--//row-->
                    </div><!--//container-->
                </div><!--//section-inner-->
            </section><!--//benefit-1-->
            
            <!-- ******Benefit-2 Section****** -->
            <section id="benefit-3" class="benefit-2 section gradient-3 has-pattern">
                <div class="section-inner">
                    <div class="container text-center">
                        <div class="row">
                            <div class="content col-md-7 col-sm-6 col-xs-12 col-md-push-4 col-sm-push-6 col-xs-push-0">
                                <div class="content-inner">
                                    <!-- <span class="pe-icon pe-7s-camera pe-4x"></span> -->
                                   <img src="daivajnya/assets/images/icons/db.png" >
                                    <h2 class="title text-center">DB HUb</h2>
                                    <p class="intro text-center">DB HUb helps us to add DB HUb Details.</p>
                                </div><!--//content-inner-->
                                <div class="testimonial text-center">                                          
                                    <!-- <div class="quote-container">
                                        <i class="fa fa-quote-left"></i>
                                        <blockquote class="quote">
                                            This is a great app and it's very easy to use and it connect with daivajnyabrahmin community peoples                               
                                        </blockquote>
                                        <div class="source">
                                            <img class="profile" src="daivajnya/assets/images/profiles/profile-2.png" alt="" />
                                            <p class="meta">
                                                <span class="source-name"><a href="#">@Somesh</a></span>
                                               
                                            </p>
                                        </div>
                                    </div>  -->                  
                                </div><!--//testimonial-->
                            </div><!--//content-->
                            <div class="device device-iphone col-md-4 col-sm-6 col-xs-12 col-md-pull-7 col-sm-pull-6 col-xs-pull-0">     
                                <div class="device-holder iphone-holder iphone-gold-holder offset-top">                 
                                    <div class="device-holder-inner">
                                        <img class="img-responsive" src="daivajnya/assets/images/devices/screens/dbhub.jpg" alt=""  style=" height: 482px;    width: 270px;">
                                    </div><!--//device-holder-inner-->  
                                </div><!--//device-holder-->                  
                            </div><!--//phone-holder-->                         
                        </div><!--//row-->
                    </div><!--//container-->
                </div><!--//section-inner-->
            </section><!--//benefit-2-->
            
            <!-- ******Benefit-3 Section****** -->
            <section id="benefit-4" class="benefit-3 section gradient-4 has-pattern">
                <div class="section-inner">
                    <div class="container text-center">
                        <div class="row">
                            <div class="content col-md-7 col-sm-6 col-xs-12">
                                <div class="content-inner">
                              <img src="daivajnya/assets/images/icons/news.png">
                                    <h2 class="title text-center">Latest News and Events</h2>
                                    <p class="intro text-center"> Here you will find the latest news, features from around the world. </p>
                                </div><!--//content-inner-->
                                <div class="testimonial text-center">                                          
                                    <!-- <div class="quote-container">
                                        <i class="fa fa-quote-left"></i>
                                        <blockquote class="quote">
                                            We will get know the latest news, it's very helpfull.                           
                                        </blockquote>
                                        <div class="source">
                                            <img class="profile" src="daivajnya/assets/images/profiles/profile-3.png" alt="" />
                                            <p class="meta">
                                                <span class="source-name"><a href="#">Kelly Ryan</a></span>
                                                
                                            </p>
                                        </div>
                                    </div>     -->          
                                </div><!--//testimonial-->
                            </div><!--//content-->
                            <div class="device device-iphone col-md-4 col-sm-6 col-xs-12">     
                                <div class="device-holder iphone-holder iphone-gold-holder offset-top">                 
                                    <div class="device-holder-inner">
                                        <img class="img-responsive" src="daivajnya/assets/images/devices/screens/events.jpg" alt="" style=" height:482px;width: 270px;">
                                    </div><!--//device-holder-inner-->  
                                </div><!--//device-holder-->                  
                            </div><!--//phone-holder-->                         
                        </div><!--//row-->
                    </div><!--//container-->
                </div><!--//section-inner-->
            </section><!--//benefit-3-->
                    
 <section id="benefit-5" class="benefit-2 section gradient-3 has-pattern">
                <div class="section-inner">
                    <div class="container text-center">
                        <div class="row">
                            <div class="content col-md-7 col-sm-6 col-xs-12 col-md-push-4 col-sm-push-6 col-xs-push-0">
                                <div class="content-inner">
                                    <!-- <span class="pe-icon pe-7s-camera pe-4x"></span> -->
                                   <img src="daivajnya/assets/images/icons/jewellery.png">
                                    <h2 class="title text-center">Jewellery Hub</h2>
                                    <p class="intro text-center">It helps in Connecting jewellery shop owners to vendors and connecting vendors to talented jewellery workers</p>
                                </div><!--//content-inner-->
                                <div class="testimonial text-center">                                          
                                   <!--  <div class="quote-container">
                                        <i class="fa fa-quote-left"></i>
                                        <blockquote class="quote">
                                            This is a great app and it's very easy to use.                               
                                        </blockquote>
                                        <div class="source">
                                            <img class="profile" src="daivajnya/assets/images/profiles/profile-2.png" alt="" />
                                            <p class="meta">
                                                <span class="source-name"><a href="#">@GeraldChavez</a></span>
                                               
                                            </p>
                                        </div>
                                    </div>    -->           
                                </div><!--//testimonial-->
                            </div><!--//content-->
                            <div class="device device-iphone col-md-4 col-sm-6 col-xs-12 col-md-pull-7 col-sm-pull-6 col-xs-pull-0">     
                                <div class="device-holder iphone-holder iphone-gold-holder offset-top">                 
                                    <div class="device-holder-inner">
                                        <img class="img-responsive" src="daivajnya/assets/images/devices/screens/jhub.jpg" alt=""  style=" height: 482px;width: 270px;">
                                    </div><!--//device-holder-inner-->  
                                </div><!--//device-holder-->                  
                            </div><!--//phone-holder-->                         
                        </div><!--//row-->
                    </div><!--//container-->
                </div><!--//section-inner-->
            </section><!--//benefit-2-->

  <!-- ******Benefit-3 Section****** -->
            

 <section id="benefit-6" class="benefit-2 section gradient-8 has-pattern">
                <div class="section-inner">
                    <div class="container text-center">
                        <div class="row">
                            <div class="content col-md-7 col-sm-6 col-xs-12">
                                <div class="content-inner">
                                     <img src="daivajnya/assets/images/icons/matrimony.png">
                                    <h2 class="title text-center">Matrimony</h2>
                                    <p class="intro text-center">The matrimony Module Help us to connect groom and bride all over the world.</p>
                                </div><!--//content-inner-->
                                <div class="testimonial text-center">                                          
                                    <!-- <div class="quote-container">
                                        <i class="fa fa-quote-left"></i>
                                        <blockquote class="quote">
                                            Fantastic app!                                
                                        </blockquote>
                                        <div class="source">
                                            <img class="profile" src="daivajnya/assets/images/profiles/profile-3.png" alt="" />
                                            <p class="meta">
                                                <span class="source-name"><a href="#">Kelly Ryan</a></span>
                                                
                                            </p>
                                        </div>
                                    </div> -->                 
                                </div><!--//testimonial-->
                            </div><!--//content-->
                            <div class="device device-iphone col-md-4 col-sm-6 col-xs-12">     
                                <div class="device-holder iphone-holder iphone-gold-holder offset-top">                 
                                    <div class="device-holder-inner">
                                        <img class="img-responsive" src="daivajnya/assets/images/devices/screens/groom.jpg" alt="" style=" height: 482px;    width: 270px;">
                                    </div><!--//device-holder-inner-->  
                                </div><!--//device-holder-->                  
                            </div><!--//phone-holder-->                         
                        </div><!--//row-->
                    </div><!--//container-->
                </div><!--//section-inner-->
            </section><!--//benefit-3-->

 <section id="benefit-7" class="benefit-2 section gradient-9 has-pattern">
                <div class="section-inner">
                    <div class="container text-center">
                        <div class="row">
                            <div class="content col-md-7 col-sm-6 col-xs-12 col-md-push-4 col-sm-push-6 col-xs-push-0">
                                <div class="content-inner">
                                    <!-- <span class="pe-icon pe-7s-camera pe-4x"></span> -->
                                   <img src="daivajnya/assets/images/icons/b2.png">
                                    <h2 class="title text-center">B2B</h2>
                                    <p class="intro text-center">We can post our Business adds and view the business adds</p>
                                </div><!--//content-inner-->
                                <div class="testimonial text-center">                                          
                                    <!-- <div class="quote-container">
                                        <i class="fa fa-quote-left"></i>
                                        <blockquote class="quote">
                                            This is a great app and it's very easy to use .                               
                                        </blockquote>
                                        <div class="source">
                                            <img class="profile" src="daivajnya/assets/images/profiles/profile-2.png" alt="" />
                                            <p class="meta">
                                                <span class="source-name"><a href="#">@GeraldChavez</a></span>
                                                
                                            </p>
                                        </div>
                                    </div>  -->               
                                </div><!--//testimonial-->
                            </div><!--//content-->
                            <div class="device device-iphone col-md-4 col-sm-6 col-xs-12 col-md-pull-7 col-sm-pull-6 col-xs-pull-0">     
                                <div class="device-holder iphone-holder iphone-gold-holder offset-top">                 
                                    <div class="device-holder-inner">
                                        <img class="img-responsive" src="daivajnya/assets/images/devices/screens/b2b.jpg" alt=""  style=" height:482px;width: 270px;">
                                    </div><!--//device-holder-inner-->  
                                </div><!--//device-holder-->                  
                            </div><!--//phone-holder-->                         
                        </div><!--//row-->
                    </div><!--//container-->
                </div><!--//section-inner-->
            </section><!--//benefit-2-->

 <!-- <section id="benefit-6" class="benefit-2 section gradient-9 has-pattern">
                <div class="section-inner">
                    <div class="container text-center">
                        <div class="row">
                            <div class="content col-md-7 col-sm-6 col-xs-12">
                                <div class="content-inner">
                                    <span class="pe-icon pe-7s-global pe-4x"></span>
                                    <h2 class="title text-center">B2B</h2>
                                    <p class="intro text-center">State one of your product benefits/features here. You can easily replace the screenshot in the phone with your own image. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</p>
                                </div> 
                                <div class="testimonial text-center">                                          
                                    <div class="quote-container">
                                        <i class="fa fa-quote-left"></i>
                                        <blockquote class="quote">
                                            Fantastic app! I love the Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere viverra dignissim. Etiam tincidunt varius luctus. Quisque est ex, lacinia id tempor et!                                
                                        </blockquote>
                                        <div class="source">
                                            <img class="profile" src="daivajnya/assets/images/profiles/profile-3.png" alt="" />
                                            <p class="meta">
                                                <span class="source-name"><a href="#">Kelly Ryan</a></span>
                                                <span class="source-title">London, UK</span>
                                            </p>
                                        </div> 
                                    </div>                 
                                </div> 
                            </div> 
                            <div class="device device-iphone col-md-4 col-sm-6 col-xs-12">     
                                <div class="device-holder iphone-holder iphone-gold-holder offset-top">                 
                                    <div class="device-holder-inner">
                                        <img class="img-responsive" src="daivajnya/assets/images/devices/screenshots/jhscreen.jpg" alt="" style=" height: 482px;">
                                    </div>
                                </div>                 
                            </div>                         
                        </div>
                    </div>
                </div>
            </section>   -->



<section id="benefit-8" class="benefit-2 section gradient-10 has-pattern">
                <div class="section-inner">
                    <div class="container text-center">
                        <div class="row">
                            <div class="content col-md-7 col-sm-6 col-xs-12">
                                <div class="content-inner">
                                    <img src="daivajnya/assets/images/icons/education.png">
                                    <h2 class="title text-center"> Careers</h2>
                                    <p class="intro text-center">Careers and Talents is one of the top job portals in India and has more than thousands of job listings covering various sectors and job profiles. So it is indeed a very good Module for searching jobs in India.</p>
                                </div><!--//content-inner-->
                                <div class="testimonial text-center">                                          
                                   <!--  <div class="quote-container">
                                        <i class="fa fa-quote-left"></i>
                                        <blockquote class="quote">
                                            For seaching job It's very Helpfull.                              
                                        </blockquote>
                                        <div class="source">
                                            <img class="profile" src="daivajnya/assets/images/profiles/profile-3.png" alt="" />
                                            <p class="meta">
                                                <span class="source-name"><a href="#"> Kelly Ryan</a></span>
                                               
                                            </p>
                                        </div>
                                    </div>  -->                  
                                </div><!--//testimonial-->
                            </div><!--//content-->
                            <div class="device device-iphone col-md-4 col-sm-6 col-xs-12">     
                                <div class="device-holder iphone-holder iphone-gold-holder offset-top">                 
                                    <div class="device-holder-inner">
                                        <img class="img-responsive" src="daivajnya/assets/images/devices/screens/career.jpg" alt="" style="height:482px;width:270px;">
                                    </div><!--//device-holder-inner-->  
                                </div><!--//device-holder-->                  
                            </div><!--//phone-holder-->                         
                        </div><!--//row-->
                    </div><!--//container-->
                </div><!--//section-inner-->
            </section><!--//benefit-3-->

<section id="benefit-9" class="benefit-2 section gradient-11 has-pattern">
                <div class="section-inner">
                    <div class="container text-center">
                        <div class="row">
                            <div class="content col-md-7 col-sm-6 col-xs-12">
                                <div class="content-inner">
                                      <img src="daivajnya/assets/images/icons/temp.png">
                                   <!--  <span class="pe-icon pe-7s-global pe-4x"></span> -->
                                    <h2 class="title text-center"> Temples</h2>
                                    <p class="intro text-center">Here view can view the all the temple details of daivajnya brahmin.</p>
                                </div><!--//content-inner-->
                                <div class="testimonial text-center">                                          
                                   <!--  <div class="quote-container">
                                        <i class="fa fa-quote-left"></i>
                                        <blockquote class="quote">
                                            Fantastic app! I love this app            
                                        </blockquote>
                                        <div class="source">
                                            <img class="profile" src="daivajnya/assets/images/profiles/profile-3.png" alt="" />
                                            <p class="meta">
                                                <span class="source-name"><a href="#">Kelly Ryan</a></span>
                                                <span class="source-title">London, UK</span>
                                            </p>
                                        </div>
                                    </div>  -->                
                                </div><!--//testimonial-->
                            </div><!--//content-->
                            <div class="device device-iphone col-md-4 col-sm-6 col-xs-12">     
                                <div class="device-holder iphone-holder iphone-gold-holder offset-top">                 
                                    <div class="device-holder-inner">
                                        <img class="img-responsive" src="daivajnya/assets/images/devices/screens/listoftemple.jpg" alt="" style=" height:482px;width: 270px;">
                                    </div><!--//device-holder-inner-->  
                                </div><!--//device-holder-->                  
                            </div><!--//phone-holder-->                         
                        </div><!--//row-->
                    </div><!--//container-->
                </div><!--//section-inner-->
            </section><!--//benefit-3-->





            <!-- ******CTA Section****** -->
            <section id="cta-section" class="cta-section section gradient-1">
                <!-- <div class="press text-center">
                    <div class="press-inner">
                        <div class="container text-center">
                            <h2 class="title">As featured on...</h2>
                            <div class="row">
                                <ul class="list-unstyled press-list">
                                    <li class="col-md-2 col-sm-4 col-xs-6"><a href="press.html"><img class="img-responsive" src="daivajnya/assets/images/press/logo-1.png" alt=""></a></li>
                                    <li class="col-md-2 col-sm-4 col-xs-6"><a href="press.html"><img class="img-responsive" src="daivajnya/assets/images/press/logo-2.png" alt=""></a></li>
                                    
                                    <li class="col-md-2 col-sm-4 col-xs-6"><a href="press.html"><img class="img-responsive" src="daivajnya/assets/images/press/logo-3.png" alt=""></a></li>                         
                                    <li class="col-md-2 col-sm-4 col-xs-6"><a href="press.html"><img class="img-responsive" src="daivajnya/assets/images/press/logo-4.png" alt=""></a></li>
                                    <li class="col-md-2 col-sm-4 col-xs-6"><a href="press.html"><img class="img-responsive" src="daivajnya/assets/images/press/logo-5.png" alt=""></a></li>
                                    <li class="col-md-2 col-sm-4 col-xs-6"><a href="press.html"><img class="img-responsive" src="daivajnya/assets/images/press/logo-6.png" alt=""></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>  -->
                <div class="download text-center ">
                    <div class="container">
                        <h3 class="title">Download Daivajnya Brahmin now <br />and  Connect to Daivajnya Brahmin all over the world by this app.</h3>
                        <ul class="list-inline download-list">
                            <li><a class="btn btn-cta btn-cta-primary" href="https://itunes.apple.com/in/app/daivajnyabrahmin/id1431653216?mt=8"><i class="fa fa-apple"></i><span class="text">Download for iOS</span> </a></li>
                            <li><a class="btn btn-cta btn-cta-primary" href="https://play.google.com/store/apps/details?id=com.gmediasolutions.app.daivajnyabrahmin"><i class="fa fa-android"></i><span class="text">Download for Android</span></a></li>
                            <!-- <li><a class="btn btn-cta btn-cta-primary" href="#"><i class="fa fa-windows"></i><span class="text">Download for Windows</span></a></li> -->
                        </ul>
                        <figure class="qr-holder">
                            <img src="daivajnya/assets/images/buttons/QR-code.png" alt="" />
                            <figcaption class="caption">Scan the QR code to download</figcaption>
                        </figure>
                    </div><!--//container-->
                </div><!--//download-->
            </section><!--//cta-section-->        
        </div><!--//sections-wrapper-->
        
            
        <!-- ******FOOTER****** --> 
        
        <footer class="footer ">
            <div class="container text-center">
                <nav class="links">
                    <ul class="list-inline">

                         <li><a href="home">Home</a></li>
                        <li><a href="about">About</a></li>
                      
                        <li><a href="pricing">Pricing</a></li>
                        <li><a href="faqs">FAQs</a></li>
                      
                       
                        <li><a href="contact">Contact Us</a></li>
                    </ul>                    
                </nav>
                <ul class="social-icons list-inline">
                    <li><a href="https://twitter.com"><i class="fa fa-twitter"></i></a></li>                        
                    <li><a href="https://www.facebook.com"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://www.instagram.com"><i class="fa fa-instagram"></i></a></li>
                    <li class="last"><a href="https://www.linkedin.com"><i class="fa fa-linkedin"></i></a></li>                    
                </ul>
                <ul class="links legal-links list-inline">
                    <li><a href="#">Privacy</a></li>
                    <li class="last"><a href="#">Terms</a></li>
                </ul>
                <small class="copyright">Copyright &copy; 2018 <a href="http://daivajnyabrahmin.com"> Daivajnya Brahmin in All right reserved</a></small>
                
            </div>
        </footer> 


    </div><!--//canvas-wrapper-->

     <!--  <footer class="footer ">
            <div class="container text-center">
                <nav class="links">
                    <ul class="list-inline">

                         <li><a href="home">Home</a></li>
                        <li><a href="about">About</a></li>
                      
                        <li><a href="pricing">Pricing</a></li>
                        <li><a href="faqs">FAQs</a></li>
                      
                       
                        <li><a href="contact">Contact Us</a></li>
                    </ul>                    
                </nav>
                <ul class="social-icons list-inline">
                    <li><a href="https://twitter.com"><i class="fa fa-twitter"></i></a></li>                        
                    <li><a href="https://www.facebook.com"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://www.instagram.com"><i class="fa fa-instagram"></i></a></li>
                    <li class="last"><a href="https://www.linkedin.com"><i class="fa fa-linkedin"></i></a></li>                    
                </ul>
                <ul class="links legal-links list-inline">
                    <li><a href="#">Privacy</a></li>
                    <li class="last"><a href="#">Terms</a></li>
                </ul>
                <small class="copyright">Copyright &copy; 2018 <a href="http://daivajnyabrahmin.com"> daivajnyabrahmin.com in All right reserved</a></small>
                
            </div>
        </footer>
    
    <!-- *****Video Modal***** -->
    <div class="modal modal-video" id="modal-video" tabindex="-1" role="dialog" aria-labelledby="videoModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 id="videoModalLabel" class="modal-title sr-only">Video Tour</h4>
                </div>
                <div class="modal-body">
                  <div class=" text-center"> 
                       <!--  <iframe id="vimeo-video" src="daivajnya/assets/images/media/dbvideo." width="720" height="405" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> -->
                 <video controls="controls" width="720" class="video-wrapper">
                 <source src="daivajnya/assets/images/media/dbvideo.mp4" type="video/mp4">
               
                       
                      </video>



                 </div> 
                </div><!--//modal-body-->
            </div><!--//modal-content-->
        </div><!--//modal-dialog-->
    </div><!--//modal-->
    

    <!-- Main Javascript -->          
    <script type="text/javascript" src="daivajnya/assets/plugins/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="daivajnya/assets/plugins/bootstrap/js/bootstrap.min.js"></script>  
    <script type="text/javascript" src="daivajnya/assets/plugins/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>             
    <script type="text/javascript" src="daivajnya/assets/plugins/jquery-placeholder/jquery.placeholder.js"></script>  
    <script type="text/javascript" src="daivajnya/assets/plugins/FitVids/jquery.fitvids.js"></script>                                                                
    <script type="text/javascript" src="daivajnya/assets/js/main.js"></script>
    
    <!-- Fullpage.js -->
    <script type="text/javascript" src="daivajnya/assets/plugins/fullPage.js/jquery.fullPage.min.js"></script>
    <script type="text/javascript" src="daivajnya/assets/js/fullpage-custom.js"></script> 
    
    <!-- Owl Carousel -->
    <script type="text/javascript" src="daivajnya/assets/plugins/owl-carousel/owl.carousel.js"></script> 
    <script type="text/javascript" src="daivajnya/assets/js/owl-custom.js"></script> 

    
    <!-- Modal video -->
    <script type="text/javascript" src="daivajnya/assets/js/modal-video.js"></script>

    
</body>


</html> 

