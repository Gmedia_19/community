 <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <link href="{{asset('news and events/css/bootstrap.min.css')}}" rel="stylesheet">
 <link href="{{asset('news and events/css/style.css')}}" rel="stylesheet">
 <script type="text/javascript" src="{{asset('news and events/js/jquery.min.js')}}">
 </script>
 <script type="text/javascript" src="{{asset('news and events/js/popper.min.js')}}">
 </script>
 <script type="text/javascript" src="{{asset('news and events/js/bootstrap.min.js')}}"></script>
 <script type="text/javascript" src="{{asset('news and events/js/news.js')}}"></script>



<style>


@media (min-width: 576px){
.navbar-expand-sm .navbar-nav .nav-link {
    padding-right: .5rem;
    padding-left: 2.5rem;
    font-family: robot;
    font-size: 18px;
    letter-spacing: 1px;
}
}

.navbar-dark .navbar-nav .nav-link {
    color: #fff;
}

.navbar-dark .navbar-brand {
    color: #fff;
    font-family: robot;
    font-size: 18px;
}
.card-title {
    margin-bottom: .75rem;
    font-family: 'Frank Ruhl Libre',Georgia,serif;
    font-size: 1.2em;
    line-height: 1.15em;
}

.bg-dark {
    background-color: #eeaf04!important;
    height: 47px;
}

 div.subcontent { display:none; }

 .hd
 {
    font-family: robot;
    color: #805d01;
 }

 .border-secondary1 {
    /* border-color: #6c757d!important; */
    box-shadow: 0px 0px 1px 1px #dad5d5;
}

</style>
</head>

